import os
import sys
import argparse
import signal
import time
import datetime
import subprocess

def install_debian_packages():
    """Download all of the packages we use (pip mostly)"""
    if(os.getuid() != 0): #if not sudoer
        raise(Exception("Must be sudoer to install packages. Exiting."))
    mysql_pwd = "averysecurepassword!"
    # Software provided by advanced package tool
    apt_packages = ["debconf-utils",
                    "mysql-server",
                    "libmysqlclient-dev",
                    "libluajit-5.1-dev",
                    "libzmq3-dev",
                    "autoconf",
                    "pkg-config",
                    "build-essential",
                    "python-pip",
                    "python3-pip"
                   ]
    # Update before we get started
    os.system("apt-get update -y")
    # Need this to check our cache
    os.system("apt-get install -y python-apt")
    # Now we use it!
    import apt
    cache = apt.Cache()
    # Install all apt packages
    for package in apt_packages:
        if cache[package].is_installed:
            print("%s is installed and ready." % package)
        else:
            print("%s is not installed; installing now." % package)
            if package == "mysql-server": #set password so we don't have prompts...
                os.system("export DEBIAN_FRONTEND='noninteractive'")
                os.system("debconf-set-selections <<<" +
                    "'mysql-server mysql-server/root_password password %s'" %mysql_pwd)
                os.system("debconf-set-selections <<<" +
                    "'mysql-server mysql-server/root_password_again password %s'" %mysql_pwd)
            os.system("apt-get install -y %s" % package)
    return(0)

def install_python_packages():
    """In case the imports below fail, have a plan to install."""
    if(os.getuid() != 0): #if not sudoer
        raise(Exception("Must be sudoer to install packages. Exiting."))
    # Software provided by python2 pip
    pip_packages = ["libtmux",
                    "psutil",
                    "schedule"
                   ]
    # Software provided by python3 pip
    pip3_packages= ["sqlalchemy",
                    "pymysql",
                    "beautifulsoup4",
                    "pyyaml",
                    "six"
                   ]
    # Install pip/pip3 packages
    for package in pip_packages:
        os.system("pip install %s" % package)
    for package in pip3_packages:
        os.system("pip3 install %s" % package)
    return(0)

#NOTE: This is how we avoid crashing on new systems. I know it's ugly. I'm fucking sorry.
try:
    import libtmux
    import psutil
    import schedule
except:
    print("Python packages missing; attempting to install!")
    install_debian_packages()
    install_python_packages()


class Game_Service:
    def __init__(self, window, parent, command):
        self.window = window
        self.parent = parent
        self.command = command

    def is_running(self):
        """Check if the window is running its process."""
        return(self.get_current_command() == self.command)

    def start(self):
        """Start the service this window is assigned to run."""
        self.window.panes[0].reset()
        self.window.panes[0].send_keys(self.command)
        return(0)

    def stop(self):
        """Stop the window's running service (usually to start again).
        It may be the case that we don't need to check if it's running
        but I'm erring to the side of caution.
        """
        if(self.is_running()):
            #the process is still running
            self.parent.proc_man.clear_process(self.get_pid())
        else:
            #The process is no longer running
            self.parent.proc_man.clear_process(self.get_pid())
        return(0)

    def restart(self):
        """Restart the service in this window."""
        self.stop()
        self.start()
        return(0)

    def get_pid(self):
        """Get the process id of this window's pane."""
        return(self.window.panes[0]._info["pane_pid"])

    def get_current_command(self):
        """Get the command this window's pane is running"""
        return(str(self.window.panes[0]._info["pane_current_command"]))

class Process_Manager:
    def __init__(self, window, parent):
        self.window = window
        self.parent = parent

    def clear_process(self, pid, sig=signal.SIGKILL):
        """Kill all threads inside a tmux pane.
        Not sure if it needs to be its own obj, but why not.
        """
        try:
            window_process = psutil.Process(pid)
            self.announce("Clearing process: %s (%s)" % (pid,str(window_process)))
        except:
            self.announce("Error clearing process: %s" %str(pid))
            return(-1)
        children = window_process.children(recursive=True)
        for process in children:
            self.announce("Killing %s!" % str(process))
            process.send_signal(sig)
        return(0)

    def announce(self, message):
        """Echo message to the Process_Manager window in tmux."""
        self.window.panes[0].send_keys("echo " + str(message))

class Handler:
    def __init__(self):
        self.services = []
        self.cwd = os.getcwd()
        self.logfile = self.cwd + "/log/" + "qlog.txt"

    def log(self, message):
        """Write out the handler's current message. (Not used much)"""
        with open(self.logfile, "a") as f:
            f.write(str(message + "\n"))

    def launch_services(self):
        """Launch all of the services in tmux panes and keep handles for API interaction.

        1) create the session, windows, panes(?) for the services
        2) create the Game_Service objects
        3) create the schedule componenets
        4) run until stopped for some reason.
        """

        self.tmux_server = libtmux.Server()
        self.game_session = self.tmux_server.new_session("game")


        win0 = self.game_session.new_window(
                window_name="dsgame",
                start_directory = self.cwd,
                attach = False,
                window_shell = None)

        win1 = self.game_session.new_window(
                window_name="dsconnect",
                start_directory = self.cwd,
                attach = False,
                window_shell = None)

        win2 = self.game_session.new_window(
                window_name="dssearch",
                start_directory = self.cwd,
                attach = False,
                window_shell = None)

        win3 = self.game_session.new_window(
                window_name="process_manager",
                start_directory = self.cwd,
                attach = False,
                window_shell = None)

        dsgame = Game_Service(win0, self, "./dsgame")
        dsconnect = Game_Service(win1, self, "./dsconnect")
        dssearch = Game_Service(win2, self, "./dssearch")
        self.proc_man = Process_Manager(win3, self)
        self.services.append(dsgame)
        self.services.append(dsconnect)
        self.services.append(dssearch)
        for service in self.services:
            service.start()

        #finally, start watching our threads.
        self.start_scheduled_tasks()
        while(True):
            schedule.run_pending()
            time.sleep(5)

    def start_scheduled_tasks(self):
        """Use schedule library to start watching our servers."""
        #TODO add in other timed utilities (mysql backup; AH restock; MotM)
        schedule.every(10).seconds.do(self.check_status)
        self.proc_man.announce("Starting services...")
        # Backup mysql every day at 0600 (change later if we want)
        schedule.every().day.at("06:00").do(self.mysql_backup)
        # Restock the auction house every monday at 0605
        schedule.every().monday.at("06:05").do(self.restock_AH)
        #TODO quickly testing this out
        #schedule.every().day.at("14:05").do(self.restock_AH)

    def check_status(self):
        """Verify that all running services are working; restart them if they went down."""
        status = 0 #nothing to report
        for service in self.services:
            if(not service.is_running()):
                self.log("Restarting %s" % 
                        str(service.window.panes[0]._info["window_name"]))
                self.proc_man.announce("Restarting %s" % 
                        str(service.window.panes[0]._info["window_name"]))
                service.restart()
        return(status)

    def mysql_backup(self):
        """Take a snapshot of the database and store it with a unique name."""
        status = 0
        self.proc_man.announce("Taking scheduled backup of database.")
        timestamp = str(datetime.datetime.now().isoformat())
        backup_file = "backups/%s_backup.sql" % timestamp
        try:
            os.system('touch %s' % backup_file)
            os.system('mysqldump -u darkstar -pATeamVan dspdb > %s' % backup_file)
            self.proc_man.announce("Backup successful.")
        except:
            self.proc_man.announce("Couldn't take backup of database!")
            status = -1
        return(status)

    def restock_AH(self):
        """Refill the auction house (takes a while, so weekly is ok.)"""
        self.proc_man.announce("Restocking the AH (THIS TAKES A LONG FUCKING TIME).")
        os.chdir(os.getcwd() + "/pydarkstar/bin")
        script = "refill.sh"
        status = os.system("sh %s --force" %script)
        os.chdir("../../")
        self.proc_man.announce("AH successfully restocked!")
        return(status)

def start_game_server():
    """Start the game server."""
    if(os.getuid() == 0): #if sudoer:
        raise(Exception("Sudoer is not allowed to use services. Exiting."))
    handler = Handler()
    handler.launch_services()
    sys.exit(2)

def halt_game_server(serv):
    """Attempt to halt the running server; write out tmux windows to log files first."""
    if(os.getuid() == 0): #if sudoer:
        raise(Exception("Sudoer is not allowed to use services. Exiting."))
    try:
        sess = serv.find_where({"session_name": "game"})
        timestamp = str(datetime.datetime.now().isoformat())
        for window in sess.windows:
            log = str('\n'.join(window.panes[0].cmd('capture-pane', '-p').stdout))
            name = window._info["window_name"]
            fname = "log/%stmux_%s" % (timestamp, name)
            with open(fname, 'w') as f:
                f.write(log)
                print("Wrote out log: %s" % fname)
        serv.kill_session("game")
        print("Successfully stopped game server.")
    except:
        print("Couldn't stop game server!")
    finally:
        return(0)

def check_game_server(serv):
    """Look in active tmux sessions to see if our game (server*3) is running."""
    if(os.getuid() == 0): #if sudoer:
        raise(Exception("Sudoer is not allowed to use services. Exiting."))
    procs = []
    try:
        for session in serv.sessions:
            for window in session.windows:
                for pane in window.panes:
                    proc = psutil.Process(int(pane._info["pane_pid"]))
                    children = proc.children(recursive=True)
                    for process in children:
                        procs.append(str(process.name()))
        if str("dsgame") in procs:
            print("dsgame appears to be running in this session.")
        else:
            print("dsgame not found in session!")
        if str("dsconnect") in procs:
            print("dsconnect appears to be running in this session.")
        else:
            print("dsconnect not found in session!")
        if str("dssearch") in procs:
            print("dssearch appears to be running in this session.")
        else:
            print("dssearch not found in session!")
    except:
        print("No game servers found; are you sure it is running?")
    finally:
        sys.exit(2)

def deploy_game_server(compile_source=False, mysql_new=False, mysql_bak=False):
    """
    Install everything needed to run the server; configure files; etc.
    WIP: may still add compilation, mysql loading, etc!
    """
    # Install all of our packages
    status = 0
    if(os.getuid() != 0): #if not sudoer
        raise(Exception("Must be sudoer to install packages. Exiting."))
    install_debian_packages()
    install_python_packages()

    # Drop sudo on next commands so we don't mess with file permissions.
    user = os.environ["SUDO_USER"]

    # Run AH bot bin generator
    os.chdir("pydarkstar")
    user_command('python3 makebin.py', user)
    os.chdir("..")
    # Move saved items.csv and config files into AH bot
    user_command('cp conf/items.csv pydarkstar/bin/items.csv', user)
    user_command('cp conf/config.yaml pydarkstar/bin/config.yaml', user)

    # Compile server source if we're doing that
    if(compile_source):
        compile_server_source(user)
    # New database?
    if mysql_new:
        database_new(user)
    # Loading a database backup?
    if mysql_bak:
        database_roll_back(user)

    print("\n\n Everything appears to have installed correctly-- proceed to database setup.\n\n")
    return(status)

def user_command(command, user):
    """Run command as user instead of root."""
    status = os.system("su -c '%s' %s" % (command, user))
    return(status)

def compile_server_source(user):
    """Compile the server source code (only used in deployment)."""
    user_command("sh autogen.sh", user)
    user_command("./configure --enable-debug=gdb", user)
    user_command("make", user)
    return(0)

def database_new():
    """Not implementing this; I worry we might accidentally hit it."""
    return(0)

def database_roll_back():
    """Not implementing this; I worry we might accidentally hit it."""
    return(0)

def main():
    parser = argparse.ArgumentParser(description="Automatically run game services")
    parser.add_argument('--restart', action='store_true', default=False,
            dest='restart', help='Force restart the running server.')
    parser.add_argument('--start',action='store_true', default=False,
            dest='start', help='Start the server.')
    parser.add_argument('--check',action='store_true', default=False,
            dest='check', help="Check the server's status.")
    parser.add_argument('--stop',action='store_true', default=False,
            dest='stop', help="Stop the server (if running).")
    parser.add_argument('--deploy',action='store_true', default=False,
            dest='deploy', help="Install packages, unpack configs, etc; only needed once.")
    parser.add_argument('-c',action='store_true', default=False,
            dest='comp', help="Compile source code (only works with --deploy)")
    results = parser.parse_args()

    if(results.restart):
        s = libtmux.Server()
        if s.has_session("game", True):
            halt_game_server(s)
            time.sleep(1)
            start_game_server()
        else:
            raise(Exception("Game server not found. Exiting."))

    elif(results.start):
        s = libtmux.Server()
        if s.has_session("game", True):
            raise(Exception("Game server already running. Exiting."))
        else:
            start_game_server()

    elif(results.check):
        s = libtmux.Server()
        check_game_server(s)

    elif(results.stop):
        s = libtmux.Server()
        if s.has_session("game", True):
            halt_game_server(s)
        sys.exit(2)

    elif(results.deploy):
        deploy_game_server(results.comp)
        sys.exit(2)

    else:
        parser.print_help()
        sys.exit(2)

if __name__ == "__main__":
    main()
