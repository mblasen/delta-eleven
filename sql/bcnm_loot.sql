/*
MySQL Data Transfer
Source Host: localhost
Source Database: dspdb
Target Host: localhost
Target Database: dspdb
Date: 31/08/2012 18:09:30
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for bcnm_loot
-- ----------------------------
DROP TABLE IF EXISTS `bcnm_loot`;
CREATE TABLE `bcnm_loot` (
  `LootDropId` smallint(5) unsigned NOT NULL default '0',
  `itemId` smallint(5) unsigned NOT NULL default '0',
  `rolls` smallint(5) unsigned NOT NULL default '0',
  `lootGroupId` tinyint(3) unsigned NOT NULL default '0',
  KEY `LootDropId` (`LootDropId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AVG_ROW_LENGTH=9;

--  ----------------------------
--  Records
--  ----------------------------
INSERT INTO `bcnm_loot` VALUES ('100', '13292', '75', '0'); --  Guardian's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13287', '32', '0'); --  Kampfer Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13300', '54', '0'); --  Conjurer's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13298', '32', '0'); --  Shinobi Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13293', '97', '0'); --  Slayer's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13289', '75', '0'); --  Sorcerer's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13286', '108', '0'); --  Soldier's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13294', '22', '0'); --  Tamer's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13296', '65', '0'); --  Tracker's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13299', '32', '0'); --  Drake Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13290', '32', '0'); --  Fencer's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13295', '86', '0'); --  Minstrel's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13288', '86', '0'); --  Medicine Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13291', '75', '0'); --  Rogue's Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13297', '11', '0'); --  Ronin Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13447', '32', '0'); --  Platinum Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13548', '376', '1'); --  Astral Ring
INSERT INTO `bcnm_loot` VALUES ('100', '13447', '22', '1'); --  Platinum Ring
INSERT INTO `bcnm_loot` VALUES ('100', '4818', '65', '1'); --  Scroll of Quake
INSERT INTO `bcnm_loot` VALUES ('100', '859', '0', '1'); --  Ram Skin, 0% according to wiki
INSERT INTO `bcnm_loot` VALUES ('100', '4172', '11', '1'); --  Reraiser
INSERT INTO `bcnm_loot` VALUES ('100', '653', '22', '1'); --  Mythril Ingot
INSERT INTO `bcnm_loot` VALUES ('100', '4902', '0', '1'); --  Light Spirit Pact, 0% according to wiki
INSERT INTO `bcnm_loot` VALUES ('100', '4814', '32', '1'); --  Scroll of Freeze
INSERT INTO `bcnm_loot` VALUES ('100', '4719', '43', '1'); --  Scroll of Regen III
INSERT INTO `bcnm_loot` VALUES ('100', '4621', '32', '1'); --  Scroll of Raise II
INSERT INTO `bcnm_loot` VALUES ('100', '703', '11', '1'); --  Petrified Log
INSERT INTO `bcnm_loot` VALUES ('100', '887', '11', '1'); --  Coral Fragment
INSERT INTO `bcnm_loot` VALUES ('100', '700', '11', '1'); --  Mahogany Log
INSERT INTO `bcnm_loot` VALUES ('100', '738', '43', '1'); --  Platinum Ore
INSERT INTO `bcnm_loot` VALUES ('100', '737', '108', '1'); --  Gold Ore
INSERT INTO `bcnm_loot` VALUES ('100', '645', '32', '1'); --  Darksteel Ore
INSERT INTO `bcnm_loot` VALUES ('100', '644', '65', '1'); --  Mythril Ore
INSERT INTO `bcnm_loot` VALUES ('100', '745', '0', '1'); --  Gold Ingot, 0% according to wiki
INSERT INTO `bcnm_loot` VALUES ('100', '654', '11', '1'); --  Darksteel Ingot
INSERT INTO `bcnm_loot` VALUES ('100', '746', '11', '1'); --  Platinum Ingot
INSERT INTO `bcnm_loot` VALUES ('100', '702', '11', '1'); --  Ebony Log
INSERT INTO `bcnm_loot` VALUES ('100', '895', '11', '1'); --  Ram Horn
INSERT INTO `bcnm_loot` VALUES ('100', '902', '11', '1'); --  Demon Horn
INSERT INTO `bcnm_loot` VALUES ('100', '1116', '0', '1'); --  Manticore Hide, 0% according to wiki
INSERT INTO `bcnm_loot` VALUES ('100', '1122', '11', '1'); --  Wyvern Skin
INSERT INTO `bcnm_loot` VALUES ('100', '866', '11', '1'); --  Wyvern Scales
INSERT INTO `bcnm_loot` VALUES ('73', '4570', '1000', '0'); --  Bird Egg
INSERT INTO `bcnm_loot` VALUES ('73', '14735', '126', '1'); --  Ashigaru Earring
INSERT INTO `bcnm_loot` VALUES ('73', '14732', '125', '1'); --  Trimmer's Earring
INSERT INTO `bcnm_loot` VALUES ('73', '14734', '125', '1'); --  Beater's Earring
INSERT INTO `bcnm_loot` VALUES ('73', '13437', '125', '1'); --  Healer's Earring
INSERT INTO `bcnm_loot` VALUES ('73', '13435', '125', '1'); --  Mercenary's Earring
INSERT INTO `bcnm_loot` VALUES ('73', '14733', '125', '1'); --  Singer's Earring
INSERT INTO `bcnm_loot` VALUES ('73', '13438', '125', '1'); --  Wizard's Earring
INSERT INTO `bcnm_loot` VALUES ('73', '13436', '125', '1'); --  Wrestler's Earring
INSERT INTO `bcnm_loot` VALUES ('73', '15285', '142', '2'); --  Avatar Belt
INSERT INTO `bcnm_loot` VALUES ('73', '15276', '142', '2'); --  Dagger Belt
INSERT INTO `bcnm_loot` VALUES ('73', '15284', '142', '2'); --  Lance Belt
INSERT INTO `bcnm_loot` VALUES ('73', '15275', '142', '2'); --  Rapier Belt
INSERT INTO `bcnm_loot` VALUES ('73', '15283', '142', '2'); --  Sarashi
INSERT INTO `bcnm_loot` VALUES ('73', '15278', '142', '2'); --  Scythe Belt
INSERT INTO `bcnm_loot` VALUES ('73', '15277', '142', '2'); --  Shield Belt
INSERT INTO `bcnm_loot` VALUES ('73', '4868', '105', '3'); --  Scroll of Dispel
INSERT INTO `bcnm_loot` VALUES ('73', '4751', '79', '3'); --  Scroll of Erase
INSERT INTO `bcnm_loot` VALUES ('73', '5070', '421', '3'); --  Scroll of Magic Finale
INSERT INTO `bcnm_loot` VALUES ('73', '4947', '79', '3'); --  Scroll of Utsusemi: Ni
INSERT INTO `bcnm_loot` VALUES ('73', '847', '330', '4'); --  Bird Feather
INSERT INTO `bcnm_loot` VALUES ('73', '694', '130', '4'); --  Chestnut Log
INSERT INTO `bcnm_loot` VALUES ('73', '690', '125', '4'); --  Elm Log
INSERT INTO `bcnm_loot` VALUES ('73', '4132', '210', '4'); --  Hi-Ether
INSERT INTO `bcnm_loot` VALUES ('73', '4222', '235', '4'); --  Horn Quiver
INSERT INTO `bcnm_loot` VALUES ('73', '651', '98', '4'); --  Iron Ingot
INSERT INTO `bcnm_loot` VALUES ('73', '795', '130', '4'); --  Lapis Lazuli
INSERT INTO `bcnm_loot` VALUES ('73', '796', '150', '4'); --  Light Opal
INSERT INTO `bcnm_loot` VALUES ('73', '653', '184', '4'); --  Mythril Ingot
INSERT INTO `bcnm_loot` VALUES ('73', '644', '159', '4'); --  Mythril Ore
INSERT INTO `bcnm_loot` VALUES ('73', '799', '160', '4'); --  Onyx
INSERT INTO `bcnm_loot` VALUES ('73', '736', '180', '4'); --  Silver Ore
INSERT INTO `bcnm_loot` VALUES ('73', '744', '175', '4'); --  Silver Ingot
INSERT INTO `bcnm_loot` VALUES ('76', '1339', '200', '0'); -- the hills are alive
INSERT INTO `bcnm_loot` VALUES ('76', '1340', '200', '0'); -- neptunal abjurations
INSERT INTO `bcnm_loot` VALUES ('76', '1341', '200', '0');
INSERT INTO `bcnm_loot` VALUES ('76', '1342', '200', '0');
INSERT INTO `bcnm_loot` VALUES ('76', '1343', '200', '0');
INSERT INTO `bcnm_loot` VALUES ('76', '1339', '200', '1');
INSERT INTO `bcnm_loot` VALUES ('76', '1340', '200', '1'); -- neptunals again
INSERT INTO `bcnm_loot` VALUES ('76', '1341', '200', '1');
INSERT INTO `bcnm_loot` VALUES ('76', '1342', '200', '1');
INSERT INTO `bcnm_loot` VALUES ('76', '1343', '200', '1');
INSERT INTO `bcnm_loot` VALUES ('76', '1324', '200', '2'); -- aquarian abjurations
INSERT INTO `bcnm_loot` VALUES ('76', '1325', '200', '2');
INSERT INTO `bcnm_loot` VALUES ('76', '1326', '200', '2');
INSERT INTO `bcnm_loot` VALUES ('76', '1327', '200', '2');
INSERT INTO `bcnm_loot` VALUES ('76', '1328', '200', '2');
INSERT INTO `bcnm_loot` VALUES ('76', '1324', '200', '3'); -- aquarian abjurations
INSERT INTO `bcnm_loot` VALUES ('76', '1325', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('76', '1326', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('76', '1327', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('76', '1328', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('76', '1329', '200', '4'); -- martial abjurations
INSERT INTO `bcnm_loot` VALUES ('76', '1330', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('76', '1331', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('76', '1332', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('76', '1333', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('76', '1329', '200', '5'); -- martial abjurations
INSERT INTO `bcnm_loot` VALUES ('76', '1330', '200', '5');
INSERT INTO `bcnm_loot` VALUES ('76', '1331', '200', '5');
INSERT INTO `bcnm_loot` VALUES ('76', '1332', '200', '5');
INSERT INTO `bcnm_loot` VALUES ('76', '1333', '200', '5');
INSERT INTO `bcnm_loot` VALUES ('76', '14568', '200', '6'); -- askar set
INSERT INTO `bcnm_loot` VALUES ('76', '14983', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('76', '15647', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('76', '15733', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('76', '16106', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('76', '1525', '1000', '7');
INSERT INTO `bcnm_loot` VALUES ('76', '14568', '200', '8'); -- askar set
INSERT INTO `bcnm_loot` VALUES ('76', '14983', '200', '8');
INSERT INTO `bcnm_loot` VALUES ('76', '15647', '200', '8');
INSERT INTO `bcnm_loot` VALUES ('76', '15733', '200', '8');
INSERT INTO `bcnm_loot` VALUES ('76', '16106', '200', '8');
INSERT INTO `bcnm_loot` VALUES ('76', '3343', '750', '9'); -- adamantoise force pop
INSERT INTO `bcnm_loot` VALUES ('76', '3344', '250', '9');
INSERT INTO `bcnm_loot` VALUES ('105', '4247', '1000', '0'); -- charming trio
INSERT INTO `bcnm_loot` VALUES ('105', '4247', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('105', '4249', '750', '2');
INSERT INTO `bcnm_loot` VALUES ('105', '15793', '250', '2');
INSERT INTO `bcnm_loot` VALUES ('105', '5736', '1000', '3');  --  LCP
INSERT INTO `bcnm_loot` VALUES ('105', '1450', '333', '4'); --  one type of hundo
INSERT INTO `bcnm_loot` VALUES ('105', '1453', '334', '4');
INSERT INTO `bcnm_loot` VALUES ('105', '1456', '333', '4');
INSERT INTO `bcnm_loot` VALUES ('105', '2573', '005', '5');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('105', '2574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2586', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2587', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2591', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2594', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2596', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2597', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2598', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2599', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2600', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2601', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16085', '005', '5');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('105', '16089', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16093', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16097', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16101', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16086', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16090', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16094', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16098', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16087', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16091', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16095', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16099', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16103', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14547', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14551', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14555', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14548', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14556', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14549', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14553', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14557', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14561', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14565', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14962', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14966', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14970', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14974', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14978', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14963', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14967', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14971', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14975', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14979', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14964', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14968', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14972', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14976', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14980', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15626', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15630', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15634', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15638', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15642', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15627', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15631', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15635', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15639', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15643', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15714', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15722', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15726', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15730', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15712', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15716', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15724', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15728', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15713', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15717', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15721', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15725', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15729', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15628', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15632', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15636', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15640', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15644', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1418', '005', '5');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('105', '1419', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1420', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1421', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1422', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1423', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1424', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '3339', '005', '5');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('105', '3340', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '3341', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '3342', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '3343', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '3344', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '4247', '025', '5'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('105', '18019', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15793', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18717', '005', '5'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('105', '17741', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18850', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18943', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '16602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18491', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '14469', '005', '5'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('105', '14468', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '15223', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '17813', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18507', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18018', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18141', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '17653', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '17744', '005', '5');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('105', '18592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '17742', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18754', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18034', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18120', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18003', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '17956', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '19102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18944', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18443', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18492', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18851', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18426', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '17743', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18753', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '18719', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1556', '005', '5');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('105', '1557', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1558', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1561', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1562', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1565', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1566', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1567', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1568', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1569', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1570', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1821', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1571', '005', '5');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('105', '1572', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1573', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1822', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1551', '005', '5'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('105', '1552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1131', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1177', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1130', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1180', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1175', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1178', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '1553', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('105', '2573', '005', '6');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('105', '2574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2586', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2587', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2591', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2594', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2596', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2597', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2598', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2599', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2600', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2601', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '2602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16085', '005', '6');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('105', '16089', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16093', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16097', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16101', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16086', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16090', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16094', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16098', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16087', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16091', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16095', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16099', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16103', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14547', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14551', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14555', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14548', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14556', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14549', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14553', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14557', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14561', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14565', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14962', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14966', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14970', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14974', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14978', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14963', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14967', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14971', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14975', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14979', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14964', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14968', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14972', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14976', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14980', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15626', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15630', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15634', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15638', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15642', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15627', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15631', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15635', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15639', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15643', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15714', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15722', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15726', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15730', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15712', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15716', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15724', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15728', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15713', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15717', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15721', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15725', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15729', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15628', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15632', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15636', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15640', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15644', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1418', '005', '6');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('105', '1419', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1420', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1421', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1422', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1423', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1424', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '3339', '005', '6');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('105', '3340', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '3341', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '3342', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '3343', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '3344', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '4247', '025', '6'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('105', '18019', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15793', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18717', '005', '6'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('105', '17741', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18850', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18943', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '16602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18491', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '14469', '005', '6'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('105', '14468', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '15223', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '17813', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18507', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18018', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18141', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '17653', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '17744', '005', '6');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('105', '18592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '17742', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18754', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18034', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18120', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18003', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '17956', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '19102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18944', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18443', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18492', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18851', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18426', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '17743', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18753', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '18719', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1556', '005', '6');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('105', '1557', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1558', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1561', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1562', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1565', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1566', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1567', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1568', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1569', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1570', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1821', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1571', '005', '6');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('105', '1572', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1573', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1822', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1551', '005', '6'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('105', '1552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1131', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1177', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1130', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1180', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1175', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1178', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('105', '1553', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14933', '333', '0'); -- operation desert swarm amir hands
INSERT INTO `bcnm_loot` VALUES ('81', '14935', '333', '0'); -- yigit hands
INSERT INTO `bcnm_loot` VALUES ('81', '14940', '334', '0'); -- pahluwan hands
INSERT INTO `bcnm_loot` VALUES ('81', '1456', '1000', '1'); -- byne x2
INSERT INTO `bcnm_loot` VALUES ('81', '1456', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('81', '5736', '1000', '3'); -- LCP x3
INSERT INTO `bcnm_loot` VALUES ('81', '5736', '1000', '4'); 
INSERT INTO `bcnm_loot` VALUES ('81', '5736', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('81', '3339', '142', '6');  --  ENM pops
INSERT INTO `bcnm_loot` VALUES ('81', '3340', '142', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3341', '142', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3342', '142', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3343', '142', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3344', '142', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1823', '148', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2573', '005', '6');  --  items 2573-2602 are ZNM pop items
INSERT INTO `bcnm_loot` VALUES ('81', '2574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2586', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2587', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2591', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2594', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2596', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2597', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2598', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2599', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2600', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2601', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16085', '005', '6');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('81', '16089', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16093', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16097', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16101', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16086', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16090', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16094', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16098', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16087', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16091', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16095', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16099', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16103', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14547', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14551', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14555', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14548', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14556', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14549', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14553', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14557', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14561', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14565', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14962', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14966', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14970', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14974', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14978', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14963', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14967', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14971', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14975', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14979', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14964', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14968', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14972', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14976', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14980', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15626', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15630', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15634', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15638', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15642', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15627', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15631', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15635', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15639', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15643', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15714', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15722', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15726', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15730', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15712', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15716', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15724', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15728', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15713', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15717', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15721', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15725', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15729', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15628', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15632', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15636', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15640', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15644', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1418', '005', '6');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('81', '1419', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1420', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1421', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1422', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1423', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1424', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3339', '005', '6');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('81', '3340', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3341', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3342', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3343', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '3344', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '4247', '025', '6'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('81', '18019', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15793', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18717', '005', '6'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('81', '17741', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18850', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18943', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '16602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18491', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '14469', '005', '6'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('81', '14468', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '15223', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '17813', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18507', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18018', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18141', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '17653', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '17744', '005', '6');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('81', '18592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '17742', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18754', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18034', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18120', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18003', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '17956', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '19102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18944', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18443', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18492', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18851', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18426', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '17743', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18753', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '18719', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1556', '005', '6');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('81', '1557', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1558', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1561', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1562', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1565', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1566', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1567', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1568', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1569', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1570', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1821', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1571', '005', '6');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('81', '1572', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1573', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1822', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1551', '005', '6'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('81', '1552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1131', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1177', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1130', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1180', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1175', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1178', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '1553', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('81', '2573', '005', '7');  --  items 2573-2602 are ZNM pop items
INSERT INTO `bcnm_loot` VALUES ('81', '2574', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2575', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2576', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2577', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2578', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2579', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2580', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2581', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2582', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2583', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2584', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2585', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2586', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2587', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2588', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2589', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2590', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2591', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2592', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2593', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2593', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2594', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2596', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2597', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2598', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2599', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2600', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2601', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '2602', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16085', '005', '7');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('81', '16089', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16093', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16097', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16101', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16086', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16090', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16094', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16098', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16102', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16087', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16091', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16095', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16099', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16103', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14547', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14551', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14555', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14559', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14563', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14548', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14552', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14556', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14560', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14564', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14549', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14553', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14557', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14561', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14565', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14962', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14966', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14970', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14974', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14978', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14963', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14967', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14971', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14975', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14979', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14964', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14968', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14972', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14976', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14980', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15626', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15630', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15634', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15638', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15642', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15627', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15631', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15635', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15639', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15643', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15714', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15718', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15722', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15726', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15730', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15712', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15716', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15720', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15724', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15728', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15713', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15717', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15721', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15725', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15729', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15628', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15632', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15636', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15640', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15644', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1418', '005', '7');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('81', '1419', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1420', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1421', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1422', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1423', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1424', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1425', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '3339', '005', '7');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('81', '3340', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '3341', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '3342', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '3343', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '3344', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '4247', '025', '7'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('81', '18019', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15793', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18717', '005', '7'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('81', '17741', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18850', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18943', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18425', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18718', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18588', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '16602', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18491', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '14469', '005', '7'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('81', '14468', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '15223', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '17813', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18507', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18018', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18141', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '17653', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '17744', '005', '7');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('81', '18592', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18720', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '17742', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18754', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18034', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18120', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18003', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '17956', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18590', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '19102', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18589', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18944', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18443', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18492', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18851', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18426', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '17743', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18753', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18719', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1556', '005', '7');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('81', '1557', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1558', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1559', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1560', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1561', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1562', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1563', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1564', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1565', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1566', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1567', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1568', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1569', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1570', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1821', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1571', '005', '7');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('81', '1572', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1573', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1574', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1575', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1576', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1577', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1578', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1579', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1580', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1581', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1582', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1583', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1584', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1585', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1822', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1551', '005', '7'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('81', '1552', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1131', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1177', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1130', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1180', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1175', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1178', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '1553', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('81', '18717', '111', '8');  --  perdu weapons
INSERT INTO `bcnm_loot` VALUES ('81', '17741', '111', '8');
INSERT INTO `bcnm_loot` VALUES ('81', '18850', '111', '8');
INSERT INTO `bcnm_loot` VALUES ('81', '18943', '111', '8');
INSERT INTO `bcnm_loot` VALUES ('81', '18425', '111', '8');
INSERT INTO `bcnm_loot` VALUES ('81', '18718', '111', '8');
INSERT INTO `bcnm_loot` VALUES ('81', '18588', '111', '8');
INSERT INTO `bcnm_loot` VALUES ('81', '16602', '111', '8');
INSERT INTO `bcnm_loot` VALUES ('81', '18491', '112', '8');
INSERT INTO `bcnm_loot` VALUES ('81', '3339', '142', '9');  --  ENM pops
INSERT INTO `bcnm_loot` VALUES ('81', '3340', '142', '9');
INSERT INTO `bcnm_loot` VALUES ('81', '3341', '142', '9');
INSERT INTO `bcnm_loot` VALUES ('81', '3342', '142', '9');
INSERT INTO `bcnm_loot` VALUES ('81', '3343', '142', '9');
INSERT INTO `bcnm_loot` VALUES ('81', '3344', '142', '9');
INSERT INTO `bcnm_loot` VALUES ('81', '1823', '148', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15604', '333', '0'); -- prehistoric pigeons amir legs
INSERT INTO `bcnm_loot` VALUES ('82', '15606', '333', '0'); -- yigit legs
INSERT INTO `bcnm_loot` VALUES ('82', '15609', '334', '0'); -- pahluwan legs
INSERT INTO `bcnm_loot` VALUES ('82', '5736', '1000', '1'); -- LCP
INSERT INTO `bcnm_loot` VALUES ('82', '5736', '1000', '2'); -- LCP
INSERT INTO `bcnm_loot` VALUES ('82', '5736', '1000', '3'); -- LCP
INSERT INTO `bcnm_loot` VALUES ('82', '3339', '142', '4');  --  ENM pops
INSERT INTO `bcnm_loot` VALUES ('82', '3340', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('82', '3341', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('82', '3342', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('82', '3343', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('82', '3344', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('82', '1823', '148', '4');
INSERT INTO `bcnm_loot` VALUES ('82', '1456', '1000', '5'); -- byne x2
INSERT INTO `bcnm_loot` VALUES ('82', '1456', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('82', '18717', '111', '7');  --  perdu weapons
INSERT INTO `bcnm_loot` VALUES ('82', '17741', '111', '7');
INSERT INTO `bcnm_loot` VALUES ('82', '18850', '111', '7');
INSERT INTO `bcnm_loot` VALUES ('82', '18943', '111', '7');
INSERT INTO `bcnm_loot` VALUES ('82', '18425', '111', '7');
INSERT INTO `bcnm_loot` VALUES ('82', '18718', '111', '7');
INSERT INTO `bcnm_loot` VALUES ('82', '18588', '111', '7');
INSERT INTO `bcnm_loot` VALUES ('82', '16602', '111', '7');
INSERT INTO `bcnm_loot` VALUES ('82', '18491', '112', '7');
INSERT INTO `bcnm_loot` VALUES ('82', '2573', '005', '8');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('82', '2574', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2575', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2576', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2577', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2578', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2579', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2580', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2581', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2582', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2583', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2584', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2585', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2586', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2587', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2588', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2589', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2590', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2591', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2592', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2593', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2593', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2594', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2596', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2597', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2598', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2599', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2600', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2601', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2602', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16085', '005', '8');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('82', '16089', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16093', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16097', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16101', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16086', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16090', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16094', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16098', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16102', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16087', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16091', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16095', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16099', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16103', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14547', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14551', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14555', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14559', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14563', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14548', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14552', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14556', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14560', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14564', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14549', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14553', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14557', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14561', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14565', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14962', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14966', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14970', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14974', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14978', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14963', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14967', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14971', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14975', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14979', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14964', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14968', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14972', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14976', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14980', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15626', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15630', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15634', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15638', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15642', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15627', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15631', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15635', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15639', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15643', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15714', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15718', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15722', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15726', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15730', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15712', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15716', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15720', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15724', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15728', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15713', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15717', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15721', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15725', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15729', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15628', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15632', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15636', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15640', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15644', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1418', '005', '8');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('82', '1419', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1420', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1421', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1422', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1423', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1424', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1425', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '3339', '005', '8');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('82', '3340', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '3341', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '3342', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '3343', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '3344', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '4247', '025', '8'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('82', '18019', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15793', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18717', '005', '8'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('82', '17741', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18850', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18943', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18425', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18718', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18588', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '16602', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18491', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '14469', '005', '8'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('82', '14468', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '15223', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '17813', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18507', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18018', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18141', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '17653', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '17744', '005', '8');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('82', '18592', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18720', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '17742', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18754', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18034', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18120', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18003', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '17956', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18590', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '19102', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18589', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18944', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18443', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18492', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18851', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18426', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '17743', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18753', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '18719', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1556', '005', '8');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('82', '1557', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1558', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1559', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1560', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1561', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1562', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1563', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1564', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1565', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1566', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1567', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1568', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1569', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1570', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1821', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1571', '005', '8');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('82', '1572', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1573', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1574', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1575', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1576', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1577', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1578', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1579', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1580', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1581', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1582', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1583', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1584', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1585', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1822', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1551', '005', '8'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('82', '1552', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1131', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1177', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1130', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1180', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1175', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1178', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '1553', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('82', '2573', '005', '9');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('82', '2574', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2575', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2576', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2577', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2578', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2579', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2580', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2581', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2582', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2583', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2584', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2585', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2586', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2587', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2588', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2589', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2590', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2591', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2592', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2593', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2593', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2594', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2596', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2597', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2598', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2599', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2600', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2601', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '2602', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16085', '005', '9');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('82', '16089', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16093', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16097', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16101', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16086', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16090', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16094', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16098', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16102', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16087', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16091', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16095', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16099', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16103', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14547', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14551', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14555', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14559', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14563', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14548', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14552', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14556', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14560', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14564', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14549', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14553', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14557', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14561', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14565', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14962', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14966', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14970', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14974', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14978', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14963', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14967', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14971', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14975', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14979', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14964', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14968', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14972', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14976', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14980', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15626', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15630', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15634', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15638', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15642', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15627', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15631', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15635', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15639', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15643', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15714', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15718', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15722', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15726', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15730', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15712', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15716', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15720', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15724', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15728', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15713', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15717', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15721', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15725', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15729', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15628', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15632', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15636', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15640', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15644', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1418', '005', '9');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('82', '1419', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1420', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1421', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1422', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1423', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1424', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1425', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '3339', '005', '9');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('82', '3340', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '3341', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '3342', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '3343', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '3344', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '4247', '025', '9'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('82', '18019', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15793', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18717', '005', '9'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('82', '17741', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18850', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18943', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18425', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18718', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18588', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '16602', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18491', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '14469', '005', '9'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('82', '14468', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '15223', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '17813', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18507', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18018', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18141', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '17653', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '17744', '005', '9');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('82', '18592', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18720', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '17742', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18754', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18034', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18120', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18003', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '17956', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18590', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '19102', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18589', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18944', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18443', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18492', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18851', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18426', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '17743', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18753', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '18719', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1556', '005', '9');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('82', '1557', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1558', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1559', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1560', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1561', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1562', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1563', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1564', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1565', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1566', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1567', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1568', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1569', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1570', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1821', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1571', '005', '9');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('82', '1572', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1573', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1574', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1575', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1576', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1577', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1578', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1579', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1580', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1581', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1582', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1583', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1584', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1585', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1822', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1551', '005', '9'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('82', '1552', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1131', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1177', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1130', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1180', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1175', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1178', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('82', '1553', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('34', '4247', '1000', '0'); -- wings of fury
INSERT INTO `bcnm_loot` VALUES ('34', '4247', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('34', '4249', '750', '2');
INSERT INTO `bcnm_loot` VALUES ('34', '15793', '250', '2');
INSERT INTO `bcnm_loot` VALUES ('34', '1450', '333', '3');  --  hundo
INSERT INTO `bcnm_loot` VALUES ('34', '1453', '333', '3');
INSERT INTO `bcnm_loot` VALUES ('34', '1456', '334', '3');
INSERT INTO `bcnm_loot` VALUES ('34', '5736', '1000', '4');  --  LCP
INSERT INTO `bcnm_loot` VALUES ('34', '2573', '005', '5');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('34', '2574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2586', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2587', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2591', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2594', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2596', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2597', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2598', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2599', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2600', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2601', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16085', '005', '5');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('34', '16089', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16093', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16097', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16101', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16086', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16090', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16094', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16098', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16087', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16091', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16095', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16099', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16103', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14547', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14551', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14555', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14548', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14556', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14549', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14553', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14557', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14561', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14565', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14962', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14966', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14970', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14974', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14978', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14963', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14967', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14971', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14975', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14979', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14964', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14968', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14972', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14976', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14980', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15626', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15630', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15634', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15638', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15642', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15627', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15631', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15635', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15639', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15643', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15714', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15722', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15726', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15730', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15712', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15716', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15724', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15728', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15713', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15717', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15721', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15725', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15729', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15628', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15632', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15636', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15640', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15644', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1418', '005', '5');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('34', '1419', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1420', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1421', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1422', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1423', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1424', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '3339', '005', '5');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('34', '3340', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '3341', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '3342', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '3343', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '3344', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '4247', '025', '5'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('34', '18019', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15793', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18717', '005', '5'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('34', '17741', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18850', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18943', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '16602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18491', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '14469', '005', '5'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('34', '14468', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '15223', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '17813', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18507', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18018', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18141', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '17653', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '17744', '005', '5');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('34', '18592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '17742', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18754', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18034', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18120', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18003', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '17956', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '19102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18944', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18443', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18492', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18851', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18426', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '17743', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18753', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '18719', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1556', '005', '5');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('34', '1557', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1558', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1561', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1562', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1565', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1566', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1567', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1568', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1569', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1570', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1821', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1571', '005', '5');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('34', '1572', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1573', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1822', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1551', '005', '5'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('34', '1552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1131', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1177', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1130', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1180', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1175', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1178', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '1553', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('34', '2573', '005', '6');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('34', '2574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2586', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2587', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2591', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2594', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2596', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2597', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2598', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2599', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2600', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2601', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '2602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16085', '005', '6');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('34', '16089', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16093', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16097', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16101', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16086', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16090', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16094', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16098', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16087', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16091', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16095', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16099', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16103', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14547', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14551', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14555', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14548', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14556', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14549', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14553', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14557', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14561', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14565', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14962', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14966', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14970', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14974', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14978', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14963', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14967', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14971', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14975', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14979', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14964', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14968', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14972', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14976', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14980', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15626', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15630', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15634', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15638', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15642', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15627', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15631', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15635', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15639', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15643', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15714', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15722', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15726', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15730', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15712', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15716', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15724', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15728', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15713', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15717', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15721', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15725', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15729', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15628', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15632', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15636', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15640', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15644', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1418', '005', '6');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('34', '1419', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1420', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1421', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1422', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1423', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1424', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '3339', '005', '6');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('34', '3340', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '3341', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '3342', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '3343', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '3344', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '4247', '025', '6'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('34', '18019', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15793', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18717', '005', '6'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('34', '17741', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18850', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18943', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '16602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18491', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '14469', '005', '6'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('34', '14468', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '15223', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '17813', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18507', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18018', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18141', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '17653', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '17744', '005', '6');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('34', '18592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '17742', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18754', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18034', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18120', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18003', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '17956', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '19102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18944', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18443', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18492', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18851', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18426', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '17743', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18753', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '18719', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1556', '005', '6');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('34', '1557', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1558', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1561', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1562', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1565', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1566', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1567', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1568', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1569', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1570', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1821', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1571', '005', '6');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('34', '1572', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1573', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1822', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1551', '005', '6'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('34', '1552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1131', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1177', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1130', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1180', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1175', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1178', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('34', '1553', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('107', '3339', '750', '0'); -- early bird catches the wyrm
INSERT INTO `bcnm_loot` VALUES ('107', '3340', '250', '0');
INSERT INTO `bcnm_loot` VALUES ('107', '16555', '200', '1'); -- ridill
INSERT INTO `bcnm_loot` VALUES ('107', '17694', '800', '1');
INSERT INTO `bcnm_loot` VALUES ('107', '17440', '200', '2'); -- kclub
INSERT INTO `bcnm_loot` VALUES ('107', '1456', '800', '2');
INSERT INTO `bcnm_loot` VALUES ('107', '1319', '200', '3'); -- earthen abj sets x2
INSERT INTO `bcnm_loot` VALUES ('107', '1320', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('107', '1321', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('107', '1322', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('107', '1323', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('107', '1319', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('107', '1320', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('107', '1321', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('107', '1322', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('107', '1323', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('107', '1130', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('107', '14570', '200', '6'); -- goliard set
INSERT INTO `bcnm_loot` VALUES ('107', '14985', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('107', '15649', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('107', '15735', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('107', '16108', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('107', '4135', '290', '7');
INSERT INTO `bcnm_loot` VALUES ('107', '4119', '225', '7');
INSERT INTO `bcnm_loot` VALUES ('107', '4173', '210', '7');
INSERT INTO `bcnm_loot` VALUES ('107', '4175', '217', '7');
INSERT INTO `bcnm_loot` VALUES ('107', '14570', '200', '8'); -- goliard set
INSERT INTO `bcnm_loot` VALUES ('107', '14985', '200', '8');
INSERT INTO `bcnm_loot` VALUES ('107', '15649', '200', '8');
INSERT INTO `bcnm_loot` VALUES ('107', '15735', '200', '8');
INSERT INTO `bcnm_loot` VALUES ('107', '16108', '200', '8');
INSERT INTO `bcnm_loot` VALUES ('107', '1526', '1000', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '17744', '025', '0'); -- jungle boogeymen vigil weapons
INSERT INTO `bcnm_loot` VALUES ('10', '18592', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18720', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '17742', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18754', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18034', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18120', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18003', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '17956', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18590', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '19102', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18589', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18944', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18443', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18492', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18851', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18426', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '17743', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18753', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '18719', '025', '0');
INSERT INTO `bcnm_loot` VALUES ('10', '5736', '500', '0');  --  LCP 50% lol
INSERT INTO `bcnm_loot` VALUES ('10', '5736', '1000', '1'); -- LCP x2 100%
INSERT INTO `bcnm_loot` VALUES ('10', '5736', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('10', '2573', '005', '3');  --  items 2573-2602 are ZNM pop items GRAB BAG 1
INSERT INTO `bcnm_loot` VALUES ('10', '2574', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2575', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2576', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2577', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2578', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2579', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2580', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2581', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2582', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2583', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2584', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2585', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2586', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2587', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2588', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2589', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2590', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2591', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2592', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2593', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2593', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2594', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2596', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2597', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2598', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2599', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2600', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2601', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '2602', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16085', '005', '3');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('10', '16089', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16093', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16097', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16101', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16086', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16090', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16094', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16098', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16102', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16087', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16091', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16095', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16099', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16103', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14547', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14551', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14555', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14559', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14563', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14548', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14552', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14556', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14560', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14564', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14549', '001', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14553', '001', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14557', '001', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14561', '001', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14565', '001', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14962', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14966', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14970', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14974', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14978', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14963', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14967', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14971', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14975', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14979', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14964', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14968', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14972', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14976', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14980', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15626', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15630', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15634', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15638', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15642', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15627', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15631', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15635', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15639', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15643', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15714', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15718', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15722', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15726', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15730', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15712', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15716', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15720', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15724', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15728', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15713', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15717', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15721', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15725', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15729', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15628', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15632', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15636', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15640', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15644', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1418', '005', '3');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('10', '1419', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1420', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1421', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1422', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1423', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1424', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1425', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '3339', '005', '3');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('10', '3340', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '3341', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '3342', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '3343', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '3344', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '4247', '025', '3'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('10', '18019', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15793', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18717', '005', '3'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('10', '17741', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18850', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18943', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18425', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18718', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18588', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '16602', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18491', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14469', '005', '3'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('10', '14468', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '15223', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '17813', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18507', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18018', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18141', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '17653', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '17744', '005', '3');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('10', '18592', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18720', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '17742', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18754', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18034', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18120', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18003', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '17956', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18590', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '19102', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18589', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18944', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18443', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18492', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18851', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18426', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '17743', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18753', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '18719', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1556', '005', '3');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('10', '1557', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1558', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1559', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1560', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1561', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1562', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1563', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1564', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1565', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1566', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1567', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1568', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1569', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1570', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1821', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1571', '005', '3');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('10', '1572', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1573', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1574', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1575', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1576', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1577', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1578', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1579', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1580', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1581', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1582', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1583', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1584', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1585', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1822', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1551', '005', '3'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('10', '1552', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1131', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1177', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1130', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1180', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1175', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1178', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '1553', '005', '3');
INSERT INTO `bcnm_loot` VALUES ('10', '14525', '333', '4'); -- amir body
INSERT INTO `bcnm_loot` VALUES ('10', '14527', '333', '4'); -- yigit body
INSERT INTO `bcnm_loot` VALUES ('10', '14530', '334', '4'); -- pahluwan body
INSERT INTO `bcnm_loot` VALUES ('10', '1450', '1000', '5'); -- hundos
INSERT INTO `bcnm_loot` VALUES ('10', '1453', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('10', '1456', '1000', '7');
INSERT INTO `bcnm_loot` VALUES ('10', '3343', '1000', '8'); -- blue pondweed
INSERT INTO `bcnm_loot` VALUES ('10', '2573', '005', '9');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('10', '2574', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2575', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2576', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2577', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2578', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2579', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2580', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2581', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2582', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2583', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2584', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2585', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2586', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2587', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2588', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2589', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2590', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2591', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2592', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2593', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2593', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2594', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2596', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2597', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2598', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2599', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2600', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2601', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '2602', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16085', '005', '9');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('10', '16089', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16093', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16097', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16101', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16086', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16090', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16094', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16098', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16102', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16087', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16091', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16095', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16099', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16103', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14547', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14551', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14555', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14559', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14563', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14548', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14552', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14556', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14560', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14564', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14549', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14553', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14557', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14561', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14565', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14962', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14966', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14970', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14974', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14978', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14963', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14967', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14971', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14975', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14979', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14964', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14968', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14972', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14976', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14980', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15626', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15630', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15634', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15638', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15642', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15627', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15631', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15635', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15639', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15643', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15714', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15718', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15722', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15726', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15730', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15712', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15716', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15720', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15724', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15728', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15713', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15717', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15721', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15725', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15729', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15628', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15632', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15636', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15640', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15644', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1418', '005', '9');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('10', '1419', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1420', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1421', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1422', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1423', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1424', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1425', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '3339', '005', '9');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('10', '3340', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '3341', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '3342', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '3343', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '3344', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '4247', '025', '9'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('10', '18019', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15793', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18717', '005', '9'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('10', '17741', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18850', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18943', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18425', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18718', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18588', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '16602', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18491', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '14469', '005', '9'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('10', '14468', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '15223', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '17813', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18507', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18018', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18141', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '17653', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '17744', '005', '9');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('10', '18592', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18720', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '17742', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18754', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18034', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18120', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18003', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '17956', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18590', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '19102', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18589', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18944', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18443', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18492', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18851', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18426', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '17743', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18753', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '18719', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1556', '005', '9');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('10', '1557', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1558', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1559', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1560', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1561', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1562', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1563', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1564', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1565', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1566', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1567', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1568', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1569', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1570', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1821', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1571', '005', '9');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('10', '1572', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1573', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1574', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1575', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1576', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1577', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1578', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1579', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1580', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1581', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1582', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1583', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1584', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1585', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1822', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1551', '005', '9'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('10', '1552', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1131', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1177', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1130', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1180', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1175', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1178', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('10', '1553', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('11', '3341', '750', '0'); -- horns of war
INSERT INTO `bcnm_loot` VALUES ('11', '3342', '250', '0'); -- behemoth force pops
INSERT INTO `bcnm_loot` VALUES ('11', '1442', '800', '1');
INSERT INTO `bcnm_loot` VALUES ('11', '14577', '200', '1'); -- valhalla breastplate
INSERT INTO `bcnm_loot` VALUES ('11', '860', '800', '2');
INSERT INTO `bcnm_loot` VALUES ('11', '13566', '200', '2'); -- defending ring
INSERT INTO `bcnm_loot` VALUES ('11', '1314', '200', '3'); -- dryadic abjurations
INSERT INTO `bcnm_loot` VALUES ('11', '1315', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('11', '1316', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('11', '1317', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('11', '1318', '200', '3');
INSERT INTO `bcnm_loot` VALUES ('11', '1314', '200', '4'); -- dryadic abjurations
INSERT INTO `bcnm_loot` VALUES ('11', '1315', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('11', '1316', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('11', '1317', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('11', '1318', '200', '4');
INSERT INTO `bcnm_loot` VALUES ('11', '1334', '200', '5'); -- wyrmal abjurations
INSERT INTO `bcnm_loot` VALUES ('11', '1335', '200', '5');
INSERT INTO `bcnm_loot` VALUES ('11', '1336', '200', '5');
INSERT INTO `bcnm_loot` VALUES ('11', '1337', '200', '5');
INSERT INTO `bcnm_loot` VALUES ('11', '1338', '200', '5');
INSERT INTO `bcnm_loot` VALUES ('11', '1334', '200', '6'); -- wyrmal abjurations
INSERT INTO `bcnm_loot` VALUES ('11', '1335', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('11', '1336', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('11', '1337', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('11', '1338', '200', '6');
INSERT INTO `bcnm_loot` VALUES ('11', '14569', '200', '7'); -- denali set
INSERT INTO `bcnm_loot` VALUES ('11', '14984', '200', '7');
INSERT INTO `bcnm_loot` VALUES ('11', '15648', '200', '7');
INSERT INTO `bcnm_loot` VALUES ('11', '15734', '200', '7');
INSERT INTO `bcnm_loot` VALUES ('11', '16107', '200', '7');
INSERT INTO `bcnm_loot` VALUES ('11', '1527', '1000', '8');
INSERT INTO `bcnm_loot` VALUES ('11', '14569', '200', '9'); -- denali set
INSERT INTO `bcnm_loot` VALUES ('11', '14984', '200', '9');
INSERT INTO `bcnm_loot` VALUES ('11', '15648', '200', '9');
INSERT INTO `bcnm_loot` VALUES ('11', '15734', '200', '9');
INSERT INTO `bcnm_loot` VALUES ('11', '16107', '200', '9');
INSERT INTO `bcnm_loot` VALUES ('12', '1450', '333', '0'); --  dropping like flies hundo 1
INSERT INTO `bcnm_loot` VALUES ('12', '1453', '333', '0');
INSERT INTO `bcnm_loot` VALUES ('12', '1456', '334', '0');
INSERT INTO `bcnm_loot` VALUES ('12', '1450', '333', '1'); --  hundo 2
INSERT INTO `bcnm_loot` VALUES ('12', '1453', '333', '1');
INSERT INTO `bcnm_loot` VALUES ('12', '1456', '334', '1');
INSERT INTO `bcnm_loot` VALUES ('12', '5736', '1000', '2'); -- LCP x2
INSERT INTO `bcnm_loot` VALUES ('12', '5736', '1000', '3'); 
INSERT INTO `bcnm_loot` VALUES ('12', '1418', '125', '4');  --  sky pop item
INSERT INTO `bcnm_loot` VALUES ('12', '1419', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('12', '1420', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('12', '1421', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('12', '1422', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('12', '1423', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('12', '1424', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('12', '1425', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('12', '2573', '005', '5');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('12', '2574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2586', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2587', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2591', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2594', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2596', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2597', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2598', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2599', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2600', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2601', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16085', '005', '5');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('12', '16089', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16093', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16097', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16101', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16086', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16090', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16094', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16098', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16087', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16091', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16095', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16099', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16103', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14547', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14551', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14555', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14548', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14556', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14549', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14553', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14557', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14561', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14565', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14962', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14966', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14970', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14974', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14978', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14963', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14967', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14971', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14975', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14979', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14964', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14968', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14972', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14976', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14980', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15626', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15630', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15634', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15638', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15642', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15627', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15631', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15635', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15639', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15643', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15714', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15722', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15726', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15730', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15712', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15716', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15724', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15728', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15713', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15717', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15721', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15725', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15729', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15628', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15632', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15636', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15640', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15644', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1418', '005', '5');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('12', '1419', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1420', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1421', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1422', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1423', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1424', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '3339', '005', '5');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('12', '3340', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '3341', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '3342', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '3343', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '3344', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '4247', '025', '5'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('12', '18019', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15793', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18717', '005', '5'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('12', '17741', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18850', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18943', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '16602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18491', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '14469', '005', '5'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('12', '14468', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '15223', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '17813', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18507', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18018', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18141', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '17653', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '17744', '005', '5');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('12', '18592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '17742', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18754', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18034', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18120', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18003', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '17956', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '19102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18944', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18443', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18492', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18851', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18426', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '17743', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18753', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '18719', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1556', '005', '5');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('12', '1557', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1558', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1561', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1562', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1565', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1566', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1567', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1568', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1569', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1570', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1821', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1571', '005', '5');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('12', '1572', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1573', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1822', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1551', '005', '5'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('12', '1552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1131', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1177', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1130', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1180', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1175', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1178', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '1553', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('12', '2573', '005', '6');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('12', '2574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2586', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2587', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2591', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2594', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2596', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2597', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2598', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2599', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2600', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2601', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '2602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16085', '005', '6');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('12', '16089', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16093', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16097', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16101', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16086', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16090', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16094', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16098', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16087', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16091', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16095', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16099', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16103', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14547', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14551', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14555', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14548', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14556', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14549', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14553', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14557', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14561', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14565', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14962', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14966', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14970', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14974', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14978', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14963', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14967', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14971', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14975', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14979', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14964', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14968', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14972', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14976', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14980', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15626', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15630', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15634', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15638', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15642', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15627', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15631', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15635', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15639', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15643', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15714', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15722', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15726', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15730', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15712', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15716', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15724', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15728', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15713', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15717', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15721', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15725', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15729', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15628', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15632', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15636', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15640', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15644', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1418', '005', '6');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('12', '1419', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1420', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1421', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1422', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1423', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1424', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '3339', '005', '6');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('12', '3340', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '3341', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '3342', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '3343', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '3344', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '4247', '025', '6'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('12', '18019', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15793', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18717', '005', '6'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('12', '17741', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18850', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18943', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '16602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18491', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '14469', '005', '6'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('12', '14468', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '15223', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '17813', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18507', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18018', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18141', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '17653', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '17744', '005', '6');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('12', '18592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '17742', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18754', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18034', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18120', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18003', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '17956', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '19102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18944', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18443', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18492', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18851', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18426', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '17743', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18753', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '18719', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1556', '005', '6');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('12', '1557', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1558', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1561', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1562', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1565', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1566', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1567', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1568', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1569', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1570', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1821', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1571', '005', '6');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('12', '1572', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1573', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1822', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1551', '005', '6'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('12', '1552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1131', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1177', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1130', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1180', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1175', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1178', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('12', '1553', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1450', '1000', '0'); --   hostile herbivores hundos x3
INSERT INTO `bcnm_loot` VALUES ('14', '1453', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('14', '1456', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('14', '5736', '1000', '3'); --  LCP x3
INSERT INTO `bcnm_loot` VALUES ('14', '5736', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('14', '5736', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('14', '2573', '005', '6');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('14', '2574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2586', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2587', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2591', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2594', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2596', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2597', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2598', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2599', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2600', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2601', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16085', '005', '6');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('14', '16089', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16093', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16097', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16101', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16086', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16090', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16094', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16098', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16087', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16091', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16095', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16099', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16103', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14547', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14551', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14555', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14548', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14556', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14549', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14553', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14557', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14561', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14565', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14962', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14966', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14970', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14974', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14978', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14963', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14967', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14971', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14975', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14979', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14964', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14968', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14972', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14976', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14980', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15626', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15630', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15634', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15638', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15642', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15627', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15631', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15635', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15639', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15643', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15714', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15722', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15726', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15730', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15712', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15716', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15724', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15728', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15713', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15717', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15721', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15725', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15729', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15628', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15632', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15636', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15640', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15644', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1418', '005', '6');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('14', '1419', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1420', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1421', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1422', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1423', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1424', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '3339', '005', '6');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('14', '3340', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '3341', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '3342', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '3343', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '3344', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '4247', '025', '6'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('14', '18019', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15793', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18717', '005', '6'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('14', '17741', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18850', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18943', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '16602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18491', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '14469', '005', '6'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('14', '14468', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '15223', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '17813', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18507', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18018', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18141', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '17653', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '17744', '005', '6');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('14', '18592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '17742', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18754', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18034', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18120', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18003', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '17956', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '19102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18944', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18443', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18492', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18851', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18426', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '17743', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18753', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '18719', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1556', '005', '6');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('14', '1557', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1558', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1561', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1562', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1565', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1566', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1567', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1568', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1569', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1570', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1821', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1571', '005', '6');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('14', '1572', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1573', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1822', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1551', '005', '6'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('14', '1552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1131', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1177', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1130', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1180', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1175', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1178', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '1553', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('14', '2573', '005', '7');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('14', '2574', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2575', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2576', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2577', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2578', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2579', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2580', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2581', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2582', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2583', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2584', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2585', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2586', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2587', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2588', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2589', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2590', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2591', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2592', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2593', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2593', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2594', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2596', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2597', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2598', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2599', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2600', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2601', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '2602', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16085', '005', '7');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('14', '16089', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16093', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16097', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16101', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16086', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16090', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16094', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16098', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16102', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16087', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16091', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16095', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16099', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16103', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14547', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14551', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14555', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14559', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14563', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14548', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14552', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14556', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14560', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14564', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14549', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14553', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14557', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14561', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14565', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14962', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14966', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14970', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14974', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14978', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14963', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14967', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14971', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14975', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14979', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14964', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14968', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14972', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14976', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14980', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15626', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15630', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15634', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15638', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15642', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15627', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15631', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15635', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15639', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15643', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15714', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15718', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15722', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15726', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15730', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15712', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15716', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15720', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15724', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15728', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15713', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15717', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15721', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15725', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15729', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15628', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15632', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15636', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15640', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15644', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1418', '005', '7');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('14', '1419', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1420', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1421', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1422', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1423', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1424', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1425', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '3339', '005', '7');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('14', '3340', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '3341', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '3342', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '3343', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '3344', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '4247', '025', '7'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('14', '18019', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15793', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18717', '005', '7'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('14', '17741', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18850', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18943', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18425', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18718', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18588', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '16602', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18491', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '14469', '005', '7'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('14', '14468', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '15223', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '17813', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18507', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18018', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18141', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '17653', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '17744', '005', '7');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('14', '18592', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18720', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '17742', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18754', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18034', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18120', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18003', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '17956', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18590', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '19102', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18589', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18944', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18443', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18492', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18851', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18426', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '17743', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18753', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '18719', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1556', '005', '7');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('14', '1557', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1558', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1559', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1560', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1561', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1562', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1563', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1564', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1565', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1566', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1567', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1568', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1569', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1570', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1821', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1571', '005', '7');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('14', '1572', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1573', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1574', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1575', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1576', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1577', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1578', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1579', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1580', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1581', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1582', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1583', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1584', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1585', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1822', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1551', '005', '7'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('14', '1552', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1131', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1177', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1130', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1180', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1175', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1178', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1553', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('14', '1551', '150', '8');  --  bcnm orbs
INSERT INTO `bcnm_loot` VALUES ('14', '1552', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('14', '1131', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('14', '1177', '150', '8');
INSERT INTO `bcnm_loot` VALUES ('14', '1130', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('14', '1180', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('14', '1175', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('14', '1178', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('14', '1553', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('15', '5736', '1000', '0'); -- double dragonian LCP
INSERT INTO `bcnm_loot` VALUES ('15', '16062', '333', '1'); -- amir head
INSERT INTO `bcnm_loot` VALUES ('15', '16064', '333', '1'); -- yigit head
INSERT INTO `bcnm_loot` VALUES ('15', '16069', '334', '1'); -- pahluwan head
INSERT INTO `bcnm_loot` VALUES ('15', '5736', '1000', '2');  -- LCP
INSERT INTO `bcnm_loot` VALUES ('15', '5736', '1000', '3');  -- LCP
INSERT INTO `bcnm_loot` VALUES ('15', '3339', '142', '4');  --  ENM pops
INSERT INTO `bcnm_loot` VALUES ('15', '3340', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('15', '3341', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('15', '3342', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('15', '3343', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('15', '3344', '142', '4');
INSERT INTO `bcnm_loot` VALUES ('15', '1823', '148', '4');
INSERT INTO `bcnm_loot` VALUES ('15', '1456', '1000', '5'); -- byne
INSERT INTO `bcnm_loot` VALUES ('15', '2573', '005', '6');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('15', '2574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2586', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2587', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2591', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2594', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2596', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2597', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2598', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2599', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2600', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2601', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16085', '005', '6');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('15', '16089', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16093', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16097', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16101', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16086', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16090', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16094', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16098', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16087', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16091', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16095', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16099', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16103', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14547', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14551', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14555', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14548', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14556', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14549', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14553', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14557', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14561', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14565', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14962', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14966', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14970', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14974', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14978', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14963', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14967', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14971', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14975', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14979', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14964', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14968', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14972', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14976', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14980', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15626', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15630', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15634', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15638', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15642', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15627', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15631', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15635', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15639', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15643', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15714', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15722', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15726', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15730', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15712', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15716', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15724', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15728', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15713', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15717', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15721', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15725', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15729', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15628', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15632', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15636', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15640', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15644', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1418', '005', '6');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('15', '1419', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1420', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1421', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1422', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1423', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1424', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '3339', '005', '6');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('15', '3340', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '3341', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '3342', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '3343', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '3344', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '4247', '025', '6'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('15', '18019', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15793', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18717', '005', '6'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('15', '17741', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18850', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18943', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '16602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18491', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '14469', '005', '6'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('15', '14468', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '15223', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '17813', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18507', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18018', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18141', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '17653', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '17744', '005', '6');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('15', '18592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '17742', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18754', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18034', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18120', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18003', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '17956', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '19102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18944', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18443', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18492', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18851', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18426', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '17743', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18753', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '18719', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1556', '005', '6');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('15', '1557', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1558', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1561', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1562', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1565', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1566', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1567', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1568', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1569', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1570', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1821', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1571', '005', '6');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('15', '1572', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1573', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1822', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1551', '005', '6'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('15', '1552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1131', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1177', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1130', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1180', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1175', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1178', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '1553', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('15', '2573', '005', '7');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('15', '2574', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2575', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2576', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2577', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2578', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2579', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2580', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2581', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2582', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2583', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2584', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2585', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2586', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2587', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2588', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2589', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2590', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2591', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2592', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2593', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2593', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2594', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2596', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2597', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2598', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2599', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2600', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2601', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '2602', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16085', '005', '7');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('15', '16089', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16093', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16097', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16101', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16086', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16090', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16094', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16098', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16102', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16087', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16091', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16095', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16099', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16103', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14547', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14551', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14555', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14559', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14563', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14548', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14552', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14556', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14560', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14564', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14549', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14553', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14557', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14561', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14565', '001', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14962', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14966', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14970', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14974', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14978', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14963', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14967', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14971', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14975', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14979', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14964', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14968', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14972', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14976', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14980', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15626', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15630', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15634', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15638', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15642', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15627', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15631', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15635', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15639', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15643', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15714', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15718', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15722', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15726', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15730', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15712', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15716', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15720', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15724', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15728', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15713', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15717', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15721', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15725', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15729', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15628', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15632', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15636', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15640', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15644', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1418', '005', '7');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('15', '1419', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1420', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1421', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1422', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1423', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1424', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1425', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '3339', '005', '7');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('15', '3340', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '3341', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '3342', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '3343', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '3344', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '4247', '025', '7'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('15', '18019', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15793', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18717', '005', '7'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('15', '17741', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18850', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18943', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18425', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18718', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18588', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '16602', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18491', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '14469', '005', '7'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('15', '14468', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '15223', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '17813', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18507', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18018', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18141', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '17653', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '17744', '005', '7');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('15', '18592', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18720', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '17742', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18754', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18034', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18120', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18003', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '17956', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18590', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '19102', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18589', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18944', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18443', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18492', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18851', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18426', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '17743', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18753', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '18719', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1556', '005', '7');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('15', '1557', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1558', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1559', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1560', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1561', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1562', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1563', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1564', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1565', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1566', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1567', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1568', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1569', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1570', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1821', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1571', '005', '7');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('15', '1572', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1573', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1574', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1575', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1576', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1577', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1578', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1579', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1580', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1581', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1582', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1583', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1584', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1585', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1822', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1551', '005', '7'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('15', '1552', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1131', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1177', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1130', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1180', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1175', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1178', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1553', '005', '7');
INSERT INTO `bcnm_loot` VALUES ('15', '1453', '1000', '8');  --  silverpiece
INSERT INTO `bcnm_loot` VALUES ('15', '18717', '111', '9');  --  perdu weapons
INSERT INTO `bcnm_loot` VALUES ('15', '17741', '111', '9');
INSERT INTO `bcnm_loot` VALUES ('15', '18850', '111', '9');
INSERT INTO `bcnm_loot` VALUES ('15', '18943', '111', '9');
INSERT INTO `bcnm_loot` VALUES ('15', '18425', '111', '9');
INSERT INTO `bcnm_loot` VALUES ('15', '18718', '111', '9');
INSERT INTO `bcnm_loot` VALUES ('15', '18588', '111', '9');
INSERT INTO `bcnm_loot` VALUES ('15', '16602', '111', '9');
INSERT INTO `bcnm_loot` VALUES ('15', '18491', '112', '9');
INSERT INTO `bcnm_loot` VALUES ('16', '13056', '150', '0'); -- under observation hundos 1-2
INSERT INTO `bcnm_loot` VALUES ('16', '1456', '850', '0'); 
INSERT INTO `bcnm_loot` VALUES ('16', '1450', '333', '1'); 
INSERT INTO `bcnm_loot` VALUES ('16', '1453', '333', '1'); 
INSERT INTO `bcnm_loot` VALUES ('16', '1456', '334', '1'); 
INSERT INTO `bcnm_loot` VALUES ('16', '14469', '100', '2'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('16', '14468', '100', '2');
INSERT INTO `bcnm_loot` VALUES ('16', '13301', '100', '2');
INSERT INTO `bcnm_loot` VALUES ('16', '15223', '100', '2');
INSERT INTO `bcnm_loot` VALUES ('16', '17813', '100', '2');
INSERT INTO `bcnm_loot` VALUES ('16', '18507', '100', '2');
INSERT INTO `bcnm_loot` VALUES ('16', '18018', '100', '2');
INSERT INTO `bcnm_loot` VALUES ('16', '18141', '100', '2');
INSERT INTO `bcnm_loot` VALUES ('16', '17653', '100', '2');
INSERT INTO `bcnm_loot` VALUES ('16', '18019', '100', '2'); 
INSERT INTO `bcnm_loot` VALUES ('16', '5736', '1000', '3'); -- LCP x2
INSERT INTO `bcnm_loot` VALUES ('16', '5736', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('16', '2573', '005', '5');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('16', '2574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2586', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2587', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2591', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2594', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2596', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2597', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2598', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2599', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2600', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2601', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16085', '005', '5');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('16', '16089', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16093', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16097', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16101', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16086', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16090', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16094', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16098', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16087', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16091', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16095', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16099', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16103', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14547', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14551', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14555', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14548', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14556', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14549', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14553', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14557', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14561', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14565', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14962', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14966', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14970', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14974', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14978', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14963', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14967', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14971', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14975', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14979', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14964', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14968', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14972', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14976', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14980', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15626', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15630', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15634', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15638', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15642', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15627', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15631', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15635', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15639', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15643', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15714', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15722', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15726', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15730', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15712', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15716', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15724', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15728', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15713', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15717', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15721', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15725', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15729', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15628', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15632', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15636', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15640', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15644', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1418', '005', '5');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('16', '1419', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1420', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1421', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1422', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1423', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1424', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '3339', '005', '5');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('16', '3340', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '3341', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '3342', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '3343', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '3344', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '4247', '025', '5'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('16', '18019', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15793', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18717', '005', '5'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('16', '17741', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18850', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18943', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '16602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18491', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '14469', '005', '5'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('16', '14468', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '15223', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '17813', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18507', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18018', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18141', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '17653', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '17744', '005', '5');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('16', '18592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '17742', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18754', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18034', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18120', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18003', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '17956', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '19102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18944', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18443', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18492', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18851', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18426', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '17743', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18753', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '18719', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1556', '005', '5');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('16', '1557', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1558', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1561', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1562', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1565', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1566', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1567', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1568', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1569', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1570', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1821', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1571', '005', '5');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('16', '1572', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1573', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1822', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1551', '005', '5'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('16', '1552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1131', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1177', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1130', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1180', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1175', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1178', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '1553', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('16', '2573', '005', '6');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('16', '2574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2586', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2587', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2591', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2594', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2596', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2597', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2598', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2599', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2600', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2601', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '2602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16085', '005', '6');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('16', '16089', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16093', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16097', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16101', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16086', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16090', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16094', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16098', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16087', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16091', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16095', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16099', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16103', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14547', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14551', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14555', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14548', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14556', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14549', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14553', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14557', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14561', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14565', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14962', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14966', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14970', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14974', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14978', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14963', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14967', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14971', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14975', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14979', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14964', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14968', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14972', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14976', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14980', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15626', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15630', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15634', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15638', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15642', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15627', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15631', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15635', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15639', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15643', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15714', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15722', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15726', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15730', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15712', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15716', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15724', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15728', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15713', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15717', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15721', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15725', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15729', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15628', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15632', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15636', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15640', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15644', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1418', '005', '6');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('16', '1419', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1420', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1421', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1422', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1423', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1424', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '3339', '005', '6');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('16', '3340', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '3341', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '3342', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '3343', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '3344', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '4247', '025', '6'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('16', '18019', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15793', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18717', '005', '6'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('16', '17741', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18850', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18943', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '16602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18491', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '14469', '005', '6'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('16', '14468', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '15223', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '17813', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18507', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18018', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18141', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '17653', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '17744', '005', '6');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('16', '18592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '17742', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18754', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18034', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18120', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18003', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '17956', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '19102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18944', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18443', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18492', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18851', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18426', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '17743', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18753', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '18719', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1556', '005', '6');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('16', '1557', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1558', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1561', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1562', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1565', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1566', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1567', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1568', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1569', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1570', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1821', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1571', '005', '6');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('16', '1572', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1573', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1822', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1551', '005', '6'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('16', '1552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1131', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1177', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1130', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1180', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1175', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1178', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('16', '1553', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '5736', '1000', '0'); -- contaminated colosseum LCP
INSERT INTO `bcnm_loot` VALUES ('17', '5736', '1000', '1'); -- LCP
INSERT INTO `bcnm_loot` VALUES ('17', '5736', '1000', '2'); -- LCP
INSERT INTO `bcnm_loot` VALUES ('17', '1456', '1000', '3'); -- byne
INSERT INTO `bcnm_loot` VALUES ('17', '1456', '1000', '4'); -- byne
INSERT INTO `bcnm_loot` VALUES ('17', '15688', '333', '5'); -- amir feet
INSERT INTO `bcnm_loot` VALUES ('17', '15690', '333', '5'); -- yigit feet
INSERT INTO `bcnm_loot` VALUES ('17', '15695', '334', '5'); -- pahluwan feet
INSERT INTO `bcnm_loot` VALUES ('17', '18717', '111', '6');  --  perdu weapons
INSERT INTO `bcnm_loot` VALUES ('17', '17741', '111', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '18850', '111', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '18943', '111', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '18425', '111', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '18718', '111', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '18588', '111', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '16602', '111', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '18491', '112', '6');
INSERT INTO `bcnm_loot` VALUES ('17', '3339', '142', '7');  --  ENM pops
INSERT INTO `bcnm_loot` VALUES ('17', '3340', '142', '7');
INSERT INTO `bcnm_loot` VALUES ('17', '3341', '142', '7');
INSERT INTO `bcnm_loot` VALUES ('17', '3342', '142', '7');
INSERT INTO `bcnm_loot` VALUES ('17', '3343', '142', '7');
INSERT INTO `bcnm_loot` VALUES ('17', '3344', '142', '7');
INSERT INTO `bcnm_loot` VALUES ('17', '1823', '148', '7');
INSERT INTO `bcnm_loot` VALUES ('17', '2573', '005', '8');  --  items 2573-2602 are ZNM pop items
INSERT INTO `bcnm_loot` VALUES ('17', '2574', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2575', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2576', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2577', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2578', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2579', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2580', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2581', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2582', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2583', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2584', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2585', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2586', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2587', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2588', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2589', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2590', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2591', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2592', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2593', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2593', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2594', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2596', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2597', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2598', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2599', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2600', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2601', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2602', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16085', '005', '8');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('17', '16089', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16093', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16097', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16101', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16086', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16090', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16094', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16098', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16102', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16087', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16091', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16095', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16099', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16103', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14547', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14551', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14555', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14559', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14563', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14548', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14552', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14556', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14560', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14564', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14549', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14553', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14557', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14561', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14565', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14962', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14966', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14970', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14974', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14978', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14963', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14967', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14971', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14975', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14979', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14964', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14968', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14972', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14976', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14980', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15626', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15630', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15634', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15638', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15642', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15627', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15631', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15635', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15639', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15643', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15714', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15718', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15722', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15726', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15730', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15712', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15716', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15720', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15724', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15728', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15713', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15717', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15721', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15725', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15729', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15628', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15632', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15636', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15640', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15644', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1418', '005', '8');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('17', '1419', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1420', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1421', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1422', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1423', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1424', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1425', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '3339', '005', '8');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('17', '3340', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '3341', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '3342', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '3343', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '3344', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '4247', '025', '8'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('17', '18019', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15793', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18717', '005', '8'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('17', '17741', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18850', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18943', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18425', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18718', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18588', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '16602', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18491', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '14469', '005', '8'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('17', '14468', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '15223', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '17813', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18507', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18018', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18141', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '17653', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '17744', '005', '8');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('17', '18592', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18720', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '17742', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18754', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18034', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18120', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18003', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '17956', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18590', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '19102', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18589', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18944', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18443', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18492', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18851', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18426', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '17743', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18753', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '18719', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1556', '005', '8');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('17', '1557', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1558', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1559', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1560', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1561', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1562', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1563', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1564', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1565', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1566', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1567', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1568', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1569', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1570', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1821', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1571', '005', '8');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('17', '1572', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1573', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1574', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1575', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1576', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1577', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1578', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1579', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1580', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1581', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1582', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1583', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1584', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1585', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1822', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1551', '005', '8'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('17', '1552', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1131', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1177', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1130', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1180', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1175', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1178', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '1553', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('17', '2573', '005', '9');  --  items 2573-2602 are ZNM pop items
INSERT INTO `bcnm_loot` VALUES ('17', '2574', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2575', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2576', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2577', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2578', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2579', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2580', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2581', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2582', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2583', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2584', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2585', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2586', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2587', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2588', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2589', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2590', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2591', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2592', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2593', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2593', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2594', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2596', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2597', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2598', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2599', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2600', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2601', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '2602', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16085', '005', '9');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('17', '16089', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16093', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16097', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16101', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16086', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16090', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16094', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16098', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16102', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16087', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16091', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16095', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16099', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16103', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14547', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14551', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14555', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14559', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14563', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14548', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14552', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14556', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14560', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14564', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14549', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14553', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14557', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14561', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14565', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14962', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14966', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14970', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14974', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14978', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14963', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14967', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14971', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14975', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14979', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14964', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14968', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14972', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14976', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14980', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15626', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15630', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15634', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15638', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15642', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15627', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15631', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15635', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15639', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15643', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15714', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15718', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15722', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15726', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15730', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15712', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15716', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15720', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15724', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15728', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15713', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15717', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15721', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15725', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15729', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15628', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15632', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15636', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15640', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15644', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1418', '005', '9');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('17', '1419', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1420', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1421', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1422', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1423', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1424', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1425', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '3339', '005', '9');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('17', '3340', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '3341', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '3342', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '3343', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '3344', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '4247', '025', '9'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('17', '18019', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15793', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18717', '005', '9'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('17', '17741', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18850', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18943', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18425', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18718', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18588', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '16602', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18491', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '14469', '005', '9'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('17', '14468', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '15223', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '17813', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18507', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18018', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18141', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '17653', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '17744', '005', '9');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('17', '18592', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18720', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '17742', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18754', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18034', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18120', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18003', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '17956', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18590', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '19102', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18589', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18944', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18443', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18492', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18851', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18426', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '17743', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18753', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '18719', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1556', '005', '9');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('17', '1557', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1558', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1559', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1560', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1561', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1562', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1563', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1564', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1565', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1566', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1567', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1568', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1569', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1570', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1821', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1571', '005', '9');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('17', '1572', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1573', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1574', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1575', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1576', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1577', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1578', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1579', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1580', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1581', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1582', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1583', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1584', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1585', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1822', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1551', '005', '9'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('17', '1552', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1131', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1177', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1130', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1180', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1175', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1178', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('17', '1553', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1450', '1000', '0'); -- lungo up in arms
INSERT INTO `bcnm_loot` VALUES ('79', '1456', '1000', '1'); -- 100byne
INSERT INTO `bcnm_loot` VALUES ('79', '1453', '1000', '2'); -- silverpiece
INSERT INTO `bcnm_loot` VALUES ('79', '5736', '1000', '3'); -- LCP x3
INSERT INTO `bcnm_loot` VALUES ('79', '5736', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('79', '5736', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('79', '2573', '033', '6'); --  all znm pop items
INSERT INTO `bcnm_loot` VALUES ('79', '2574', '043', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2575', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2576', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2577', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2578', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2579', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2580', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2581', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2582', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2583', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2584', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2585', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2586', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2587', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2588', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2589', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2590', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2591', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2592', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2593', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2594', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2595', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2596', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2597', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2598', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2599', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2600', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2601', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '2602', '033', '6');
INSERT INTO `bcnm_loot` VALUES ('79', '15185', '850', '7');  --  kclub
INSERT INTO `bcnm_loot` VALUES ('79', '17440', '150', '7');
INSERT INTO `bcnm_loot` VALUES ('79', '2573', '005', '8');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('79', '2574', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2575', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2576', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2577', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2578', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2579', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2580', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2581', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2582', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2583', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2584', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2585', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2586', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2587', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2588', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2589', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2590', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2591', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2592', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2593', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2593', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2594', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2596', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2597', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2598', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2599', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2600', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2601', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2602', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16085', '005', '8');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('79', '16089', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16093', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16097', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16101', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16086', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16090', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16094', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16098', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16102', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16087', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16091', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16095', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16099', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16103', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14547', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14551', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14555', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14559', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14563', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14548', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14552', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14556', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14560', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14564', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14549', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14553', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14557', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14561', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14565', '001', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14962', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14966', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14970', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14974', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14978', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14963', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14967', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14971', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14975', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14979', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14964', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14968', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14972', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14976', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14980', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15626', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15630', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15634', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15638', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15642', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15627', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15631', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15635', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15639', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15643', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15714', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15718', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15722', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15726', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15730', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15712', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15716', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15720', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15724', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15728', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15713', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15717', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15721', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15725', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15729', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15628', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15632', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15636', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15640', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15644', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1418', '005', '8');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('79', '1419', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1420', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1421', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1422', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1423', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1424', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1425', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '3339', '005', '8');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('79', '3340', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '3341', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '3342', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '3343', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '3344', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '4247', '025', '8'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('79', '18019', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15793', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18717', '005', '8'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('79', '17741', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18850', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18943', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18425', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18718', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18588', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '16602', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18491', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '14469', '005', '8'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('79', '14468', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '15223', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '17813', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18507', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18018', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18141', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '17653', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '17744', '005', '8');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('79', '18592', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18720', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '17742', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18754', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18034', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18120', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18003', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '17956', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18590', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '19102', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18589', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18944', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18443', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18492', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18851', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18426', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '17743', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18753', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '18719', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1556', '005', '8');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('79', '1557', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1558', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1559', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1560', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1561', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1562', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1563', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1564', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1565', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1566', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1567', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1568', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1569', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1570', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1821', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1571', '005', '8');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('79', '1572', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1573', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1574', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1575', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1576', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1577', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1578', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1579', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1580', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1581', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1582', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1583', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1584', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1585', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1822', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1551', '005', '8'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('79', '1552', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1131', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1177', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1130', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1180', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1175', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1178', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '1553', '005', '8');
INSERT INTO `bcnm_loot` VALUES ('79', '2573', '005', '9');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('79', '2574', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2575', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2576', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2577', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2578', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2579', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2580', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2581', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2582', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2583', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2584', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2585', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2586', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2587', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2588', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2589', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2590', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2591', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2592', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2593', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2593', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2594', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2596', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2597', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2598', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2599', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2600', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2601', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '2602', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16085', '005', '9');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('79', '16089', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16093', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16097', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16101', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16086', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16090', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16094', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16098', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16102', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16087', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16091', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16095', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16099', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16103', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14547', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14551', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14555', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14559', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14563', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14548', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14552', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14556', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14560', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14564', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14549', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14553', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14557', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14561', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14565', '001', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14962', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14966', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14970', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14974', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14978', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14963', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14967', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14971', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14975', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14979', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14964', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14968', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14972', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14976', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14980', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15626', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15630', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15634', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15638', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15642', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15627', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15631', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15635', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15639', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15643', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15714', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15718', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15722', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15726', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15730', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15712', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15716', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15720', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15724', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15728', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15713', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15717', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15721', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15725', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15729', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15628', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15632', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15636', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15640', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15644', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1418', '005', '9');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('79', '1419', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1420', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1421', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1422', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1423', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1424', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1425', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '3339', '005', '9');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('79', '3340', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '3341', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '3342', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '3343', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '3344', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '4247', '025', '9'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('79', '18019', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15793', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18717', '005', '9'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('79', '17741', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18850', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18943', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18425', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18718', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18588', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '16602', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18491', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '14469', '005', '9'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('79', '14468', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '15223', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '17813', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18507', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18018', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18141', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '17653', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '17744', '005', '9');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('79', '18592', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18720', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '17742', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18754', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18034', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18120', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18003', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '17956', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18590', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '19102', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18589', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18944', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18443', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18492', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18851', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18426', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '17743', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18753', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '18719', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1556', '005', '9');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('79', '1557', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1558', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1559', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1560', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1561', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1562', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1563', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1564', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1565', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1566', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1567', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1568', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1569', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1570', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1821', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1571', '005', '9');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('79', '1572', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1573', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1574', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1575', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1576', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1577', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1578', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1579', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1580', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1581', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1582', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1583', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1584', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1585', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1822', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1551', '005', '9'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('79', '1552', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1131', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1177', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1130', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1180', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1175', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1178', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('79', '1553', '005', '9');
INSERT INTO `bcnm_loot` VALUES ('35', '1450', '333', '0'); -- petrifying pair hundo 1
INSERT INTO `bcnm_loot` VALUES ('35', '1453', '333', '0');
INSERT INTO `bcnm_loot` VALUES ('35', '1456', '334', '0');
INSERT INTO `bcnm_loot` VALUES ('35', '1450', '333', '1');  --  hundo 2
INSERT INTO `bcnm_loot` VALUES ('35', '1453', '333', '1');
INSERT INTO `bcnm_loot` VALUES ('35', '1456', '334', '1');
INSERT INTO `bcnm_loot` VALUES ('35', '5736', '1000', '2');  --  LCP x2
INSERT INTO `bcnm_loot` VALUES ('35', '5736', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('35', '1418', '125', '4');  --  sky pop item
INSERT INTO `bcnm_loot` VALUES ('35', '1419', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('35', '1420', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('35', '1421', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('35', '1422', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('35', '1423', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('35', '1424', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('35', '1425', '125', '4');
INSERT INTO `bcnm_loot` VALUES ('35', '2573', '005', '5');  --  items 2573-2602 are ZNM pop items GRAB BAG1
INSERT INTO `bcnm_loot` VALUES ('35', '2574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2586', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2587', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2591', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2593', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2594', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2596', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2597', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2598', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2599', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2600', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2601', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16085', '005', '5');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('35', '16089', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16093', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16097', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16101', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16086', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16090', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16094', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16098', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16087', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16091', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16095', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16099', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16103', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14547', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14551', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14555', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14548', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14556', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14549', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14553', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14557', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14561', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14565', '001', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14962', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14966', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14970', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14974', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14978', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14963', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14967', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14971', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14975', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14979', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14964', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14968', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14972', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14976', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14980', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15626', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15630', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15634', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15638', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15642', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15627', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15631', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15635', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15639', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15643', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15714', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15722', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15726', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15730', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15712', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15716', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15724', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15728', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15713', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15717', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15721', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15725', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15729', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15628', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15632', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15636', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15640', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15644', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1418', '005', '5');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('35', '1419', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1420', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1421', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1422', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1423', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1424', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '3339', '005', '5');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('35', '3340', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '3341', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '3342', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '3343', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '3344', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '4247', '025', '5'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('35', '18019', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15793', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18717', '005', '5'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('35', '17741', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18850', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18943', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18425', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18718', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18588', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '16602', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18491', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '14469', '005', '5'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('35', '14468', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '15223', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '17813', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18507', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18018', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18141', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '17653', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '17744', '005', '5');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('35', '18592', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18720', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '17742', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18754', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18034', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18120', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18003', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '17956', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18590', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '19102', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18589', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18944', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18443', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18492', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18851', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18426', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '17743', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18753', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '18719', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1556', '005', '5');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('35', '1557', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1558', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1559', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1560', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1561', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1562', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1563', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1564', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1565', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1566', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1567', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1568', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1569', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1570', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1821', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1571', '005', '5');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('35', '1572', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1573', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1574', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1575', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1576', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1577', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1578', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1579', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1580', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1581', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1582', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1583', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1584', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1585', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1822', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1551', '005', '5'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('35', '1552', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1131', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1177', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1130', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1180', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1175', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1178', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '1553', '005', '5');
INSERT INTO `bcnm_loot` VALUES ('35', '2573', '005', '6');  --  items 2573-2602 are ZNM pop items GRAB BAG2
INSERT INTO `bcnm_loot` VALUES ('35', '2574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2586', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2587', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2591', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2593', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2594', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2596', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2597', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2598', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2599', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2600', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2601', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '2602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16085', '005', '6');  --  salvage component pieces
INSERT INTO `bcnm_loot` VALUES ('35', '16089', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16093', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16097', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16101', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16086', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16090', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16094', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16098', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16087', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16091', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16095', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16099', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16103', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14547', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14551', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14555', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14548', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14556', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14549', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14553', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14557', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14561', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14565', '001', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14962', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14966', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14970', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14974', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14978', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14963', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14967', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14971', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14975', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14979', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14964', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14968', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14972', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14976', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14980', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15626', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15630', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15634', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15638', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15642', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15627', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15631', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15635', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15639', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15643', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15714', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15722', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15726', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15730', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15712', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15716', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15724', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15728', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15713', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15717', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15721', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15725', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15729', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15628', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15632', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15636', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15640', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15644', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1418', '005', '6');  --  sky pop items 1418-1425
INSERT INTO `bcnm_loot` VALUES ('35', '1419', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1420', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1421', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1422', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1423', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1424', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '3339', '005', '6');  --  ENM spawn items 3339-3344
INSERT INTO `bcnm_loot` VALUES ('35', '3340', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '3341', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '3342', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '3343', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '3344', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '4247', '025', '6'); --  EXP items
INSERT INTO `bcnm_loot` VALUES ('35', '18019', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15793', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18717', '005', '6'); --  perdu weapons 
INSERT INTO `bcnm_loot` VALUES ('35', '17741', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18850', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18943', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18425', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18718', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18588', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '16602', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18491', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '14469', '005', '6'); --  odd items
INSERT INTO `bcnm_loot` VALUES ('35', '14468', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15223', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '17813', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18507', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18018', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18141', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '17653', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '17744', '005', '6');  --  vigil weapons
INSERT INTO `bcnm_loot` VALUES ('35', '18592', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18720', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '17742', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18754', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18034', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18120', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18003', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '17956', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18590', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '19102', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18589', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18944', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18443', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18492', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18851', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18426', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '17743', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18753', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '18719', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1556', '005', '6');  --  attestations items 1556-1570 + 1821
INSERT INTO `bcnm_loot` VALUES ('35', '1557', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1558', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1559', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1560', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1561', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1562', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1563', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1564', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1565', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1566', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1567', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1568', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1569', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1570', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1821', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1571', '005', '6');  --  fragments items 1571-1585 + 1822
INSERT INTO `bcnm_loot` VALUES ('35', '1572', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1573', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1574', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1575', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1576', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1577', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1578', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1579', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1580', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1581', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1582', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1583', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1584', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1585', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1822', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1551', '005', '6'); -- BCNM orbs
INSERT INTO `bcnm_loot` VALUES ('35', '1552', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1131', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1177', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1130', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1180', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1175', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1178', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '1553', '005', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '809', '0', '6'); -- yes 0%
INSERT INTO `bcnm_loot` VALUES ('35', '15276', '79', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '645', '105', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15281', '26', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '4132', '184', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '796', '79', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15273', '157', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '653', '210', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '799', '26', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15283', '263', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15277', '105', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15280', '105', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15274', '131', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '744', '79', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '806', '131', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '0', '600', '6');
INSERT INTO `bcnm_loot` VALUES ('35', '15271', '552', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '15272', '131', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '809', '0', '7'); -- yes 0%
INSERT INTO `bcnm_loot` VALUES ('35', '15276', '79', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '645', '105', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '15281', '26', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '4132', '184', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '796', '79', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '15273', '157', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '653', '210', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '799', '26', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '15283', '263', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '15277', '105', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '15280', '105', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '15274', '131', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '744', '79', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '806', '131', '7');
INSERT INTO `bcnm_loot` VALUES ('35', '0', '600', '7');
-- limbus                      lootID,ItemID,Roll,lootgroup
-- SE Appollyon first floor
INSERT INTO `bcnm_loot` VALUES ('110', '1875', '1000', '0'); -- ancient beastcoin*4
INSERT INTO `bcnm_loot` VALUES ('110', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('110', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('110', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('110', '1939', '350', '4'); --  RDM,
INSERT INTO `bcnm_loot` VALUES ('110', '1941', '278', '4'); --  THF,
INSERT INTO `bcnm_loot` VALUES ('110', '1959', '174', '4'); --   SMN,
INSERT INTO `bcnm_loot` VALUES ('110', '1949', '200', '4');  --  BRD
INSERT INTO `bcnm_loot` VALUES ('110', '1945', '47', '5');  --  DRK,
INSERT INTO `bcnm_loot` VALUES ('110', '1951', '49', '5');  --  RNG,
INSERT INTO `bcnm_loot` VALUES ('110', '1955', '200', '5');  --  NIN,
INSERT INTO `bcnm_loot` VALUES ('110', '2659', '62', '5');  --  COR,
INSERT INTO `bcnm_loot` VALUES ('110', '2715', '407', '5');  --  DNC
-- -------------------------------------
-- SE Appollyon Second floor
-- -------------------------------------
INSERT INTO `bcnm_loot` VALUES ('111', '1875', '1000', '0'); -- ancient beastcoin*4
INSERT INTO `bcnm_loot` VALUES ('111', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('111', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('111', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('111', '1959', '47', '4'); --   SMN,
INSERT INTO `bcnm_loot` VALUES ('111', '1949', '30', '4');  --  BRD
INSERT INTO `bcnm_loot` VALUES ('111', '1943', '200', '4'); -- PLD,
INSERT INTO `bcnm_loot` VALUES ('111', '1947', '460', '4'); --  BST,
INSERT INTO `bcnm_loot` VALUES ('111', '2661', '400', '4'); --  PUP
INSERT INTO `bcnm_loot` VALUES ('111', '1951', '20', '5');  --  RNG,
INSERT INTO `bcnm_loot` VALUES ('111', '1955', '80', '5');  --  NIN,
INSERT INTO `bcnm_loot` VALUES ('111', '1945', '90', '5');  --  DRK,
INSERT INTO `bcnm_loot` VALUES ('111', '2659', '100', '5');  --  COR,
INSERT INTO `bcnm_loot` VALUES ('111', '2715', '120', '5');  --  DNC
-- SE Appollyon Third floor
-- -------------------------------------
INSERT INTO `bcnm_loot` VALUES ('112', '1875', '1000', '0'); -- ancient beastcoin*4
INSERT INTO `bcnm_loot` VALUES ('112', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('112', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('112', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('112', '1955', '59', '4');  --  NIN,
INSERT INTO `bcnm_loot` VALUES ('112', '1959', '139', '4'); --   SMN,
INSERT INTO `bcnm_loot` VALUES ('112', '1949', '39', '4');  --  BRD
INSERT INTO `bcnm_loot` VALUES ('112', '1681', '39', '4'); --   Light Steel
INSERT INTO `bcnm_loot` VALUES ('112', '645', '39', '4');  --  Darksteel Ore
INSERT INTO `bcnm_loot` VALUES ('112', '1933', '627', '4'); --  MNK,
INSERT INTO `bcnm_loot` VALUES ('112', '1945', '159', '5');  --  DRK,
INSERT INTO `bcnm_loot` VALUES ('112', '1951', '139', '5');  --  RNG,
INSERT INTO `bcnm_loot` VALUES ('112', '2659', '39', '5');  --  COR,
INSERT INTO `bcnm_loot` VALUES ('112', '664', '20', '5');  --  Darksteel Sheet
INSERT INTO `bcnm_loot` VALUES ('112', '646', '20', '5');  --   Adaman Ore
INSERT INTO `bcnm_loot` VALUES ('112', '1931', '200', '5'); --  WAR,
-- ---SE Appollyon fourth floor-------------------------
INSERT INTO `bcnm_loot` VALUES ('113', '1875', '1000', '0'); -- ancient beastcoin*5
INSERT INTO `bcnm_loot` VALUES ('113', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('113', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('113', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('113', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('113', '1935', '220', '5'); --  WHM,
INSERT INTO `bcnm_loot` VALUES ('113', '1937', '300', '5'); -- BLM,
INSERT INTO `bcnm_loot` VALUES ('113', '1957', '260', '5'); --  DRG,
INSERT INTO `bcnm_loot` VALUES ('113', '1953', '340', '5');  --  SAM,
INSERT INTO `bcnm_loot` VALUES ('113', '2657', '220', '6');  --  BLU,
INSERT INTO `bcnm_loot` VALUES ('113', '2717', '180', '6');  --  SCH
INSERT INTO `bcnm_loot` VALUES ('113', '1931', '300', '6');  -- WAR,
INSERT INTO `bcnm_loot` VALUES ('113', '1909', '1000', '7'); --  Smalt Chip,
INSERT INTO `bcnm_loot` VALUES ('113', '2127', '59', '8');  --  metal chip
INSERT INTO `bcnm_loot` VALUES ('113', '1875', '100', '8');
--  ----NE APOLLYON FIRST FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('114', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('114', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('114', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('114', '1875', '1000', '3');

INSERT INTO `bcnm_loot` VALUES ('114', '1953', '304', '4'); -- SAM,
INSERT INTO `bcnm_loot` VALUES ('114', '1943', '18', '4'); --  PLD,
INSERT INTO `bcnm_loot` VALUES ('114', '1941', '200', '4'); -- THF,
INSERT INTO `bcnm_loot` VALUES ('114', '2715', '200', '4'); -- DNC
INSERT INTO `bcnm_loot` VALUES ('114', '2661', '36', '4'); -- PUP

INSERT INTO `bcnm_loot` VALUES ('114', '1933', '18', '5'); -- MNK,
INSERT INTO `bcnm_loot` VALUES ('114', '1939', '36', '5'); -- RDM,
INSERT INTO `bcnm_loot` VALUES ('114', '1935', '411', '5'); -- WHM,
INSERT INTO `bcnm_loot` VALUES ('114', '2717', '482', '5'); --  SCH,
INSERT INTO `bcnm_loot` VALUES ('114', '1947', '18', '5'); -- BST,

--  ----NE APOLLYON SECOND FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('115', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('115', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('115', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('115', '1875', '1000', '3');

INSERT INTO `bcnm_loot` VALUES ('115', '1947', '26', '4'); -- BST,
INSERT INTO `bcnm_loot` VALUES ('115', '1933', '53', '4'); -- MNK,
INSERT INTO `bcnm_loot` VALUES ('115', '1943', '26', '4'); -- PLD,
INSERT INTO `bcnm_loot` VALUES ('115', '2661', '26', '4'); -- PUP
INSERT INTO `bcnm_loot` VALUES ('115', '1937', '395', '4'); -- BLM

INSERT INTO `bcnm_loot` VALUES ('115', '1957', '289', '5'); -- DRG,
INSERT INTO `bcnm_loot` VALUES ('115', '1941', '53', '5'); -- THF,
INSERT INTO `bcnm_loot` VALUES ('115', '1939', '112', '5'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('115', '2657', '477', '5'); -- BLU,

--  ----NE APOLLYON thIRd FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('116', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('116', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('116', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('116', '1875', '1000', '3');

INSERT INTO `bcnm_loot` VALUES ('116', '1931', '788', '4'); -- WAR,
INSERT INTO `bcnm_loot` VALUES ('116', '1939', '30', '4'); -- RDM,
INSERT INTO `bcnm_loot` VALUES ('116', '1953', '130', '4'); -- SAM,
INSERT INTO `bcnm_loot` VALUES ('116', '1957', '100', '4'); --  DRG
INSERT INTO `bcnm_loot` VALUES ('116', '1947', '90', '4'); -- BST,

INSERT INTO `bcnm_loot` VALUES ('116', '1933', '30', '5'); -- MNK,
INSERT INTO `bcnm_loot` VALUES ('116', '1941', '99', '5'); -- THF
INSERT INTO `bcnm_loot` VALUES ('116', '2661', '61', '5'); -- PUP
INSERT INTO `bcnm_loot` VALUES ('116', '2715', '30', '5'); -- DNC
INSERT INTO `bcnm_loot` VALUES ('116', '1943', '160', '5'); -- PLD

INSERT INTO `bcnm_loot` VALUES ('116', '1633', '30', '6'); -- Clot Plasma
INSERT INTO `bcnm_loot` VALUES ('116', '821', '40', '6'); -- Rainbow Thread

INSERT INTO `bcnm_loot` VALUES ('116', '1311', '50', '7'); -- Oxblood
INSERT INTO `bcnm_loot` VALUES ('116', '1883', '40', '7'); -- Shell Powder
INSERT INTO `bcnm_loot` VALUES ('116', '2004', '20', '7'); -- Carapace Powder

--  ----NE APOLLYON FOURTH FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('117', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('117', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('117', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('117', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('117', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('117', '1875', '1000', '5');

INSERT INTO `bcnm_loot` VALUES ('117', '1949', '326', '6'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('117', '1945', '256', '6'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('117', '1951', '395', '6'); -- RNG

INSERT INTO `bcnm_loot` VALUES ('117', '1959', '279', '7'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('117', '1955', '256', '7'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('117', '2659', '326', '7'); -- COR

--  ----NE APOLLYON FIVE FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('118', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('118', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('118', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('118', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('118', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('118', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('118', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('118', '1875', '100', '7');
INSERT INTO `bcnm_loot` VALUES ('118', '2127', '59', '7'); -- Metal Chip.
INSERT INTO `bcnm_loot` VALUES ('118', '1910', '1000', '8'); -- Smoky Chip.

--  ----SW APOLLYON FIRST FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('119', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('119', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('119', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('119', '1875', '1000', '3');

INSERT INTO `bcnm_loot` VALUES ('119', '1949', '464', '4');-- BRD
INSERT INTO `bcnm_loot` VALUES ('119', '1945', '250', '4');-- DRK
INSERT INTO `bcnm_loot` VALUES ('119', '1953', '110', '4');-- SAM
INSERT INTO `bcnm_loot` VALUES ('119', '1937', '71', '4');-- BLM

INSERT INTO `bcnm_loot` VALUES ('119', '1931', '180', '5');-- WAR
INSERT INTO `bcnm_loot` VALUES ('119', '2657', '210', '5');-- BLU
INSERT INTO `bcnm_loot` VALUES ('119', '2717', '111', '5');-- SCH
INSERT INTO `bcnm_loot` VALUES ('119', '1935', '107', '5');-- WHM

--  ----SW APOLLYON SECOND FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('120', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('120', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('120', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('120', '1875', '1000', '3');

INSERT INTO `bcnm_loot` VALUES ('120', '1951', '154', '4');-- RNG
INSERT INTO `bcnm_loot` VALUES ('120', '1935', '95', '4');-- WHM
INSERT INTO `bcnm_loot` VALUES ('120', '1959', '269', '4');-- SMN

INSERT INTO `bcnm_loot` VALUES ('120', '1937', '106', '5');-- BLM
INSERT INTO `bcnm_loot` VALUES ('120', '1931', '77', '5');-- WAR
INSERT INTO `bcnm_loot` VALUES ('120', '2659', '423', '5');-- COR
INSERT INTO `bcnm_loot` VALUES ('120', '1957', '110', '5');-- DRG

--  ----SW APOLLYON THIRD FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('121', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('121', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('121', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('121', '1875', '1000', '3');


INSERT INTO `bcnm_loot` VALUES ('121', '1955', '595', '4');-- NIN
INSERT INTO `bcnm_loot` VALUES ('121', '1957', '100', '4');-- DRG
INSERT INTO `bcnm_loot` VALUES ('121', '1937', '24', '4');-- BLM
INSERT INTO `bcnm_loot` VALUES ('121', '1953', '48', '4');-- SAM

INSERT INTO `bcnm_loot` VALUES ('121', '1931', '120', '5');-- WAR
INSERT INTO `bcnm_loot` VALUES ('121', '1953', '48', '5');-- SAM
INSERT INTO `bcnm_loot` VALUES ('121', '1935', '24', '5');-- WHM
INSERT INTO `bcnm_loot` VALUES ('121', '2657', '24', '5');-- BLU
INSERT INTO `bcnm_loot` VALUES ('121', '2717', '71', '5');-- SCH

INSERT INTO `bcnm_loot` VALUES ('121', '1311', '32', '6');-- Oxblood
INSERT INTO `bcnm_loot` VALUES ('121', '1883', '40', '6');-- Shell Powder
INSERT INTO `bcnm_loot` VALUES ('121', '1681', '31', '6');-- Light Steel
INSERT INTO `bcnm_loot` VALUES ('121', '1633', '71', '6');-- Clot Plasma
INSERT INTO `bcnm_loot` VALUES ('121', '645', '31', '6');-- Darksteel Ore
INSERT INTO `bcnm_loot` VALUES ('121', '664', '63', '6');-- Darksteel Sheet
INSERT INTO `bcnm_loot` VALUES ('121', '646', '31', '6');-- Adaman Ore
INSERT INTO `bcnm_loot` VALUES ('121', '821', '63', '6');-- Rainbow Thread

--  ----SW APOLLYON Fourth FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('122', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('122', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('122', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('122', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('122', '1875', '1000', '4');

INSERT INTO `bcnm_loot` VALUES ('122', '1941', '468', '5');-- THF
INSERT INTO `bcnm_loot` VALUES ('122', '1947', '340', '5');-- BST
INSERT INTO `bcnm_loot` VALUES ('122', '1933', '255', '5');-- MNK
INSERT INTO `bcnm_loot` VALUES ('122', '1939', '191', '5');-- RDM

INSERT INTO `bcnm_loot` VALUES ('122', '1943', '170', '6');-- PLD
INSERT INTO `bcnm_loot` VALUES ('122', '2661', '340', '6');-- PUP
INSERT INTO `bcnm_loot` VALUES ('122', '2715', '170', '6');-- DNC

INSERT INTO `bcnm_loot` VALUES ('122', '1987', '1000', '7');-- Charcoal Chip
INSERT INTO `bcnm_loot` VALUES ('122', '2127', '59', '8');-- Metal Chip
INSERT INTO `bcnm_loot` VALUES ('122', '1875', '100', '8');
--  ----NW APOLLYON FIRST FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('123', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('123', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('123', '1937', '25', '2');--  BLM,
INSERT INTO `bcnm_loot` VALUES ('123', '2657', '175', '2');--  BLU,
INSERT INTO `bcnm_loot` VALUES ('123', '1957', '100', '2');--  DRG,
INSERT INTO `bcnm_loot` VALUES ('123', '1943', '25', '2');--  PLD,
INSERT INTO `bcnm_loot` VALUES ('123', '1953', '250', '2');--  SAM,
INSERT INTO `bcnm_loot` VALUES ('123', '2717', '75', '2');--  SCH,
INSERT INTO `bcnm_loot` VALUES ('123', '1931', '225', '2');--  WAR,
INSERT INTO `bcnm_loot` VALUES ('123', '1935', '50', '2');--  WHM
--  ----NW APOLLYON SECOND FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('124', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('124', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('124', '1943', '235', '2');--  BRD,
INSERT INTO `bcnm_loot` VALUES ('124', '2659', '59', '2');--  COR,
INSERT INTO `bcnm_loot` VALUES ('124', '1945', '235', '2');--  DRK,
INSERT INTO `bcnm_loot` VALUES ('124', '1955', '147', '2');--  NIN,
INSERT INTO `bcnm_loot` VALUES ('124', '1951', '118', '2');--  RNG,
INSERT INTO `bcnm_loot` VALUES ('124', '1959', '176', '2');--  SMN,
INSERT INTO `bcnm_loot` VALUES ('124', '1935', '110', '2');--  WHM
--  ----NW APOLLYON THIRD FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('125', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('125', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('125', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('125', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('125', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('125', '1947', '133', '5');--  BST,
INSERT INTO `bcnm_loot` VALUES ('125', '1933', '133', '5');--  MNK,
INSERT INTO `bcnm_loot` VALUES ('125', '1943', '133', '5');--  PLD,
INSERT INTO `bcnm_loot` VALUES ('125', '2661', '133', '5');--  PUP,
INSERT INTO `bcnm_loot` VALUES ('125', '1939', '110', '5');--  RDM,
INSERT INTO `bcnm_loot` VALUES ('125', '1941', '400', '5');--  THF,
INSERT INTO `bcnm_loot` VALUES ('125', '646', '50', '6');--  Adaman Ore,
INSERT INTO `bcnm_loot` VALUES ('125', '1633', '50', '6');--  Clot Plasma,
INSERT INTO `bcnm_loot` VALUES ('125', '664', '50', '6');--  Darksteel Sheet,
INSERT INTO `bcnm_loot` VALUES ('125', '645', '50', '6');--  Darksteel Ore,
INSERT INTO `bcnm_loot` VALUES ('125', '1311', '50', '6');--  Oxblood,
INSERT INTO `bcnm_loot` VALUES ('125', '1681', '50', '6');--  Light Steel,
INSERT INTO `bcnm_loot` VALUES ('125', '821', '50', '6');--  Rainbow Thread,
INSERT INTO `bcnm_loot` VALUES ('125', '1883', '50', '6');--  Shell Powder
--  ----NW APOLLYON FOURTH FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('126', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('126', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('126', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('126', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('126', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('126', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('126', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('126', '1875', '1000', '7');
INSERT INTO `bcnm_loot` VALUES ('126', '1937', '80', '8');--  BLM,
INSERT INTO `bcnm_loot` VALUES ('126', '2657', '70', '8');--  BLU,
INSERT INTO `bcnm_loot` VALUES ('126', '1949', '48', '8');--  BRD
INSERT INTO `bcnm_loot` VALUES ('126', '1947', '30', '8');--  BST,
INSERT INTO `bcnm_loot` VALUES ('126', '2659', '25', '8');--  COR,
INSERT INTO `bcnm_loot` VALUES ('126', '1957', '19', '8');--  DRG,
INSERT INTO `bcnm_loot` VALUES ('126', '1945', '48', '8');--  DRK,
INSERT INTO `bcnm_loot` VALUES ('126', '1933', '90', '8');--  MNK,
INSERT INTO `bcnm_loot` VALUES ('126', '1955', '00', '8');--  NIN,
INSERT INTO `bcnm_loot` VALUES ('126', '2661', '48', '8');--  PUP,
INSERT INTO `bcnm_loot` VALUES ('126', '1939', '136', '8');--  RDM,
INSERT INTO `bcnm_loot` VALUES ('126', '1951', '80', '8');--  RNG,
INSERT INTO `bcnm_loot` VALUES ('126', '1953', '110', '8');--  SAM,
INSERT INTO `bcnm_loot` VALUES ('126', '1959', '95', '8');--  SMN,
INSERT INTO `bcnm_loot` VALUES ('126', '2715', '123', '8');--  DNC,
INSERT INTO `bcnm_loot` VALUES ('126', '1935', '48', '8');--  WHM
--  ----NW APOLLYON FIfth FLOOR ----------------------------
INSERT INTO `bcnm_loot` VALUES ('127', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('127', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('127', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('127', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('127', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('127', '1937', '109', '5');--  BLM,
INSERT INTO `bcnm_loot` VALUES ('127', '2657', '152', '5');--  BLU,
INSERT INTO `bcnm_loot` VALUES ('127', '1949', '283', '5');--  BRD,
INSERT INTO `bcnm_loot` VALUES ('127', '1947', '109', '5');--  BST,
INSERT INTO `bcnm_loot` VALUES ('127', '2659', '65', '5');--  COR,
INSERT INTO `bcnm_loot` VALUES ('127', '2715', '130', '5');--  DNC,
INSERT INTO `bcnm_loot` VALUES ('127', '1957', '65', '6');--  DRG,
INSERT INTO `bcnm_loot` VALUES ('127', '1945', '174', '6');--  DRK,
INSERT INTO `bcnm_loot` VALUES ('127', '1933', '130', '6');--  MNK,
INSERT INTO `bcnm_loot` VALUES ('127', '1955', '196', '6');--  NIN,
INSERT INTO `bcnm_loot` VALUES ('127', '1943', '174', '6');--  PLD,
INSERT INTO `bcnm_loot` VALUES ('127', '2661', '174', '6');--  PUP,
INSERT INTO `bcnm_loot` VALUES ('127', '1939', '109', '6');--  RDM
INSERT INTO `bcnm_loot` VALUES ('127', '1951', '130', '7');--  RNG,
INSERT INTO `bcnm_loot` VALUES ('127', '1953', '304', '7');--  SAM,
INSERT INTO `bcnm_loot` VALUES ('127', '2717', '87', '7');--  SCH,
INSERT INTO `bcnm_loot` VALUES ('127', '1959', '217', '7');--  SMN,
INSERT INTO `bcnm_loot` VALUES ('127', '1941', '174', '7');--  THF,
INSERT INTO `bcnm_loot` VALUES ('127', '1931', '130', '7');--  WAR,
INSERT INTO `bcnm_loot` VALUES ('127', '1935', '109', '7');--  WHM
INSERT INTO `bcnm_loot` VALUES ('127', '1988', '1000', '8');-- magenta-chip
INSERT INTO `bcnm_loot` VALUES ('127', '2127', '59', '9');-- Metal Chip
INSERT INTO `bcnm_loot` VALUES ('127', '1875', '100', '9');
-- ----------  CENTRAL APOLLYON --------------------------------

INSERT INTO `bcnm_loot` VALUES ('128', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('128', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('128', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('128', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('128', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('128', '1925', '659', '5'); -- Omega's Eye
INSERT INTO `bcnm_loot` VALUES ('128', '1927', '394', '5'); -- Omega's Foreleg
INSERT INTO `bcnm_loot` VALUES ('128', '1928', '388', '5'); -- Omega's Hind Leg
INSERT INTO `bcnm_loot` VALUES ('128', '1929', '404', '5'); -- Omega's Tail

INSERT INTO `bcnm_loot` VALUES ('128', '1928', '394', '6'); -- Omega's Hind Leg
INSERT INTO `bcnm_loot` VALUES ('128', '1929', '402', '6'); -- Omega's Tail
INSERT INTO `bcnm_loot` VALUES ('128', '1925', '659', '6'); -- Omega's Eye
INSERT INTO `bcnm_loot` VALUES ('128', '1927', '383', '6'); -- Omega's Foreleg

INSERT INTO `bcnm_loot` VALUES ('128', '1926', '265', '7'); -- Omega's Heart
INSERT INTO `bcnm_loot` VALUES ('128', '1875', '100', '7');
-- -----------------------------------------------------------------

INSERT INTO `bcnm_loot` VALUES ('129', '1933', '9', '0'); -- Ancient Brass (MNK)(0%)
INSERT INTO `bcnm_loot` VALUES ('129', '1931', '53', '0'); -- Argyro Rivet (WAR)(5.3%)
INSERT INTO `bcnm_loot` VALUES ('129', '1959', '6', '0'); -- Astral Leather (SMN)(0.6%)
INSERT INTO `bcnm_loot` VALUES ('129', '1935', '12', '0'); -- Benedict Yarn (WHM)(1.2%)
INSERT INTO `bcnm_loot` VALUES ('129', '1945', '29', '0'); -- Black Rivet (DRK)(2.9%)
INSERT INTO `bcnm_loot` VALUES ('129', '1957', '12', '0'); -- Blue Rivet (DRG)(1.2%)
INSERT INTO `bcnm_loot` VALUES ('129', '1949', '35', '0'); -- Brown Doeskin (BRD)(3.5%)
INSERT INTO `bcnm_loot` VALUES ('129', '2659', '35', '0'); -- Canvas Toile (COR)(3.5%)
INSERT INTO `bcnm_loot` VALUES ('129', '1939', '12', '0'); -- Cardinal Cloth (RDM) (1.2%)
INSERT INTO `bcnm_loot` VALUES ('129', '1951', '12', '0'); -- Charcoal Cotton (RNG) (1.2%)
INSERT INTO `bcnm_loot` VALUES ('129', '2661', '12', '0'); -- Corduroy Cloth (PUP) (1.2%)
INSERT INTO `bcnm_loot` VALUES ('129', '1937', '18', '0'); -- Diabolic Yarn (BLM)(1.8%)
INSERT INTO `bcnm_loot` VALUES ('129', '1955', '29', '0'); -- Ebony Lacquer (NIN) (2.9%)
INSERT INTO `bcnm_loot` VALUES ('129', '2717', '12', '0'); -- Electrum Stud (SCH)(1.2%)
INSERT INTO `bcnm_loot` VALUES ('129', '1947', '12', '0'); -- Fetid Lanolin (BST)(1.2%)
INSERT INTO `bcnm_loot` VALUES ('129', '2657', '18', '0'); -- Flameshun Cloth (BLU)(1.8%)
INSERT INTO `bcnm_loot` VALUES ('129', '2715', '5', '0'); -- Gold Stud (DNC) (0%)
INSERT INTO `bcnm_loot` VALUES ('129', '1953', '35', '0'); -- Kurogane (SAM)(3.5%)
INSERT INTO `bcnm_loot` VALUES ('129', '1941', '41', '0'); -- Light Filament (THF)(4.1%)
INSERT INTO `bcnm_loot` VALUES ('129', '1943', '18', '0'); -- White Rivet (PLD)(1.8%)
INSERT INTO `bcnm_loot` VALUES ('129', '1987', '53', '0'); -- Charcoal Chip (5.3%)
INSERT INTO `bcnm_loot` VALUES ('129', '1988', '76', '0'); -- Magenta Chip (7.6%)
INSERT INTO `bcnm_loot` VALUES ('129', '1909', '64', '0'); -- Smalt Chip (6.4%)
INSERT INTO `bcnm_loot` VALUES ('129', '1910', '41', '0'); -- Smoky Chip (4.1%)
INSERT INTO `bcnm_loot` VALUES ('129', '646', '50', '1');--  Adaman Ore,
INSERT INTO `bcnm_loot` VALUES ('129', '1633', '50', '1');--  Clot Plasma,
INSERT INTO `bcnm_loot` VALUES ('129', '664', '50', '1');--  Darksteel Sheet,
INSERT INTO `bcnm_loot` VALUES ('129', '645', '50', '1');--  Darksteel Ore,
INSERT INTO `bcnm_loot` VALUES ('129', '1311', '50', '1');--  Oxblood,
INSERT INTO `bcnm_loot` VALUES ('129', '1681', '50', '1');--  Light Steel,
INSERT INTO `bcnm_loot` VALUES ('129', '821', '50', '1');--  Rainbow Thread,
INSERT INTO `bcnm_loot` VALUES ('129', '1883', '50', '1');--  Shell Powder

-- ------------------------------------------------------------------------------------



-- Temenos_Northern_Tower --------------------------------------------------------------
-- F1
INSERT INTO `bcnm_loot` VALUES ('130', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('130', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('130', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('130', '1954', '159', '3'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('130', '1940', '146', '3'); -- THF
INSERT INTO `bcnm_loot` VALUES ('130', '1932', '85', '3'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('130', '1956', '171', '3'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('130', '1934', '110', '3'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('130', '2658', '220', '3'); -- COR
INSERT INTO `bcnm_loot` VALUES ('130', '2716', '98', '3'); -- SCH

-- F2
INSERT INTO `bcnm_loot` VALUES ('131', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('131', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('131', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('131', '1932', '333', '3'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('131', '1954', '200', '3'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('131', '1950', '100', '3'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('131', '1940', '90', '3'); -- THF
INSERT INTO `bcnm_loot` VALUES ('131', '1942', '70', '3'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('131', '1934', '90', '3'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('131', '1936', '100', '3'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('131', '1958', '90', '3'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('131', '2656', '67', '3'); -- BLU
INSERT INTO `bcnm_loot` VALUES ('131', '1956', '167', '3'); -- DRG

-- F3
INSERT INTO `bcnm_loot` VALUES ('132', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('132', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('132', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('132', '1956', '27', '3'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('132', '1932', '324', '3'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('132', '1950', '80', '3'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('132', '1934', '189', '3'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('132', '1930', '50', '3'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('132', '1940', '27', '4'); -- THF
INSERT INTO `bcnm_loot` VALUES ('132', '1936', '81', '4'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('132', '1944', '80', '4'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('132', '1958', '81', '4'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('132', '2658', '270', '4'); -- COR
INSERT INTO `bcnm_loot` VALUES ('132', '2714', '108', '4'); -- DNC

-- F4
INSERT INTO `bcnm_loot` VALUES ('133', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('133', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('133', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('133', '1942', '90', '3'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('133', '1934', '435', '3'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('133', '1956', '80', '3'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('133', '1940', '174', '3'); -- THF
INSERT INTO `bcnm_loot` VALUES ('133', '1958', '87', '3'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('133', '1954', '90', '3'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('133', '1936', '87', '3'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('133', '1930', '43', '3'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('133', '2656', '27', '3'); -- BLU
INSERT INTO `bcnm_loot` VALUES ('133', '2658', '261', '3'); -- COR

-- F5
INSERT INTO `bcnm_loot` VALUES ('134', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('134', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('134', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('134', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('134', '1954', '67', '4'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('134', '1940', '353', '4'); -- THF
INSERT INTO `bcnm_loot` VALUES ('134', '1936', '87', '4'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('134', '1956', '110', '4'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('134', '1958', '87', '4'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('134', '1942', '50', '4'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('134', '1950', '60', '4'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('134', '1932', '59', '4'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('134', '2716', '100', '4'); -- SCH
INSERT INTO `bcnm_loot` VALUES ('134', '2714', '110', '4'); -- DNC

-- F6
INSERT INTO `bcnm_loot` VALUES ('135', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('135', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('135', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('135', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('135', '1954', '263', '4'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('135', '1932', '59', '4'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('135', '1942', '53', '4'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('135', '1934', '60', '4'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('135', '1956', '526', '4'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('135', '1930', '60', '5'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('135', '1936', '53', '5'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('135', '1950', '158', '5'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('135', '2716', '105', '5'); -- SCH

-- F7
INSERT INTO `bcnm_loot` VALUES ('136', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('136', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('136', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('136', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('136', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('136', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('136', '1956', '240', '6'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('136', '1932', '120', '6'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('136', '1940', '200', '6'); -- THF
INSERT INTO `bcnm_loot` VALUES ('136', '1934', '40', '7'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('136', '1954', '120', '7'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('136', '2658', '200', '7'); -- COR
INSERT INTO `bcnm_loot` VALUES ('136', '2716', '80', '7'); -- SCH
INSERT INTO `bcnm_loot` VALUES ('136', '1875', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('136', '2127', '55', '8'); -- Metal Chip
INSERT INTO `bcnm_loot` VALUES ('136', '1904', '1000', '9'); -- Ivory Chip

-- Temenos_Western_Tower --------------------------------------------------------------
-- F1
INSERT INTO `bcnm_loot` VALUES ('137', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('137', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('137', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('137', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('137', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('137', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('137', '1948', '172', '6'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('137', '1938', '138', '6'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('137', '1952', '138', '6'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('137', '1958', '207', '6'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('137', '1930', '241', '6'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('137', '2656', '172', '6'); -- BLU


-- F2
INSERT INTO `bcnm_loot` VALUES ('138', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('138', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('138', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('138', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('138', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('138', '1948', '179', '5'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('138', '1938', '571', '5'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('138', '1944', '71', '5'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('138', '1952', '179', '5'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('138', '1946', '120', '6'); -- BST
INSERT INTO `bcnm_loot` VALUES ('138', '1934', '71', '6'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('138', '1930', '143', '6'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('138', '2660', '143', '6'); -- PUP


-- F3
INSERT INTO `bcnm_loot` VALUES ('139', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('139', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('139', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('139', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('139', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('139', '1948', '536', '5'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('139', '1952', '107', '5'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('139', '1938', '60', '5'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('139', '1934', '110', '5'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('139', '1930', '80', '5'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('139', '2660', '90', '5'); -- PUP
INSERT INTO `bcnm_loot` VALUES ('139', '1946', '71', '6'); -- BST
INSERT INTO `bcnm_loot` VALUES ('139', '1944', '103', '6'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('139', '1958', '160', '6'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('139', '1954', '36', '6'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('139', '2656', '250', '6'); -- BLU
INSERT INTO `bcnm_loot` VALUES ('139', '2716', '350', '6'); -- SCH
-- F4
INSERT INTO `bcnm_loot` VALUES ('140', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('140', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('140', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('140', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('140', '1952', '533', '4'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('140', '1946', '90', '4'); -- BST
INSERT INTO `bcnm_loot` VALUES ('140', '1938', '133', '4'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('140', '1932', '90', '4'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('140', '1958', '10', '5'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('140', '1954', '133', '5'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('140', '1944', '133', '5'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('140', '1930', '133', '5'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('140', '2660', '33', '5'); -- PUP


-- F5
INSERT INTO `bcnm_loot` VALUES ('141', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('141', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('141', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('141', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('141', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('141', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('141', '1954', '59', '6'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('141', '1930', '294', '6'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('141', '1946', '59', '6'); -- BST
INSERT INTO `bcnm_loot` VALUES ('141', '1934', '78', '6'); --  WHM
INSERT INTO `bcnm_loot` VALUES ('141', '2716', '59', '6'); -- SCH
INSERT INTO `bcnm_loot` VALUES ('141', '1958', '176', '7'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('141', '1938', '59', '7'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('141', '1948', '25', '7'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('141', '1932', '118', '7'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('141', '2656', '294', '7'); -- BLU

-- F6
INSERT INTO `bcnm_loot` VALUES ('142', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('142', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('142', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('142', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('142', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('142', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('142', '1954', '200', '6'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('142', '1958', '400', '6'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('142', '1948', '100', '6'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('142', '1934', '150', '6'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('142', '1932', '50', '7'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('142', '1930', '60', '7'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('142', '1938', '200', '7'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('142', '1944', '60', '7'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('142', '1952', '200', '7'); -- SAM

-- F7
INSERT INTO `bcnm_loot` VALUES ('143', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('143', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('143', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('143', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('143', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('143', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('143', '1948', '36', '6'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('143', '1952', '143', '6'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('143', '1930', '143', '6'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('143', '1958', '214', '6'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('143', '1938', '71', '6'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('143', '2656', '321', '6'); -- BLU
INSERT INTO `bcnm_loot` VALUES ('143', '1875', '100', '7');
INSERT INTO `bcnm_loot` VALUES ('143', '2127', '55', '7'); -- Metal Chip
INSERT INTO `bcnm_loot` VALUES ('143', '1906', '1000', '8'); -- Emerald Chip
-- Temenos_Eastern_Tower --------------------------------------------------------------

-- F1
INSERT INTO `bcnm_loot` VALUES ('144', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('144', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('144', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('144', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('144', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('144', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('144', '1944', '65', '6'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('144', '1936', '97', '6'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('144', '1946', '40', '6'); -- BST
INSERT INTO `bcnm_loot` VALUES ('144', '1942', '95', '6'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('144', '2660', '194', '6'); -- PUP
INSERT INTO `bcnm_loot` VALUES ('144', '2714', '32', '6'); -- DNC
INSERT INTO `bcnm_loot` VALUES ('144', '1950', '161', '6'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('144', '2716', '190', '6'); -- SCH
INSERT INTO `bcnm_loot` VALUES ('144', '2656', '210', '6'); -- BLU
-- F2
INSERT INTO `bcnm_loot` VALUES ('145', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('145', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('145', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('145', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('145', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('145', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('145', '1936', '367', '6'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('145', '1952', '70', '6'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('145', '1950', '40', '6'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('145', '1942', '333', '6'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('145', '1958', '20', '6'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('145', '1956', '106', '7'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('145', '1938', '33', '7'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('145', '1944', '76', '7'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('145', '1948', '95', '7'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('145', '2658', '67', '7'); -- COR
INSERT INTO `bcnm_loot` VALUES ('145', '1946', '133', '7'); -- BST

-- F3
INSERT INTO `bcnm_loot` VALUES ('146', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('146', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('146', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('146', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('146', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('146', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('146', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('146', '1942', '625', '7'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('146', '1944', '102', '7'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('146', '1950', '42', '7'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('146', '1952', '83', '7'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('146', '1946', '50', '7'); -- BST
INSERT INTO `bcnm_loot` VALUES ('146', '1940', '83', '8'); -- THF
INSERT INTO `bcnm_loot` VALUES ('146', '1936', '70', '8'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('146', '1938', '42', '8'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('146', '1948', '42', '8'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('146', '2660', '292', '8'); -- PUP

-- F4
INSERT INTO `bcnm_loot` VALUES ('147', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('147', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('147', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('147', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('147', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('147', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('147', '1950', '417', '6'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('147', '1956', '75', '6'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('147', '1944', '208', '6'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('147', '1940', '167', '6'); -- THF
INSERT INTO `bcnm_loot` VALUES ('147', '1946', '62', '7'); -- BST
INSERT INTO `bcnm_loot` VALUES ('147', '1936', '69', '7'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('147', '2660', '208', '7'); -- PUP
INSERT INTO `bcnm_loot` VALUES ('147', '1952', '42', '7'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('147', '2658', '83', '7'); -- COR

-- F5
INSERT INTO `bcnm_loot` VALUES ('148', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('148', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('148', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('148', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('148', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('148', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('148', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('148', '1944', '208', '7'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('148', '1938', '42', '7'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('148', '1946', '36', '7'); -- BST
INSERT INTO `bcnm_loot` VALUES ('148', '1940', '83', '7'); -- THF
INSERT INTO `bcnm_loot` VALUES ('148', '1942', '20', '7'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('148', '1952', '94', '8'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('148', '1956', '42', '8'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('148', '1936', '49', '8'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('148', '1950', '167', '8'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('148', '2714', '458', '8'); -- DNC

-- F6
INSERT INTO `bcnm_loot` VALUES ('149', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('149', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('149', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('149', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('149', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('149', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('149', '1942', '68', '6'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('149', '1948', '74', '6'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('149', '1936', '259', '6'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('149', '1940', '74', '6'); -- THF
INSERT INTO `bcnm_loot` VALUES ('149', '1956', '74', '6'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('149', '1950', '62', '6'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('149', '2656', '150', '7'); -- BLU
INSERT INTO `bcnm_loot` VALUES ('149', '1938', '76', '7'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('149', '1952', '53', '7'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('149', '2658', '111', '7'); -- COR
INSERT INTO `bcnm_loot` VALUES ('149', '2714', '370', '7'); -- DNC
INSERT INTO `bcnm_loot` VALUES ('149', '1946', '333', '7'); -- BST

-- F7
INSERT INTO `bcnm_loot` VALUES ('150', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('150', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('150', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('150', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('150', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('150', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('150', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('150', '1942', '38', '7'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('150', '1950', '67', '7'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('150', '1944', '100', '7'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('150', '1936', '233', '7'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('150', '1946', '80', '7'); -- BST
INSERT INTO `bcnm_loot` VALUES ('150', '2660', '333', '7'); -- PUP
INSERT INTO `bcnm_loot` VALUES ('150', '2714', '67', '7'); -- DNC
INSERT INTO `bcnm_loot` VALUES ('150', '1875', '100', '8');
INSERT INTO `bcnm_loot` VALUES ('150', '2127', '55', '8'); -- Metal Chip
INSERT INTO `bcnm_loot` VALUES ('150', '1905', '1000', '9'); -- Scarlet Chip,

-- Central_Temenos_1st_Floor --------------------------------------------------------------
INSERT INTO `bcnm_loot` VALUES ('151', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('151', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('151', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('151', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('151', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('151', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('151', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('151', '1930', '265', '7'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('151', '1938', '118', '7'); -- RDM
INSERT INTO `bcnm_loot` VALUES ('151', '1948', '147', '7'); -- BRD
INSERT INTO `bcnm_loot` VALUES ('151', '1958', '147', '7'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('151', '1952', '118', '7'); -- SAM
INSERT INTO `bcnm_loot` VALUES ('151', '2656', '235', '7'); -- BLU
INSERT INTO `bcnm_loot` VALUES ('151', '1986', '1000', '8'); -- Orchid Chip

-- Central_Temenos_2nd_Floor --------------------------------------------------------------
INSERT INTO `bcnm_loot` VALUES ('152', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('152', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('152', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('152', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('152', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('152', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('152', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('152', '1944', '250', '7'); -- DRK
INSERT INTO `bcnm_loot` VALUES ('152', '1936', '94', '7'); -- BLM
INSERT INTO `bcnm_loot` VALUES ('152', '1950', '63', '7'); -- RNG
INSERT INTO `bcnm_loot` VALUES ('152', '1942', '125', '7'); -- PLD
INSERT INTO `bcnm_loot` VALUES ('152', '1946', '63', '7'); -- BST
INSERT INTO `bcnm_loot` VALUES ('152', '2660', '281', '7'); -- PUP
INSERT INTO `bcnm_loot` VALUES ('152', '2714', '125', '7'); -- DNC
INSERT INTO `bcnm_loot` VALUES ('152', '1908', '1000', '8'); -- Cerulean Chip

-- Central_Temenos_3rd_Floor --------------------------------------------------------------
INSERT INTO `bcnm_loot` VALUES ('153', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('153', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('153', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('153', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('153', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('153', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('153', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('153', '1934', '53', '7'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('153', '1940', '132', '7'); -- THF
INSERT INTO `bcnm_loot` VALUES ('153', '1954', '105', '7'); -- NIN
INSERT INTO `bcnm_loot` VALUES ('153', '1932', '211', '7'); -- MNK
INSERT INTO `bcnm_loot` VALUES ('153', '1956', '211', '7'); -- DRG
INSERT INTO `bcnm_loot` VALUES ('153', '1930', '100', '7'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('153', '2658', '211', '7'); -- COR
INSERT INTO `bcnm_loot` VALUES ('153', '2716', '105', '7'); -- SCH
INSERT INTO `bcnm_loot` VALUES ('153', '1907', '1000', '8'); -- Silver Chip
-- Central_Temenos_4th_Floor --------------------------------------------------------------
INSERT INTO `bcnm_loot` VALUES ('154', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('154', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('154', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('154', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('154', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('154', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('154', '1875', '1000', '6');
INSERT INTO `bcnm_loot` VALUES ('154', '1920', '659', '7'); -- Ultima's Cerebrum
INSERT INTO `bcnm_loot` VALUES ('154', '1924', '394', '7'); -- Ultima's Tail
INSERT INTO `bcnm_loot` VALUES ('154', '1923', '388', '7'); -- ultimas-leg
INSERT INTO `bcnm_loot` VALUES ('154', '1922', '404', '7'); -- Ultima's Claw
INSERT INTO `bcnm_loot` VALUES ('154', '1924', '394', '8'); -- Ultima's Tail
INSERT INTO `bcnm_loot` VALUES ('154', '1922', '402', '8'); -- Ultima's Claw
INSERT INTO `bcnm_loot` VALUES ('154', '1920', '659', '8'); -- Ultima's Cerebrum
INSERT INTO `bcnm_loot` VALUES ('154', '1923', '383', '8'); -- ultimas-leg
INSERT INTO `bcnm_loot` VALUES ('154', '1921', '265', '9'); -- Ultima's Heart
INSERT INTO `bcnm_loot` VALUES ('154', '1875', '100', '9');

INSERT INTO `bcnm_loot` VALUES ('155', '1875', '1000', '0');
INSERT INTO `bcnm_loot` VALUES ('155', '1875', '1000', '1');
INSERT INTO `bcnm_loot` VALUES ('155', '1875', '1000', '2');
INSERT INTO `bcnm_loot` VALUES ('155', '1875', '1000', '3');
INSERT INTO `bcnm_loot` VALUES ('155', '1875', '1000', '4');
INSERT INTO `bcnm_loot` VALUES ('155', '1875', '1000', '5');
INSERT INTO `bcnm_loot` VALUES ('155', '1934', '200', '6'); -- WHM
INSERT INTO `bcnm_loot` VALUES ('155', '1930', '200', '6'); -- WAR
INSERT INTO `bcnm_loot` VALUES ('155', '1958', '200', '6'); -- SMN
INSERT INTO `bcnm_loot` VALUES ('155', '2658', '400', '6'); -- COR
INSERT INTO `bcnm_loot` VALUES ('155', '1940', '200', '6'); -- THF

-- BCNM Amphibian Assualt
INSERT INTO `bcnm_loot` VALUES ('156', '13155', '100', '0');
INSERT INTO `bcnm_loot` VALUES ('156', '13152', '100', '0');
INSERT INTO `bcnm_loot` VALUES ('156', '13150', '100', '0');
INSERT INTO `bcnm_loot` VALUES ('156', '13160', '100', '0');
INSERT INTO `bcnm_loot` VALUES ('156', '13156', '250', '1');
INSERT INTO `bcnm_loot` VALUES ('156', '13148', '250', '1');
INSERT INTO `bcnm_loot` VALUES ('156', '13151', '250', '1');
INSERT INTO `bcnm_loot` VALUES ('156', '13154', '250', '1');
INSERT INTO `bcnm_loot` VALUES ('156', '13158', '250', '1');
INSERT INTO `bcnm_loot` VALUES ('156', '4896', '48', '2');
INSERT INTO `bcnm_loot` VALUES ('156', '4874', '48', '2');
INSERT INTO `bcnm_loot` VALUES ('156', '4751', '143', '2');
INSERT INTO `bcnm_loot` VALUES ('156', '4714', '119', '2');
INSERT INTO `bcnm_loot` VALUES ('156', '4621', '150', '2');
INSERT INTO `bcnm_loot` VALUES ('156', '4175', '77', '3');
INSERT INTO `bcnm_loot` VALUES ('156', '4173', '154', '3');
INSERT INTO `bcnm_loot` VALUES ('156', '1260', '48', '4');
INSERT INTO `bcnm_loot` VALUES ('156', '1257', '48', '4');
INSERT INTO `bcnm_loot` VALUES ('156', '1256', '48', '4');
INSERT INTO `bcnm_loot` VALUES ('156', '1259', '48', '4');
INSERT INTO `bcnm_loot` VALUES ('156', '1261', '48', '4');
INSERT INTO `bcnm_loot` VALUES ('156', '1255', '48', '4');
INSERT INTO `bcnm_loot` VALUES ('156', '1262', '48', '4');
INSERT INTO `bcnm_loot` VALUES ('156', '1258', '48', '4');
INSERT INTO `bcnm_loot` VALUES ('156', '887', '150', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '837', '10', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '751', '850', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '810', '50', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '797', '50', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '803', '50', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '784', '50', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '791', '50', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '802', '50', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '652', '100', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '702', '30', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '771', '30', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '769', '30', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '776', '30', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '772', '30', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '773', '30', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '801', '30', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '775', '30', '5');
INSERT INTO `bcnm_loot` VALUES ('156', '774', '30', '5');

-- ENM Like the Wind
INSERT INTO `bcnm_loot` VALUES ('179','1763','30','1');
INSERT INTO `bcnm_loot` VALUES ('179','1769','30','1');
INSERT INTO `bcnm_loot` VALUES ('179','1764','30','1');
INSERT INTO `bcnm_loot` VALUES ('179','1842','100','0');
INSERT INTO `bcnm_loot` VALUES ('179','17946','100','2');
INSERT INTO `bcnm_loot` VALUES ('179','18358','100','2');
INSERT INTO `bcnm_loot` VALUES ('179','16976','100','2');
INSERT INTO `bcnm_loot` VALUES ('179','4990','230','2');
INSERT INTO `bcnm_loot` VALUES ('179','17946','100','3');
INSERT INTO `bcnm_loot` VALUES ('179','18358','100','3');
INSERT INTO `bcnm_loot` VALUES ('179','16976','100','3');
INSERT INTO `bcnm_loot` VALUES ('179','4990','230','3');

-- ENM Brothers
INSERT INTO `bcnm_loot` VALUES ('180','1767','271','0');
INSERT INTO `bcnm_loot` VALUES ('180','1762','340','0');
INSERT INTO `bcnm_loot` VALUES ('180','1771','330','0');
INSERT INTO `bcnm_loot` VALUES ('180','1842','44','0');
INSERT INTO `bcnm_loot` VALUES ('180','15302','123','1');
INSERT INTO `bcnm_loot` VALUES ('180','17277','163','1');
INSERT INTO `bcnm_loot` VALUES ('180','17707','167','1');
INSERT INTO `bcnm_loot` VALUES ('180','18098','148','1');
INSERT INTO `bcnm_loot` VALUES ('180','4748','281','1');
INSERT INTO `bcnm_loot` VALUES ('180','15302','128','2');
INSERT INTO `bcnm_loot` VALUES ('180','17277','163','2');
INSERT INTO `bcnm_loot` VALUES ('180','17707','167','2');
INSERT INTO `bcnm_loot` VALUES ('180','18098','153','2');
INSERT INTO `bcnm_loot` VALUES ('180','4748','271','2');
