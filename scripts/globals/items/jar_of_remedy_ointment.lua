-----------------------------------------
-- ID: 5356
-- Item: Remedy Ointment
-- Item Effect: This potion remedies status ailments.
-- Works on paralysis, silence, blindness, poison, disease, and mortality itself.
-----------------------------------------
require("scripts/globals/status");
-----------------------------------------

function onItemCheck(target)
    return 0;
end;

function onItemUse(target)

    if (target:hasStatusEffect(dsp.effect.EFFECT_SILENCE) == true) then
        target:delStatusEffect(dsp.effect.EFFECT_SILENCE);
    end
    if (target:hasStatusEffect(dsp.effect.EFFECT_BLINDNESS) == true) then
        target:delStatusEffect(dsp.effect.EFFECT_BLINDNESS);
    end
    if (target:hasStatusEffect(dsp.effect.EFFECT_POISON) == true) then
        target:delStatusEffect(dsp.effect.EFFECT_POISON);
    end
    if (target:hasStatusEffect(dsp.effect.EFFECT_PARALYSIS) == true) then
        target:delStatusEffect(dsp.effect.EFFECT_PARALYSIS);
    end

    local rDisease = math.random(1,2) -- Disease is not garunteed to be cured, 1 means removed 2 means fail. 50% chance
    if (rDisease == 1 and target:hasStatusEffect(dsp.effect.EFFECT_DISEASE) == true) then
        target:delStatusEffect(dsp.effect.EFFECT_DISEASE);
    end

    local rDoom = math.random(1,2) -- 50% chance of saving you from certain death
    if (rDoom == 1 and target:hasStatusEffect(dsp.effect.EFFECT_DOOM) == true) then
        target:delStatusEffect(dsp.effect.EFFECT_DOOM);
        target:messageBasic(msgBasic.NARROWLY_ESCAPE);
    end
end;
