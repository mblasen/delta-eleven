-- apply mir to player treasure pool as they enter dyna
-- instead of GM applying it

function apply_mir(player)
  local count = 0; -- 0 = fresh start
    print("setting...\n");
  player:setMir(count);
  local pchars = zone:getPlayers();
  for name, pchar in pairs(pchars) do
    pchar:PrintToPlayer(string.format("Your treasure pool now has mir."));
  end
end

function check_party_readiness(player)
  print("Checking for party...\n");
  local pchars = zone:getPlayers();
  local player_leader = player:getPartyLeader();
  local has_party = false;
  for name, pchar in pairs(pchars) do
    if(player == pchar) then
      -- not necessarily a party...
      has_party = has_party; -- so whatever
    else
      pchar_leader = pchar:getPartyLeader();
      if (pchar_leader == player_leader) then
        -- yes, there's a party
        print("party found...\n");
        has_party = true;
      end
    end
  end
  if (has_party) then
    print("applying...\n");
    apply_mir(player);
  end
end

function qualifies_for_mir(player)
  print("Checking for mir qualification...\n");
  if (player:checkIfMirUsed() == 0) then
    print("player qualifies! \n");
    -- mir has not been applied to this player's treasure pool
    check_party_readiness(player);
  end
end
