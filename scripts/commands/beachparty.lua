---------------------------------------------------------------------------------------------------
-- func: beachparty
-- desc: sends player to purgonorgo isle
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

function onTrigger(player)
    player:setPos( -398, -3, -413, 64, 4);
    player:PrintToPlayer("To the beach!!");
end
