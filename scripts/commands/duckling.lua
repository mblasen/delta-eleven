---------------------------------------------------------------------------------------------------
-- func: duckling <baby>
-- desc: Get a lost duckling back to its momma
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "ss"
};

function onTrigger(player, baby)
    if (baby == nil) then
        player:PrintToPlayer("Usage: @duckling <baby> (optional)<momma>");
        return
    end

    local baby = GetPlayerByName( baby );

    local momma = baby:getPartyLeader();

    if (momma == nil) then
        player:PrintToPlayer("No valid momma.");
    end

    if (baby ~= nil and momma ~=nil) then
        baby:setPos( momma:getXPos(), momma:getYPos(), momma:getZPos(), 0, momma:getZoneID() );
    else
        player:PrintToPlayer( string.format( "Couldn't get %s to %s.",baby,momma ));
    end
    return
end
