---------------------------------------------------------------------------------------------------
-- func: announce
-- desc: sends a basic message to all active players
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "s"
};

function error(player, msg)
    player:PrintToPlayer(msg);
    player:PrintToPlayer("!announce <message>");
end;

function onTrigger(player, message)
    -- validate message
    if (message == nil) then
        error(player, "Can't announce an empty string.");
        return;
    end

    -- inject message packet
    player:announce(message);
end
