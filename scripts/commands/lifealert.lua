---------------------------------------------------------------------------------------------------
-- func: lifealert
-- desc: raise a player or humiliate them depending on time
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

local life_delay = 3600; -- seconds between uses

function onTrigger(player)
    local alert_use = player:getVar(string.format("[lifealerttime]"));
    local alert_state = player:getVar(string.format("[lifealertstate]"));
    if ((alert_use < os.time()) or (alert_use == nil)) then
        local new_time = os.time() + life_delay;
        player:setVar("[lifealerttime]", new_time);
        player:setVar("[lifealertstate]", 1);
        player:sendRaise(1);
    elseif (alert_state == 1) then
        player:announce(string.format("Help! %s has fallen and they can't get up!",player:getName()));
        player:setVar("[lifealertstate]", 2);
    else
        player:PrintToPlayer("No one hears your cries for help.");
    end;
end;
