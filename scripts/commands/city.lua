---------------------------------------------------------------------------------------------------
-- func: city <id>
-- desc: Moves player to the city associated with the id.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0, -- 0 -> normal players (?)
    parameters = "s" -- string (?)
};

-- Hash table to store camp details
-- {x_pos, y_pos, z_pos, rotation, zone_id, message}
city = {}
city['bastok'] = {37, 0, -60, 195, 234, "Welcome to Bastok!"}
city['bastoks'] = {-286, -10, -109, 177, 87, "Welcome to Bastok! Don't become your own dad."}
city['bastys'] = {-286, -10, -109, 177, 87, "Welcome to Bastok! Don't become your own dad."}
city['sandoria'] = {49, 2, -25, 111, 230, "Welcome to San d' Oria!"}
city['sandorias'] = {-86, 1, -55, 54, 80, "Welcome to San d' Oria! Don't become your own dad."}
city['sandys'] = {-86, 1, -55, 54, 80, "Welcome to San d' Oria! Don't become your own dad."}
city['windurst'] = {-87, -5, 62, 88, 241, "You're in Windurst now."}
city['windursts'] = {-41, -5, 140, 37, 94, "You're in Windurst now. Become your own dad if that's what you're in to."}
city['windys'] = {-41, -5, 140, 37, 94, "You're in Windurst now. Become your own dad if that's what you're in to."}
city['jeuno'] = {-52, 0, 28, 83, 244, "Welcome to Jeuno!"}
city['whitegate'] = {-100, 0, -80, 223, 50, "Welcome to Whitegate!"}
city['rabao'] = {-28, 0, -81, 198, 247, "Welcome to Rabao!"}
city['norg'] = {-20, 0, -55, 185, 252, "Welcome t' Norg!"}
city['nashmau'] = {7, 0, -21, 106, 53, "Welcome to Nashmau!"}
city['kazham'] = {-42, -9, -72, 82, 250, "Welcome to Kazham!"}
city['selbina'] = {23, -10, -33, 20, 248, "Welcome to Selbina!"}
city['mhaura'] = {-12, -15, 84, 213, 249, "Welcome to Mhaura!"}
city['basty'] = {37, 0, -60, 195, 234, "Welcome to Bastok!"}
city['windy'] = {-87, -5, 62, 88, 241, "You're in Windurst now."}
city['sandy'] = {49, 2, -25, 111, 230, "Welcome to San d' Oria!"}

function onTrigger(player, id)
    if (id == nil or city[id] == nil) then
        player:PrintToPlayer("No valid city specified; try: @city kazham");
        return;
    else
        player:setPos( city[id][1], city[id][2], city[id][3], city[id][4], city[id][5] );
        player:PrintToPlayer(city[id][6]);
    end
end
