---------------------------------------------------------------------------------------------------
-- func: expcamp <id>
-- desc: Moves player to the location associated with the id.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0, -- 0 -> normal players (?)
    parameters = "i" -- integer (?)
};

-- Hash table to store camp details
-- {x_pos, y_pos, z_pos, rotation, zone_id, message}
camps = {}
camps[1] = {549, 0, -319, 0, 107, "Get page 5. Fight bees and worms, then lizards and crabs near LL. Then go to camp 10."} -- book in south gustaberg
camps[10] = {135, -7, 94, 149, 103, "Get page 1. Fight lizards and rabbits in NW E-7. Then go to camp 14."} -- camps 10-18 drop people at the valkurm dunes outpost in front of the book
camps[11] = {135, -7, 94, 149, 103, "Get page 1. Fight lizards and rabbits in NW E-7. Then go to camp 14."}
camps[12] = {135, -7, 94, 149, 103, "Get page 1. Fight lizards and rabbits in NW E-7. Then go to camp 14."}
camps[13] = {135, -7, 94, 149, 103, "Get page 1. Fight lizards and rabbits in NW E-7. Then go to camp 14."}
camps[14] = {135, -7, 94, 149, 103, "Get page 3. Fight flies and crabs at the grove at I-7. Then go to camp 19."}
camps[15] = {441, -30, 181, 195, 173, "Get page 1. Fight worms and later bats. Bats are extremely dangerous due to jet stream."} -- bonus camp korroloka
camps[16] = {135, -7, 94, 149, 103, "Get page 3. Fight flies and crabs at the grove at I-7. Then go to camp 19."}
camps[17] = {135, -7, 94, 149, 103, "Get page 3. Fight flies and crabs at the grove at I-7. Then go to camp 19."}
camps[18] = {189, -21, -20, 125, 126, "Get page 1. Watch for worm and gigas aggro. Then go to camp 22."} -- worm camp at qufim
camps[19] = {189, -21, -20, 125, 126, "Get page 1. Watch for worm and gigas aggro. Then go to camp 22."}
camps[20] = {189, -21, -20, 125, 126, "Get page 1. Watch for worm and gigas aggro. Then go to camp 22."}
camps[21] = {189, -21, -20, 125, 126, "Get page 1. Watch for worm and gigas aggro. Then go to camp 22."}
camps[22] = {-257, -19, 300, 95, 126, "Get page 3. Head to the ravine at G/H-7. Watch for gigas aggro. Then go to camp 25."} -- fish camp at qufim
camps[23] = {-257, -19, 300, 95, 126, "Get page 3. Head to the ravine at G/H-7. Watch for gigas aggro. Then go to camp 25."}
camps[25] = {-237, 0, 507, 58, 123, "NO PAGE. Kill mandragoras until 27. Watch out for sleepgas and/or use poison potions. Then go to camp 27."} -- kazham mandies
camps[26] = {-237, 0, 507, 58, 123, "NO PAGE. Kill mandragoras until 27. Watch out for sleepgas and/or use poison potions. Then go to camp 27."}
camps[27] = {-238, 0, -401, 20, 123, "Get page 3. Kill lots of apes. Shadows and hate bouncing advised, watch for their savage weapon skills. Then go to camp 31."} -- outpost opo-opos
camps[28] = {-238, 0, -401, 20, 123, "Get page 3. Kill lots of apes. Shadows and hate bouncing advised, watch for their savage weapon skills. Then go to camp 31."}
camps[29] = {-238, 0, -401, 20, 123, "Get page 3. Kill lots of apes. Shadows and hate bouncing advised, watch for their savage weapon skills. Then go to camp 31."}
camps[30] = {-238, 0, -401, 20, 123, "Get page 3. Kill lots of apes. Shadows and hate bouncing advised, watch for their savage weapon skills. Then go to camp 31."}
camps[31] = {-378, -5, 363, 253, 200, "Get page 2. And for god's sake don't stay until level 75 on this page. Can stay until 40 but please for your mental health go to camp 36. Alternate CN at camp 35."} -- garlaige shitadel NASCAR
camps[32] = {-378, -5, 363, 253, 200, "Get page 2. And for god's sake don't stay until level 75 on this page. Can stay until 40 but please for your mental health go to camp 36. Alternate CN at camp 35."}
camps[33] = {-378, -5, 363, 253, 200, "Get page 2. And for god's sake don't stay until level 75 on this page. Can stay until 40 but please for your mental health go to camp 36. Alternate CN at camp 35."}
camps[34] = {-378, -5, 363, 253, 200, "Get page 2. And for god's sake don't stay until level 75 on this page. Can stay until 40 but please for your mental health go to camp 36. Alternate CN at camp 35."}
camps[35] = {353, -32, -23, 83, 197, "Get page 1. Gentlemen, start your engines: this is NASCAR. Watch out for final sting, it will one shot you in the low 30s!"}
camps[36] = {522, -19, -254, 98, 81, "NO PAGE. Kill birds until 40. Bring extra food of course. We don't recommend fighting ladybugs at all. Then go to camp 41."} -- east ronfaure s birds
camps[37] = {522, -19, -254, 98, 81, "NO PAGE. Kill birds until 40. Bring extra food of course. We don't recommend fighting ladybugs at all. Then go to camp 41."}
camps[38] = {522, -19, -254, 98, 81, "NO PAGE. Kill birds until 40. Bring extra food of course. We don't recommend fighting ladybugs at all. Then go to camp 41."}
camps[39] = {522, -19, -254, 98, 81, "NO PAGE. Kill birds until 40. Bring extra food of course. We don't recommend fighting ladybugs at all. Then go to camp 41."}
camps[40] = {522, -19, -254, 98, 81, "NO PAGE. Kill birds until 40. Bring extra food of course. We don't recommend fighting ladybugs at all. Then go to camp 41."}
camps[41] = {245, -23, 202, 9, 97, "NO PAGE. Kill lynx until 45. Be ready for paralyze and silence. Roam up and down around these mountains for more mobs. Then go to camp 45."} -- meriphataud s lynxes
camps[42] = {245, -23, 202, 9, 97, "NO PAGE. Kill lynx until 45. Be ready for paralyze and silence. Roam up and down around these mountains for more mobs. Then go to camp 45."}
camps[43] = {245, -23, 202, 9, 97, "NO PAGE. Kill lynx until 45. Be ready for paralyze and silence. Roam up and down around these mountains for more mobs. Then go to camp 45."}
camps[44] = {245, -23, 202, 9, 97, "NO PAGE. Kill lynx until 45. Be ready for paralyze and silence. Roam up and down around these mountains for more mobs. Then go to camp 45."}
camps[45] = {-26, 0, 25, 123, 167, "Get page 2. Kill bats until 48. Jet stream is deadly at 45. Beware of blood aggro. Then go to camp 48."} -- bostaneaux bats
camps[46] = {-26, 0, 25, 123, 167, "Get page 2. Kill bats until 48. Jet stream is deadly at 45. Beware of blood aggro. Then go to camp 48."}
camps[47] = {-26, 0, 25, 123, 167, "Get page 2. Kill bats until 48. Jet stream is deadly at 45. Beware of blood aggro. Then go to camp 48."}
camps[48] = {-28, -29, 629, 165, 96, "NO PAGE. Kill dragonflies until 52. Beware of aggro, everything around here aggroes sight. Then go to camp 52."} -- karugo-narugo dragonflies
camps[49] = {-28, -29, 629, 165, 96, "NO PAGE. Kill dragonflies until 52. Beware of aggro, everything around here aggroes sight. Then go to camp 52."}
camps[50] = {-28, -29, 629, 165, 96, "NO PAGE. Kill dragonflies until 52. Beware of aggro, everything around here aggroes sight. Then go to camp 52."}
camps[51] = {-28, -29, 629, 165, 96, "NO PAGE. Kill dragonflies until 52. Beware of aggro, everything around here aggroes sight. Then go to camp 52."}
camps[52] = {41, -10, 256, 81, 174, "Get page 1. Kill until 54. Then go to camp 54 and never look back."} -- kuftal tunnel robber crabs
camps[53] = {41, -10, 256, 81, 174, "Get page 1. Kill until 54. Then go to camp 54 and never look back."}
camps[54] = {-233, -15, 82, 255, 51, "NO PAGE. Kill birds until 58. Then go to camp 59."} -- wajaom lesser colibri
camps[55] = {-233, -15, 82, 255, 51, "NO PAGE. Kill birds until 58. Then go to camp 59."}
camps[56] = {-233, -15, 82, 255, 51, "NO PAGE. Kill birds until 58. Then go to camp 59."}
camps[57] = {-233, -15, 82, 255, 51, "NO PAGE. Kill birds until 58. Then go to camp 59."}
camps[58] = {-233, -15, 82, 255, 51, "NO PAGE. Kill birds until 58. Then go to camp 59."}
camps[59] = {-252, -18, -425, 209, 51, "NO PAGE. Kill puks until 62. Then go to camp 63."} -- wajaom puks
camps[60] = {-252, -18, -425, 209, 51, "NO PAGE. Kill puks until 62. Then go to camp 63."}
camps[61] = {-252, -18, -425, 209, 51, "NO PAGE. Kill puks until 62. Then go to camp 63."}
camps[62] = {-252, -18, -425, 209, 51, "NO PAGE. Kill puks until 62. Then go to camp 63."}
camps[63] = {-20, -19, -160, 171, 153, "Get page 2. Consider other pages too, this place has a lot to choose from. Eruca camp is 64."} -- zhayolm magmatic erucas
camps[64] = {345, -25, -139, 123, 61, "NO PAGE. Kill erucas until 66. Then go to camp 66."}
camps[65] = {345, -25, -139, 123, 61, "NO PAGE. Kill erucas until 66. Then go to camp 66."}
camps[66] = {-702, 9, -236, 50, 79, "NO PAGE. Kill fomors until about 69. Then go to camp 67-70."} -- caedarva fomors
camps[67] = {539, -17, -421, 192, 79, "NO PAGE. Kill imps and jnuns until 70. Then go to camp 71."} -- caedarva imps
camps[68] = {539, -17, -421, 192, 79, "NO PAGE. Kill imps and jnuns until 70. Then go to camp 71."}
camps[69] = {539, -17, -421, 192, 79, "NO PAGE. Kill imps and jnuns until 70. Then go to camp 71."}
camps[70] = {539, -17, -421, 192, 79, "NO PAGE. Kill imps and jnuns until 70. Then go to camp 71."}
camps[71] = {-15, 0, 4, 199, 172, "Get page 4. EXP remains considerable until 75 with high chains. Camp 72 for Greater Colibri."} -- zehrun book
camps[72] = {222, 0, 19, 0, 72, "NO PAGE. No feeding the birds. Camp 73 for Mamools."} -- nyzuul isle staging point for greater colibri
camps[73] = {-140, -9, -697, 200, 52, "NO PAGE. If you see it, kill it. You've arrived at the last expcamp. Good hustle out there, go hit the showers!"} -- mamools

function onTrigger(player, id)
    if (id == nil or camps[id] == nil) then
        player:PrintToPlayer("No valid expcamp specified; try: @expcamp 1");
        return;
    else
        player:setPos( camps[id][1], camps[id][2], camps[id][3], camps[id][4], camps[id][5] );
        player:PrintToPlayer(tostring(camps[id][6]));
    end
end
