---------------------------------------------------------------------------------------------------
-- func: loveshack
-- desc: sends player to ghelsba op bcnm door
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

function onTrigger(player)
    player:setPos( -162, -11, 78, 116, 140);
    player:PrintToPlayer("Do you enter the love shack? The answer is always yes.");
end
