---------------------------------------------------------------------------------------------------
-- func: motm
-- desc: if params, add all to motm and print. if no params, print out current motm.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 4,
    parameters = "iiii"
};

motm = {}
motm[1] = {"Aern, Phuabo, Xzomit, Hpemde, Euvhi, Ghrah", {3,194,269,109,144,122,123,124}}
motm[2] = {"Antica, Sahagin, Tonberry", {25,213,243,244}}
motm[3] = {"Antlion, Diremite", {26,357,81}}
motm[4] = {"Bat Trio, Giant Bat", {47,46}}
motm[5] = {"Bee, Beetle, Fly, Ladybug", {48,49,113,374,375,170,390}}
motm[6] = {"Ghost, Skeleton, Hound", {52,121,227,142,143}}
motm[7] = {"Bird, Colibri, Apkallu, Giant Bird, Cockatrice", {55,72,27,125,70}}
motm[8] = {"Bomb, Snoll, Djinn", {56,232,82}}
motm[9] = {"Buffalo, Ram, Wivre, Marid", {57,208,257,180,371}}
motm[10]= {"Clot, Slime, Hecteyes", {66,228,229,230,139}}
motm[11]= {"Cluster", {68,69}}
motm[12]= {"Coeurl, Tiger", {71,242}}
motm[13]= {"Crab", {75,76,77,372}}
motm[14]= {"Crawler, Eruca", {79,107,108}}
motm[15]= {"Dhalmel", {80}}
motm[16]= {"Doll", {83,84,85,367,368,498}}
motm[17]= {"Doomed", {86}}
motm[18]= {"Lizard, Icelizard, Eft", {97,174,98}}
motm[19]= {"Fomor, Shade", {115,221,222,223,359}}
motm[20]= {"Funguar", {116}}
motm[21]= {"Gigas", {126,127,128,129,130,328}}
motm[22]= {"Goblin, Moblin, Bugbear", {133,327,373,272,184,59}}
motm[23]= {"Goobbue, Mandragora", {136,178}}
motm[24]= {"Hippogryph", {140,141}}
motm[25]= {"Imp, Soulflayer, Flan, Poroggo", {165,166,233,112,196}}
motm[26]= {"Karakul, Sheep, Bugard", {167,266,58}}
motm[27]= {"Kindred, Ahriman", {169,358,4}}
motm[28]= {"Leech, Worm", {172,369,258}}
motm[29]= {"Magic Pot, Golem, Evil Weapon", {175,135,110}}
motm[30]= {"Mamool, Troll, Lamia, Merrow", {176,177,285,171,182,246}}
motm[31]= {"Manticore, Taurus", {179,240}}
motm[32]= {"Opo-opo", {188}}
motm[33]= {"Orc, Warmachine, Quadav, Yagudo", {189,190,200,201,202,337,270,360}}
motm[34]= {"Puk, Wyvern", {198, 266}}
motm[35]= {"Qiqirn", {199}}
motm[36]= {"Qutrub", {203,204,205}}
motm[37]= {"Rabbit, Snow Rabbit, Cure Rabbit(?)", {206,404,405}}
motm[38]= {"Raptor", {210,376,377}}
motm[39]= {"Flytrap, Sapling, Treant, Sabotender", {216,114,245,212}}
motm[40]= {"Scorpion, Spider", {217,235}}
motm[41]= {"Sea Monk, Uragnite, Pugil", {218,219,251,197}}
motm[42]= {"Wamoura, Wamouracampa", {253,254}}

-- every motm group picked must be unique
function no_duplicates(int_tab)
    for index = 1, #int_tab, 1 do
        for i = 1, #int_tab, 1 do
            if (int_tab[i] == int_tab[index] and i ~= index) then
                return false
            end
        end
    end
    return true
end

function onTrigger(player,m1,m2,m3,m4)
    if (m1 == nil) then --player wants to know what motm is
        local current = player:getMotm();
        return
    else --player wants to change something
        if (m1 == -1) then --pick some random mob families and make them motm
            done = false;
            motm_nums = {}
            while (done == false) do
                motm_nums[1] = math.random(1,42);
                motm_nums[2] = math.random(1,42);
                motm_nums[3] = math.random(1,42);
                motm_nums[4] = math.random(1,42);
                m_count = 0
                for index = 1, #motm_nums, 1 do
                    m_count = m_count + #motm[motm_nums[index]][2]
                end
                if(no_duplicates(motm_nums)) then
                        done = true;
                end
            end
        else
            motm_nums[1] = m1
            motm_nums[2] = m2
            motm_nums[3] = m3
            motm_nums[4] = m4
            m_count = 0
            for index = 1, #motm_nums, 1 do
                m_count = m_count + #motm[motm_nums[index]][2]
            end
            if((no_duplicates(motm_nums) == false) or (m_count >16)) then
                player:PrintToPlayer("Error: duplicate motm or >16 family IDs. exiting.");
                return
            end
        end

        motm_values = {};
        counter = 1;
        filename = "./conf/motm.txt";
        file = io.open(filename, "w+");
        io.output(file);

        for index = 1, #motm_nums, 1 do
            current = motm_nums[index];

            player:PrintToPlayer(string.format("%s is a motm.", motm[current][1]));

            if type(motm[current][2]) == "table" then -- it is a table, go elt by elt
                tab = motm[current][2];
                for k,v in ipairs(tab) do
                    table.insert(motm_values,v);
                    counter = counter + 1;
                    io.write(string.format("%d\n", v));
                end
            else -- it is an int, just insert it
                table.insert(motm_values,motm[current][2]);
                counter = counter + 1;
                io.write(string.format("%d\n", motm[current][2]));
            end
        end

        while(counter <= 16) do
            table.insert(motm_values,0);
            counter = counter + 1;
        end

        io.close(file)

        --I know it is shit but this whole language is shit so it will do.
        m1 = motm_values[1]
        m2 = motm_values[2]
        m3 = motm_values[3]
        m4 = motm_values[4]
        m5 = motm_values[5]
        m6 = motm_values[6]
        m7 = motm_values[7]
        m8 = motm_values[8]
        m9 = motm_values[9]
        m10= motm_values[10]
        m11= motm_values[11]
        m12= motm_values[12]
        m13= motm_values[13]
        m14= motm_values[14]
        m15= motm_values[15]
        m16= motm_values[16]

        player:setMotm(m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16);
        return
    end
end
