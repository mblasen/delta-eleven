---------------------------------------------------------------------------------------------------
-- func: homp
-- desc: sends player to the linkshell standing area.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

function onTrigger(player)
    player:setPos( -21, 0, -20, 14, 245);
    player:PrintToPlayer("Returning to home point.");
end
