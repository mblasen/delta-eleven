---------------------------------------------------------------------------------------------------
-- func: bird
-- desc: puts player on a chocobo and sets a timer on recast.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

local bird_delay = 900; -- seconds between uses

function onTrigger(player)
    local next_use = player:getVar(string.format("[bird]"));
    if ((next_use < os.time())or (next_use == nil)) then
        local new_time = os.time() + bird_delay; -- is this right? prob not
        player:setVar("[bird]", new_time);
        local duration = 1800 + (player:getMod(MOD_CHOCOBO_RIDING_TIME) * 60);
        player:addStatusEffectEx(dsp.effect.MOUNTED,dsp.effect.MOUNTED,0,0,duration,true);
    else
        player:PrintToPlayer("!bird isn't ready, try again soon.");
    end;
end;

