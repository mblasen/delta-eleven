---------------------------------------------------------------------------------------------------
-- func: mir
-- desc: Sets the current mir count (0 is what you want)
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 1,
    parameters = "i"
};

function onTrigger(player)
    local count = 0; -- 0 = fresh start
    local leader = player:getPartyLeader();
    --local mirAlreadyUsed = player:checkIfMirUsed()
    --print(mirAlreadyUsed)

    if (leader == nil) then
        player:PrintToPlayer(string.format("You must be in a party to activate mir."));
        return
    end

    if (player:checkIfMirUsed() == 1) then
        player:PrintToPlayer(string.format("mir was already activated."));
        return
    end


    player:setMir(count);
    leader:PrintToPlayer(string.format("Your treasure pool now has mir."));
    player:PrintToPlayer(string.format("Treasure pool now has mir."));
end
