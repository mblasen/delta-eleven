---------------------------------------------------------------------------------------------------
-- func: warp
-- desc: sends player to their homepoint.
---------------------------------------------------------------------------------------------------

cmdprops =
{
    permission = 0,
    parameters = ""
};

function onTrigger(player)
    player:warp();
    player:PrintToPlayer("Returning to home point.");
end
