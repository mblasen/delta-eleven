-----------------------------------
-- Area: Aht Urhgan Whitegate
--  NPC: Ghanraam
-- Type: "Nyzul Weapon/Salvage Armor Storer,"
-- !pos 108.773 -6.999 -51.297 50
-----------------------------------
package.loaded["scripts/zones/Aht_Urhgan_Whitegate/TextIDs"] = nil;
-----------------------------------

function onTrade(player,npc,trade)
    --player trades the 3 salvage pieces. no money, no crafting items.

    --Ares set
    if( (trade:hasItemQty(16085,1) == true)
        and (trade:hasItemQty(16086,1) == true)
        and (trade:hasItemQty(16087,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(16084); --Ares mask
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 16084 );
    elseif( (trade:hasItemQty(14547,1) == true)
        and (trade:hasItemQty(14548,1) == true)
        and (trade:hasItemQty(14549,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14546); --Ares cuirass
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14546 );
    elseif( (trade:hasItemQty(14962,1) == true)
        and (trade:hasItemQty(14963,1) == true)
        and (trade:hasItemQty(14964,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14961); --Ares gauntlets
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14961 );
    elseif( (trade:hasItemQty(15626,1) == true)
        and (trade:hasItemQty(15627,1) == true)
        and (trade:hasItemQty(15628,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15625); --Ares flanchard
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15625 );
    elseif( (trade:hasItemQty(15712,1) == true)
        and (trade:hasItemQty(15713,1) == true)
        and (trade:hasItemQty(15714,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15711); --Ares sollerets
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15711 );

    --Skadi set
    elseif( (trade:hasItemQty(16089,1) == true)
        and (trade:hasItemQty(16090,1) == true)
        and (trade:hasItemQty(16091,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(16088); --Skadi visor
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 16088 );
    elseif( (trade:hasItemQty(14551,1) == true)
        and (trade:hasItemQty(14552,1) == true)
        and (trade:hasItemQty(14553,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14550); --Skadi Cuirie
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14550 );
    elseif( (trade:hasItemQty(14966,1) == true)
        and (trade:hasItemQty(14967,1) == true)
        and (trade:hasItemQty(14968,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14965); --Skadi bazubands
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14965 );
    elseif( (trade:hasItemQty(15630,1) == true)
        and (trade:hasItemQty(15631,1) == true)
        and (trade:hasItemQty(15632,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15629); --Skadi chausses
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15629 );
    elseif( (trade:hasItemQty(15716,1) == true)
        and (trade:hasItemQty(15717,1) == true)
        and (trade:hasItemQty(15718,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15715); --Skadi jambeaux
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15715 );

    --Ushukane set
    elseif( (trade:hasItemQty(16093,1) == true)
        and (trade:hasItemQty(16094,1) == true)
        and (trade:hasItemQty(16095,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(16092); --Ushukane somen
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 16092 );
    elseif( (trade:hasItemQty(14555,1) == true)
        and (trade:hasItemQty(14556,1) == true)
        and (trade:hasItemQty(14557,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14554); --Ushukane haramaki
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14554 );
    elseif( (trade:hasItemQty(14970,1) == true)
        and (trade:hasItemQty(14971,1) == true)
        and (trade:hasItemQty(14972,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14969); --Ushukane gote
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14969 );
    elseif( (trade:hasItemQty(15634,1) == true)
        and (trade:hasItemQty(15635,1) == true)
        and (trade:hasItemQty(15636,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15633); --Ushukane hizayoroi
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15633 );
    elseif( (trade:hasItemQty(15720,1) == true)
        and (trade:hasItemQty(15721,1) == true)
        and (trade:hasItemQty(15722,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15719); --Ushukane suneate
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15719 );

    --Marduk set
    elseif( (trade:hasItemQty(16097,1) == true)
        and (trade:hasItemQty(16098,1) == true)
        and (trade:hasItemQty(16099,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(16096); --Marduk tiara
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 16096 );
    elseif( (trade:hasItemQty(14559,1) == true)
        and (trade:hasItemQty(14560,1) == true)
        and (trade:hasItemQty(14561,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14558); --Marduk jubbah
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14558 );
    elseif( (trade:hasItemQty(14974,1) == true)
        and (trade:hasItemQty(14975,1) == true)
        and (trade:hasItemQty(14976,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14973); --Marduk dastanas
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14973 );
    elseif( (trade:hasItemQty(15638,1) == true)
        and (trade:hasItemQty(15639,1) == true)
        and (trade:hasItemQty(15640,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15637); --Marduk shalwar
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15637 );
    elseif( (trade:hasItemQty(15724,1) == true)
        and (trade:hasItemQty(15725,1) == true)
        and (trade:hasItemQty(15726,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15723); --Marduk crackows
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15723 );

    --Morrigan set
    elseif( (trade:hasItemQty(16101,1) == true)
        and (trade:hasItemQty(16102,1) == true)
        and (trade:hasItemQty(16103,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(16100); --Morrigan coronal
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 16100 );
    elseif( (trade:hasItemQty(14563,1) == true)
        and (trade:hasItemQty(14564,1) == true)
        and (trade:hasItemQty(14565,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14562); --Morrigan robe
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14562 );
    elseif( (trade:hasItemQty(14978,1) == true)
        and (trade:hasItemQty(14979,1) == true)
        and (trade:hasItemQty(14980,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(14977); --Morrigan cuffs
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 14977 );
    elseif( (trade:hasItemQty(15642,1) == true)
        and (trade:hasItemQty(15643,1) == true)
        and (trade:hasItemQty(15644,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15641); --Morrigan slops
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15641 );
    elseif( (trade:hasItemQty(15728,1) == true)
        and (trade:hasItemQty(15729,1) == true)
        and (trade:hasItemQty(15720,1) == true)
        and (trade:getItemCount() == 3)) then
            player:addItem(15727); --Morrigan pigaches
            player:tradeComplete();
            player:messageSpecial( ITEM_OBTAINED, 15727 );
    end
end;

function onTrigger(player,npc)
    player:PrintToPlayer("Trade me all 3 salvage pieces for the item. No weapons.")
    --player:startEvent(0x037d);
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;

