-- Variable TextID   Description text

-- General Texts
ITEM_CANNOT_BE_OBTAINED = 6380; -- You cannot obtain the item <item>. Come back after sorting your inventory.
          ITEM_OBTAINED = 6386; -- Obtained: <item>.
           GIL_OBTAINED = 6387; -- Obtained <number> gil.
       KEYITEM_OBTAINED = 6389; -- Obtained key item: <keyitem>.

    SENSE_OF_FOREBODING = 6401; -- You are suddenly overcome with a sense of foreboding...
NOTHING_OUT_OF_ORDINARY = 6400; -- There is nothing out of the ordinary here.
    IRREPRESSIBLE_MIGHT = 6404; -- An aura of irrepressible might threatens to overwhelm you...

-- ZM4 Dialog
       SOMETHING_BETTER = 7314; -- Don't you have something better to do right now?
     CANNOT_REMOVE_FRAG = 7317; -- It is an oddly shaped stone monument. A shining stone is embedded in it, but cannot be removed...?Prompt?
  ALREADY_OBTAINED_FRAG = 7318; -- You have already obtained this monument's
        FOUND_ALL_FRAGS = 7320; -- You have obtained ! You now have all 8 fragments of light!
        ZILART_MONUMENT = 7321; -- It is an ancient Zilart monument.?Prompt?

-- conquest Base
          CONQUEST_BASE = 7048; -- Tallying conquest results...
