-----------------------------------
-- Area: Lower Jeuno
-- NPC: Signet Guy
-- !pos !home
-------------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/status");
require("scripts/globals/conquest");
require("scripts/zones/Lower_Jeuno/TextIDs");

local guardnation = OTHER; -- SANDORIA, BASTOK, WINDURST, OTHER(Jeuno).
local guardtype   = 1;     -- 1: city, 2: foreign, 3: outpost, 4: border

-----------------------------------
-- onTrade Action
-----------------------------------

-- i'm going to hope this works without the other boilerplate...
function onTrade(player,npc,trade)
    tradeConquestGuard(player,npc,trade,guardnation,guardtype);
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    -- we just want to give them signet. no menus.
    local duration = (player:getRank() + getNationRank(player:getNation()) + 3) * 3600;
    player:delStatusEffect(dsp.effect.SIGIL);
    player:delStatusEffect(dsp.effect.SANCTION);
    player:delStatusEffect(dsp.effect.SIGNET);
    player:addStatusEffect(dsp.effect.SIGNET,0,0,duration); -- Grant Signet
    player:PrintToPlayer("I have granted you Signet. May some crystal forever be in your pocket.");
    player:PrintToPlayer("I can recharge EXP rings too!");
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

-- no idea what this shit is for
function onEventUpdate(player,csid,option)
--    local inventory, size;
--    if (player:getNation() == 0) then
--        inventory = SandInv;
--        size = #SandInv;
--    elseif (player:getNation() == 1) then
--        inventory = BastInv;
--        size = #BastInv;
--    else
--        inventory = WindInv;
--        size = #WindInv;
--    end
--
--    updateConquestGuard(player,csid,option,size,inventory);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

-- no idea what this shit is for
function onEventFinish(player,csid,option)
--    local inventory, size;
--    if (player:getNation() == 0) then
--        inventory = SandInv;
--        size = #SandInv;
--    elseif (player:getNation() == 1) then
--        inventory = BastInv;
--        size = #BastInv;
--    else
--        inventory = WindInv;
--        size = #WindInv;
--    end
--
--    finishConquestGuard(player,csid,option,size,inventory,guardnation);
end;
