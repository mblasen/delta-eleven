-----------------------------------
-- Area: Lower Jeuno
-- NPC:  Synergy Guy
-- !pos who knows
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-----------------------------------

require("scripts/zones/Lower_Jeuno/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
    local inv_space = player:getFreeSlotsCount();
    local count = trade:getItemCount();
    if (inv_space < 3) then
        player:PrintToPlayer("You need at least 3 free spaces to trade with me.");
        return(0);
    elseif (count == 1) then -- exchanging relic for artifact
        return_item = nil;

        -- blu (mirage -> magus)
        if (trade:hasItemQty(11292,1)) then -- jubbah
            return_item = 14521;
        elseif (trade:hasItemQty(15025,1)) then -- bazubands
            return_item = 14928;
        elseif (trade:hasItemQty(16346,1)) then -- shalwar
            return_item = 15600;
        elseif (trade:hasItemQty(11465,1)) then --  keffiyeh
            return_item = 15265;
        elseif (trade:hasItemQty(11382,1)) then -- charuqs
            return_item = 15684;

        -- cor
        elseif (trade:hasItemQty(11468,1)) then -- tricorne
            return_item = 15266;
        elseif (trade:hasItemQty(15028,1)) then -- gants
            return_item = 14929;
        elseif (trade:hasItemQty(11295,1)) then -- frac
            return_item = 14522;
        elseif (trade:hasItemQty(16349,1)) then -- culottes
            return_item = 15601;
        elseif (trade:hasItemQty(11385,1)) then --bottes
            return_item = 15685;

        -- pup
        elseif (trade:hasItemQty(11471,1)) then -- taj
            return_item = 15267;
        elseif (trade:hasItemQty(15031,1)) then -- dastanas
            return_item = 14930;
        elseif (trade:hasItemQty(11298,1)) then -- tobe
            return_item = 14523;
        elseif (trade:hasItemQty(16352,1)) then -- churidars
            return_item = 15602;
        elseif (trade:hasItemQty(11388,1)) then --babouches
            return_item = 15686;

        --sch (head onry)
        elseif (trade:hasItemQty(11480,1)) then -- mortarboard
            return_item = 16140;

        else
            player:PrintToPlayer("I don't recognize that item.");
            return(0);
        end;

        if (return_item == nil) then
            player:PrintToPlayer("I don't recognize that item.");
            return(0);
        else
            player:addItem(return_item, 1);
            player:tradeComplete(trade);
            player:messageSpecial( ITEM_OBTAINED, return_item);
        end

    elseif (count == 8) then -- they want their hachirin-no-obi or gorget
        return_item = nil;
        if(
           trade:hasItemQty(15435,1) and -- karin
           trade:hasItemQty(15436,1) and -- hyorin
           trade:hasItemQty(15437,1) and -- furin
           trade:hasItemQty(15438,1) and -- dorin
           trade:hasItemQty(15439,1) and -- rairin
           trade:hasItemQty(15440,1) and -- suirin
           trade:hasItemQty(15441,1) and -- korin
           trade:hasItemQty(15442,1) -- anrin
          ) then
            return_item = 28419; -- hachirin-no-obi
        elseif(
           trade:hasItemQty(15495,1) and -- flame
           trade:hasItemQty(15496,1) and -- snow
           trade:hasItemQty(15497,1) and -- breeze
           trade:hasItemQty(15498,1) and -- soil
           trade:hasItemQty(15499,1) and -- thunder
           trade:hasItemQty(15500,1) and -- aqua
           trade:hasItemQty(15501,1) and -- light
           trade:hasItemQty(15502,1) -- shadow
          ) then
            return_item = 27510; -- fotia gorget
        end
        player:addItem(return_item, 1);
        player:tradeComplete(trade);
        player:messageSpecial(ITEM_OBTAINED, return_item);
    end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    -- player:startEvent(0x0366); -- or 0x0001
    player:PrintToPlayer("Synergy functions are as follows:");
    player:PrintToPlayer("Trade a BLU,COR, or PUP relic armor for respective artifact.");
    player:PrintToPlayer("Trade all 8 elemental obis for Hachirin-no-obi.");
    player:PrintToPlayer("Trade all 8 elemental gorgets for Fotia Gorget.");
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;
