-----------------------------------
-- Area: Aht Urhgan Whitegate
--  NPC: Asrahd
-- Type: Imperial Gate Guard
-- !pos 0.011 -1 10.587 50
-----------------------------------
-- package.loaded["scripts/zones/Aht_Urhgan_Whitegate/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/status");
-- require("scripts/globals/besieged");
-- require("scripts/zones/Aht_Urhgan_Whitegate/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
    player:PrintToPlayer("I can't do trades; try Signet Guy");
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    -- we just want to give them sanction. no menus.
    local duration = (player:getRank() + getNationRank(player:getNation()) + 3) * 3600;
    player:delStatusEffect(dsp.effect.SIGIL);
    player:delStatusEffect(dsp.effect.SANCTION);
    player:delStatusEffect(dsp.effect.SIGNET);
    player:addStatusEffect(dsp.effect.SANCTION,0,0,duration, 0); -- Grant Sanction
    player:PrintToPlayer("I have granted you Sanction. May some crystal forever be in your pocket.");
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
--     if (csid == 0x0276 and option >= 1 and option <= 2049) then
--         itemid = getISPItem(option)
--         player:updateEvent(0,0,0,canEquip(player,itemid))
--     end
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
--    if (csid == 0x276) then
--        if (option == 0 or option == 16 or option == 32 or option == 48) then -- player chose sanction.
--            if (option ~= 0) then
--                player:delCurrency("imperial_standing", 100);
--            end
--
--            player:delStatusEffect(EFFECT_SIGIL);
--            player:delStatusEffect(EFFECT_SANCTION);
--            player:delStatusEffect(EFFECT_SIGNET);
--            local duration = getSanctionDuration(player);
--            local subPower = 0; -- getImperialDefenseStats()
--            player:addStatusEffect(EFFECT_SANCTION,option / 16,0,duration,subPower); -- effect size 1 = regen, 2 = refresh, 3 = food.
--            player:messageSpecial(SANCTION);
--
--        elseif (option % 256 == 17) then -- player bought one of the maps
--            id = 1862 + (option - 17) / 256;
--            player:addKeyItem(id);
--            player:messageSpecial(KEYITEM_OBTAINED,id);
--            player:delCurrency("imperial_standing", 1000);
--        elseif (option <= 2049) then -- player bought item
--            item, price = getISPItem(option)
--            if (player:getFreeSlotsCount() > 0) then
--                player:delCurrency("imperial_standing", price);
--                player:addItem(item);
--                player:messageSpecial(ITEM_OBTAINED,item);
--            else
--                player:messageSpecial(ITEM_CANNOT_BE_OBTAINED,item);
--            end
--        end
--    end
end;
