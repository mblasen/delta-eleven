-----------------------------------
-- Area: Lower Jeuno
--  NPC: Ghebi Damomohe
-- Type: Standard Merchant
-- Starts and Finishes Quest: Tenshodo Membership
-- @zone 245
-- !pos 16 0 -5
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil
package.loaded["scripts/globals/settings"] = nil
-----------------------------------
require("scripts/zones/Lower_Jeuno/TextIDs")
require("scripts/globals/keyitems")
require("scripts/globals/settings")
require("scripts/globals/npc_util")
require("scripts/globals/titles")
require("scripts/globals/quests")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
    if player:getQuestStatus(JEUNO,TENSHODO_MEMBERSHIP) ~= QUEST_COMPLETED and npcUtil.tradeHas(trade, 548) then
        -- Finish Quest: Tenshodo Membership (Invitation)
        player:startEvent(108)
    elseif player:getCurrentMission(COP) == DARKNESS_NAMED and
           not player:hasKeyItem(dsp.ki.PSOXJA_PASS) and
           player:getVar("PXPassGetGems") == 1 and
           (npcUtil.tradeHas(trade, 1692) or npcUtil.tradeHas(trade, 1694) or npcUtil.tradeHas(trade, 1693)) then
        player:startEvent(52, 500 * GIL_RATE)
    end
end

function onTrigger(player,npc)
    local GetGems = player:getVar("PXPassGetGems");

    if player:getFameLevel(JEUNO) >= 2 and player:getQuestStatus(JEUNO, TENSHODO_MEMBERSHIP) == QUEST_AVAILABLE then
        -- Start Quest: Tenshodo Membership
        player:startEvent(106, 8)
    elseif player:hasKeyItem(dsp.ki.TENSHODO_APPLICATION_FORM) then
        -- Finish Quest: Tenshodo Membership
        player:startEvent(107)
    elseif player:getCurrentMission(COP) == DARKNESS_NAMED and not player:hasKeyItem(dsp.ki.PSOXJA_PASS) and GetGems == 0 then
        -- Mission: Darkness Named
        player:startEvent(54)
    elseif (GetGems == 1) then
        player:startEvent(53)
    else
        player:startEvent(106,4)
    end
end;

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
    if csid == 106 and option == 0 then
        local stock = {0x172C,1,  -- special k
                 0x172D,1, -- breastmilk
				 0x1738,1, -- frontier soda
				 0x101F,1, -- max potion +3
				 0x102F,1, -- pro-ether +3
				 0x1523,1, -- dawn mulsum
				 0x1075,1, -- icarus wing
				 0x1045,1, -- silent oil
				 0x1044,1, -- prism powder
				 0x14EC,1, -- remedy ointment
				 0x101B,1, -- x potion +3
				 0x1035,1, -- panacea
                 0x1039,1} -- antacid

        dsp.shop.general(player, stock, NORG)
    elseif csid == 106 and option == 2 then
        player:addQuest(JEUNO, TENSHODO_MEMBERSHIP)
    elseif csid == 107 then
        -- Finish Quest: Tenshodo Membership (Application Form)
        if npcUtil.completeQuest(player, JEUNO, TENSHODO_MEMBERSHIP, { item=548, title=dsp.title.TENSHODO_MEMBER, keyItem=dsp.ki.TENSHODO_MEMBERS_CARD }) then
            player:delKeyItem(dsp.ki.TENSHODO_APPLICATION_FORM)
        end
    elseif csid == 108 then
        -- Finish Quest: Tenshodo Membership (Invitation)
        if npcUtil.completeQuest(player, JEUNO, TENSHODO_MEMBERSHIP, { item=548, title=dsp.title.TENSHODO_MEMBER, keyItem=dsp.ki.TENSHODO_MEMBERS_CARD }) then
            player:confirmTrade()
            player:delKeyItem(dsp.ki.TENSHODO_APPLICATION_FORM)
        end
    elseif csid == 52 then
        player:confirmTrade()
        player:addGil(500 * GIL_RATE)
        player:addKeyItem(dsp.ki.PSOXJA_PASS)
        player:messageSpecial(KEYITEM_OBTAINED, dsp.ki.PSOXJA_PASS)
        player:setVar("PXPassGetGems",0)
    elseif csid == 54 then
        player:setVar("PXPassGetGems",1)
    end
end
