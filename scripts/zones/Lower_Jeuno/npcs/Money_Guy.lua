-----------------------------------
-- Area: Aht Urhgan Whitegate
-- NPC:  Ahaadah
-- !pos -70 -6 105 50
-----------------------------------
package.loaded["scripts/zones/Lower_Jeuno/TextIDs"] = nil;
-------------------------------------
require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/dynamis");
require("scripts/zones/Lower_Jeuno/TextIDs");


-----------------------------------
-- onTrade Action
-----------------------------------

-- the primary goal of this guy is to trade
-- so here's a fuckin' mess of trades
function onTrade(player,npc,trade)
    -- 1449, 1450, 1451 -> single, hundred, 10k Shells
    -- 1452, 1453, 1454 -> single, hundred, 10k Coins
    -- 1455, 1456, 1457 -> single, hundred, 10k Bills
    local inv_space = player:getFreeSlotsCount();
    local count = trade:getItemCount();
    if (inv_space < 5) then
        player:PrintToPlayer("You need at least 5 free spaces to trade with me.");
        return(0);
    elseif (count == 1) then -- breaking currency
        return_count = 100;
        return_item = nil;

        if (trade:hasItemQty(1456,1)) then -- breaking 100 byne
            return_item = 1455;
        elseif (trade:hasItemQty(1453,1)) then -- breaking 100 coin
            return_item = 1452;
        elseif (trade:hasItemQty(1450,1)) then -- breaking 100 shell
            return_item = 1449;
        elseif (trade:hasItemQty(1457,1)) then -- breaking 10k byne
            return_item = 1456;
        elseif (trade:hasItemQty(1454,1)) then -- breaking 10k coin
            return_item = 1453;
        elseif (trade:hasItemQty(1451,1)) then -- breaking 10k shell
            return_item = 1450;
        else
            player:PrintToPlayer("I can't break that item.");
            return(0);
        end;

        if (return_item == nil) then
            player:PrintToPlayer("I don't recognize that item.");
            return(0);
        else
            player:addItem(return_item, 99);
            player:addItem(return_item, 1);
            player:tradeComplete(trade);
            player:messageSpecial( ITEM_OBTAINED, return_item);
        end
    else -- must be trying to trade for larger currency
        -- figure out what is trading (hopefully 100 of a currency)
        if (count >= 100) then
            if (trade:hasItemQty(1449, count)) then -- each checks that it's only one type of currency...
                base_item = 1449;
            elseif (trade:hasItemQty(1450, count)) then
                base_item = 1450;
            elseif (trade:hasItemQty(1452, count)) then
                base_item = 1452;
            elseif (trade:hasItemQty(1453, count)) then
                base_item = 1453;
            elseif (trade:hasItemQty(1455, count)) then
                base_item = 1455;
            elseif (trade:hasItemQty(1456, count)) then
                base_item = 1456;
            else
                player:PrintToPlayer("One type of ancient currency at a time, please.");
                return(0);
            end
            adv_item = base_item + 1;
            adv_count = 0;
            while (count >= 100) do
                adv_count = adv_count + 1;
                count = count - 100;
            end
            player:addItem(base_item, count);
            player:addItem(adv_item, adv_count);
            player:tradeComplete(trade);
            player:messageSpecial( ITEM_OBTAINED, adv_item);
            player:messageSpecial( ITEM_OBTAINED, base_item);
        else
            if(count > 1 and count < 100) then
                if (trade:hasItemQty(1449, count)) then -- each checks that it's only one type of currency...
                    new_item = 1452;
                elseif (trade:hasItemQty(1450, count)) then
                    new_item = 1453;
                elseif (trade:hasItemQty(1452, count)) then
                    new_item = 1455;
                elseif (trade:hasItemQty(1453, count)) then
                    new_item = 1456;
                elseif (trade:hasItemQty(1455, count)) then
                    new_item = 1449;
                elseif (trade:hasItemQty(1456, count)) then
                    new_item = 1450;
                else
                    player:PrintToPlayer("One type of ancient currency at a time, please.");
                    return(0);
                end
            end
            player:addItem(new_item, count);
            player:tradeComplete(trade);
            player:messageSpecial( ITEM_OBTAINED, new_item);
        end
    end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    --player:startEvent(0x0366); -- or 0x0001
    player:PrintToPlayer("I can convert ancient currency up/down for you.");
    player:PrintToPlayer("Only one type of money at a time, please!");
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;
