-----------------------------------
-- Area: Bastok Mines
--  NPC: Galzerio
-- Standard Merchant NPC
-----------------------------------
package.loaded["scripts/zones/Bastok_Mines/TextIDs"] = nil
-----------------------------------
require("scripts/zones/Bastok_Mines/TextIDs")
require("scripts/globals/shop")

function onTrade(player,npc,trade)
end

function onTrigger(player,npc)
    local stock =
    {
        0x1692, 1,3,     --crepe b hellene
        0x142B,   1,3,     --sole sushi +1

        0x1685,     1,3,     --r curry bun +1
        0x1437,   1,3,     --ler taco

        0x1851, 1,3,     --cyc. coal.
        0x1941,    1,3,     --behemoth steak +1
        0x1875,    1,3,     --akamochi +1
        0x3138,   216,3,     --Robe
        0x31B8,   118,3,     --Cuffs
        0x3238,   172,3,     --Slops
        0x32B8,   111,3,     --Ash Clogs
        0x30B0,  1742,3,     --Headgear
        0x3130,  2470,3,     --Doublet
        0x31B0,  1363,3,     --Gloves
        0x3230,  1899,3,     --Brais
        0x32B0,  1269,3         --Gaiters
    }

    player:showText(npc,GELZERIO_SHOP_DIALOG)
    dsp.shop.nation(player, stock, dsp.nation.BASTOK)
end

function onEventUpdate(player,csid,option)
end

function onEventFinish(player,csid,option)
end
