-----------------------------------
-- Area: Castle Oztroja
-- NPC:  Odontotyrannus spawn point
-- Involved in Quest: A Boy's Dream
-- !pos-80 -40 -79
-----------------------------------
package.loaded["scripts/zones/Castle_Oztroja/TextIDs"] = nil;
-----------------------------------

require("scripts/globals/settings");
require("scripts/globals/keyitems");
require("scripts/globals/quests");
require("scripts/zones/Castle_Oztroja/TextIDs");

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
    --player:messageSpecial(NOTHING_OUT_OF_ORDINARY);
    if (player:getQuestStatus(SANDORIA,A_BOY_S_DREAM) == QUEST_ACCEPTED and player:hasItem(4562) == false) then
      --SpawnMob(17182721):updateClaim(player); -- spawns in Castle_Oztroja S???
      player:addItem(4562, 1);
      player:messageSpecial(ITEM_OBTAINED, 4562);
    end
end;

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;

-----------------------------------
-- onEventFinish
-----------------------------------

function onEventFinish(player,csid,option)
    -- printf("CSID: %u",csid);
    -- printf("RESULT: %u",option);
end;
