-----------------------------------
-- Area: Nashmau
--  NPC: Paparoon
-- Standard Info NPC
-----------------------------------
package.loaded["scripts/zones/Nashmau/TextIDs"] = nil;
-----------------------------------
require("scripts/zones/Nashmau/TextIDs");

local mythic_cost = 10000; -- the new mythic cost

-----------------------------------
-- onTrade Action
-----------------------------------

function onTrade(player,npc,trade)
    local current_alex_amount = player:getCurrency("bayld")
    local item_count = trade:getItemCount();

    -- check if player can afford it now; use it later if needed
    if(current_alex_amount >= mythic_cost) then
        has_enough_alex = true;
    else
        has_enough_alex = false;
    end

    if (trade:hasItemQty(2488,item_count) and trade:getItemCount() == item_count) then
        -- add their new alexandrites
        player:addCurrency("bayld", item_count, player);
        local newAmount = player:getCurrency("bayld");
        player:PrintToPlayer(string.format("You now have %i alexandrites stored.", newAmount));
        player:tradeComplete(trade);

    elseif (has_enough_alex) then
        -- check if they are giving us a vigil weapon
        if(trade:hasItemQty(18492,1) and trade:getItemCount() == 1) then
            player:addItem(18991); -- conqueror
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18991 );

        elseif(trade:hasItemQty(18753,1) and trade:getItemCount() == 1) then
            player:addItem(18992); --glanzfaust
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18992 );

        elseif(trade:hasItemQty(18851,1) and trade:getItemCount() == 1) then
            player:addItem(18993); --yagrush
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18993 );

        elseif(trade:hasItemQty(18589,1) and trade:getItemCount() == 1) then
            player:addItem(18994); --laevateinn
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18994 );

        elseif(trade:hasItemQty(17742,1) and trade:getItemCount() == 1) then
            player:addItem(18995); --murgleis
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18995 );

        elseif(trade:hasItemQty(18003,1) and trade:getItemCount() == 1) then
            player:addItem(18996); --vajra
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18996 );

        elseif(trade:hasItemQty(17744,1) and trade:getItemCount() == 1) then
            player:addItem(18997); --burtgang
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18997 );

        elseif(trade:hasItemQty(18944,1) and trade:getItemCount() == 1) then
            player:addItem(18998); --liberator
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18998 );

        elseif(trade:hasItemQty(17956,1) and trade:getItemCount() == 1) then
            player:addItem(18999); --aymur
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18999 );

        elseif(trade:hasItemQty(18034,1) and trade:getItemCount() == 1) then
            player:addItem(19000); --carnwenhan
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19000 );

        elseif(trade:hasItemQty(18719,1) and trade:getItemCount() == 1) then
            player:addItem(19001); --gastraphetes
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19001 );

        elseif(trade:hasItemQty(18443,1) and trade:getItemCount() == 1) then
            player:addItem(19002); --kogarasumaru
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19002 );

        elseif(trade:hasItemQty(18426,1) and trade:getItemCount() == 1) then
            player:addItem(19003); --nagi
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19003 );

        elseif(trade:hasItemQty(18120,1) and trade:getItemCount() == 1) then
            player:addItem(19004); --ryunohige
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19004 );

        elseif(trade:hasItemQty(18590,1) and trade:getItemCount() == 1) then
            player:addItem(19005); --nirvana
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19005 );

        elseif(trade:hasItemQty(17743,1) and trade:getItemCount() == 1) then
            player:addItem(19006); --tizona
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19006 );

        elseif(trade:hasItemQty(18720,1) and trade:getItemCount() == 1) then
            player:addItem(19007); --death penalty
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19007 );

        elseif(trade:hasItemQty(18754,1) and trade:getItemCount() == 1) then
            player:addItem(19008); --kenkonken
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 19008 );

        elseif(trade:hasItemQty(19102,1) and trade:getItemCount() == 1) then
            player:addItem(18989); --terpischore
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18989 );

        elseif(trade:hasItemQty(18592,1) and trade:getItemCount() == 1) then
            player:addItem(18990) --tupsimati
            player:tradeComplete(trade);
            player:delCurrency("bayld", mythic_cost);
            player:messageSpecial( ITEM_OBTAINED, 18990 );

        else
            -- they did something stupid
            player:PrintToPlayer(string.format("Invalid trade.", current_alex_amount));
            player:PrintToPlayer(string.format("You now have %i alexandrites stored.", current_alex_amount));
        end
    else
        -- they did something stupid
        player:PrintToPlayer(string.format("Invalid trade.", current_alex_amount));
        player:PrintToPlayer(string.format("You now have %i alexandrites stored.", current_alex_amount));
    end
end;

-----------------------------------
-- onTrigger Action
-----------------------------------

function onTrigger(player,npc)
--player:startEvent(0x001A);
    player:PrintToPlayer(string.format("Bring me %i alexandrite and a Vigil Weapon for a Mythic Weapon!", mythic_cost));
    local newAmount = player:getCurrency("bayld");
    player:PrintToPlayer(string.format("You now have %i alexandrites stored.", newAmount));
end; 

-----------------------------------
-- onEventUpdate
-----------------------------------

function onEventUpdate(player,csid,option)
end;

function onEventFinish(player,csid,option)
end;

