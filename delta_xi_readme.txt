This is intended to capture some of the major points in operating the deltaXI
server. It contains some specific details about my understanding of the DSP
software, usage of our process-management software (including `tmux`), general
`bash` tips and tricks, and more. It's not a complete guide by any means but
it should help the reader get started. -dave

Contents:
  0.  DSP documentation
  1.  Quick start
    - Test server tips
  2.  Interacting with `bash`
    - Autocomplete
    - History
    - Miscellaneous bash tips
  3.  Using the process management software
    - tmux
  4.  Troubleshooting
    - common issues
    - dsconnect issues
    - dsgame issues
    - dssearch issues
  5.  Misc
    - Testing on the server
      - git
      - c++
      - lua
      - sql
    - Updating the live server

Test Server credentials:
  username: game
  password: gamegame
  mysql username: game
  mysql password: gamegame

--------------------------------------------------------------------------------
0:  DSP documentation

DSP has a guide for setting up the server at
https://wiki.dspt.info/index.php/Building_the_Server
which I use regularly to keep track of what I need to do. The general setup
is to configure the operating system, clone the DSP software, compile the c++,
pack the database, and configure the software's scripts (and zone IDs in mysql).
I find it's helpful to follow along with this as I make changes since it gives
a nice road map of the whole process.

There are other sources of DSP documentation but they are pretty incomplete, so
I can't really recommend any of them. There has been talk of a `doxygen` install
in the future which would significantly improve the documentation but I don't
think anyone's actually done it yet.

--------------------------------------------------------------------------------
1.  Quick start

The operating system will ask for your username. Then it will ask for the
password for that user; you won't see any input when typing the password.

Once you're logged in, you need to get to the delta_xi install directory. Enter
`ls -l`
to see what's in your home directory as a --list. You should see some entries
with a 'd' in their file descriptors meaning they are directories. You probably
want the most recently changed directory so
`cd <dirname>`
into that directory. You need to `cd` until you are in delta_xi.

Now we will try to start the software. Enter
`python automaton.py --start &`
to try to launch the software. If it works you should see something like
[1] 29307
back, which means that the OS has started a background process (that & above
tells the OS that this shouldn't run in the foreground).

Now let's check on the processes. Enter
`tmux a`
to attach to the tmux session that the script started. You should see this
along the bottom of the screen:
[game] 0:bash  1:dsgame  2:dsconnect  3:dssearch  4:process_manager
These are all the windows in this tmux session. You are probably on 0. You can
go to the next window with
`Ctrl+b -> n`
Note that Virtualbox reserves right ctrl for escaping the VM, so be warned!

Move to each of the ds___ windows and make sure the process is running. You
should see each of the processes reporting that they are ready. For example,
dsgame shows this as its last line of output:
[19/Dec] [14:50:58][Status] luautils::free:lua free...   - [OK]
and this means it's alive and well. If you see any errors but it reaches this
point, then it's probably ok! However, if you have a bash prompt:
game@test:~/delta-eleven$
Then you have a problem. This means the program exited from its errors... not
good. See Troubleshooting for what this could mean.

If everything is running then you should be able to connect and play. You can
also see a lot of what's going on in the tmux windows, so poke around!

------Test server tips----------------------------------------------------------

Test Server credentials:
  username: game
  password: gamegame
  mysql username: game
  mysql password: gamegame

UNIX passwords have to be at least 8 chars. Oh well.

You probably need to change your database zone settings as per the DSP guide.
Enter this into `bash` to find your local IP address:
`ifconfig`
and take note of your ethernet adapter's entry (should resemble this):
eth0      Link encap:Ethernet  HWaddr 08:00:27:9f:85:a5
          inet addr:192.168.0.103  Bcast:192.168.0.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fe9f:85a5/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:572821 errors:0 dropped:0 overruns:0 frame:0
          TX packets:171254 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:332040268 (332.0 MB)  TX bytes:34440366 (34.4 MB)
The field you want is `inet addr` (192.168.0.103). Take that number and write
over the values in the database as described in the DSP guide.

Remember that Virtualbox reserves right ctrl to escape the VM. You can use PuTTY
(windows program that gives you secure shell behavior) to connect to the test
server (and also to a live deltaXI install!) and get around the limitations of
Vbox, like the tiny screen. I recommend giving it a try since it's how you can
also talk to a live install.

--------------------------------------------------------------------------------
2.  Interacting with `bash`

`bash` is how we generally talk to *NIX machines, though there are other shells.
It has a lot of handy features that you will just plain have to learn on your
own but I'll try to give you a helpful push.

...................
Autocomplete:  `bash` will try to autocomplete if you press <Tab> partway
through a keyword. For example, if I have typed `cd delt` and press <Tab>, it
will probably complete to `cd delta_xi/`. If you press <Tab> and nothing happens
then it either has no idea what you want or there are multiple possible answers.
If you press <Tab> twice in rapid succession, it will list the possible answers
and you can type a little more to help it along. For example:

game@test:~/delta-eleven$ vim con
conf/          config.guess   config.log     config.status  config.sub
game@test:~/delta-eleven$ vim conf/
config.yaml               items.csv                 lan_config.conf
generate_items_csv.py     items.csv.1               login_darkstar.conf
game@test:~/delta-eleven$ vim conf/login_darkstar.conf

Here I have used <Tab> suggestions twice to zero in on the file I want; first
I find the directory `conf` then I peek inside it for possible files.

...................
History:  `bash` keeps a `history` of previous commands you've run. The easiest
use of this is to press the <Up> key which will load the previous command into
your prompt. You can do this as far as your `history` goes.

You can also use `history` directly. You'll get something like this:

game@test:~/delta-eleven$ history
    1  vim scripts/commands/bird.lua
    2  git pull
    3  git status
    4  sh autogen.sh
    5  python automaton.py --stop
    6  vim conf/login_darkstar.conf
    7  vim conf/search_server.conf
    8  vim conf/map_darkstar.conf

and so on. You can also manipulate the output of `history`. My favorite trick
is to pipe (|) the output into `grep` (a powerful utility I will not get into)
and search for an identifier. Let's say I want to see previous calls of `mysql`:

game@test:~/delta-eleven$ history | grep mysql
   14  mysql -u game -pgamegame
   21  mysql dspdb -u game -pgamegame < item_mods.sql
   22  mysql dspdb -u game -pgamegame < mob_droplist.sql
   23  mysql -u game -pgamegame
   83  history | grep mysql
   84  mysql -u game -pgamegame
  135  mysql -u game -pgamegame
  194  mysql -u game -pgamegame
  201  history | grep mysql

You can re-run one of these commands like so:
`!194`
This will re-run command 194. Very handy indeed. Play around with this.

...................
Miscellaneous bash tips:

`bash` uses a bunch of small common programs to do its work. It knows what they
are by looking in its so-called $PATH which includes places like `/usr/bin` to
tell it what program names are. I don't recommend looking around in there or,
god forbid, changing anything, but it helps to keep in mind that this is how
`bash` knows what stuff is called. Some practical programs you'll want to know:

`ls`: list directory contents. Add -l for list format, -a to show hidden files.
`cd`: change directory
`nano`: a very simple file editor.
`vim`: a much less simple editor. Not recommended for newcomers.
`top`: show running processes
  alt: `htop`: show processes in a much nicer output
`rm`: remove a file
`mv`: move a file (also: rename)
`touch`: make a file (also: update its access history)
`sudo`: super user do; run the next command as super user (danger will robinson)

And some other programs we specifically interact with:

`git`: version control software
`mysql`: database software
`mysqldump`: put the database to a file you can take home.
`gcc`: GNU Compiler Collection; turn source code into binary.
  or in our case `make`: runs makefiles that call `gcc` how it's needed.
`python`: python interpreter; runs python scripts or launches python shell
`sh`: shell; run a file as the system's default shell (`bash` usually).
`tmux`: terminal multiplexer; make 'windows' in terminal you can swap around.
  -details on this in section 3.

Books have been written about linux system administration, bash, and other
topics covered here. I recommend using google to chase down questions as they
come up rather than trying to learn it all at once.

--------------------------------------------------------------------------------
3.  Using the process management software

The `automaton.py` file launches a `tmux` session that continually checks on
all of the processes the game servers need. I wouldn't mess with the file
unless you think you can make an improvement or need to know about its operation
for some reason.

...................
tmux

All commands in `tmux` are started by sending the sequence
Ctrl+b
at which point `tmux` will start listening. The next command you send will then
be processed by `tmux` instead of `bash`. The commands you probably want are:
`n`: next window
`p`: previous window
`c`: create a new window
`x`: destroy a window (careful: this can wreck the automaton.)
`d`: detach from the session (go back to when you entered)

There are a *ton* of commands for `tmux` but these are the basics.

--------------------------------------------------------------------------------
4.  Troubleshooting

...................
...................
common issues:
...................
...................

Yeah, you're going to have problems. We'll try to document them here as they
arise.

One of the biggest sources of issues I have seen is the config files:
 lan_config.conf
 login_darkstar.conf
 map_darkstar.conf
make sure they have the correct `mysql` username and password!! I cannot tell
you how often this issue comes up.

In addition, make sure the `zone_settings` table in `mysql` has the right IP
address! This should be the public IP address in the case of live or the LAN
IP address in the case of test. Either way, find it with `ifconfig`.

...................
...................
dsconnect issues:
...................
...................

...................
dsconnect keeps throwing an error with make_listen_bind or similar

this is normal on restart. UNIX needs to wait until previous clients give up on
messaging it before it can give the socket to the new process. Just wait and
let the server keep trying to restart dsconnect until it succeeds.

...................
dsconnect won't let me log in and says something like:
[31/Dec 09:24][Error] lobbyview_parse: Incorrect client version: got 301712xx_x, expected 301711xx_x
[31/Dec 09:24][Error] lobbyview_parse: The server must be updated to support this client version

The version lock is enabled. Type this straight into dsconnect:
`verlock`
and it should tell you that the version lock has been disabled. voila.

...................
...................
dsgame issues:
...................
...................

dsgame is pretty robust generally, but see the above tips if it has problems.

...................
...................
dssearch issues:
...................
...................

dssearch barely does anything so it should rarely have problems.

--------------------------------------------------------------------------------
5.  Misc

...................
Testing on the server:

  git:
Use `git` to bring in your changes before proceeding.
First, `pull` to make sure you are up to date. Then `checkout` master and `pull`
again to make really sure. Somewhere, `git` should tell you it's getting some
new stuff. Now `checkout` the branch you want to test. If you're on your branch
of interest, you are ready to proceed.

  c++:
You can do this while server is running, but must restart.
Re-run `make` as per DSP build instructions and you should see it rebuilding
some stuff. If it exits without any ERRORs in its output, it's probably gravy.
If there are errors, then you have either compiler problems or bad c++. Once
it's compiled, you should be able to restart the servers.

  lua:
You can do this while server is running but may need to restart.
Simply paste over the old files with the new. This can be done by directly
replacing a file or by using `git checkout` to change which version of a file
is in use by the OS.

  sql:
**You should not do this while the server is running.**

First, make a backup if it's on live. Takes a minute but prevents grief.

Once the new version of your sql file is on the server, go to
`delta_xi/sql/` and load the file in like this (e.g. on test server):
`mysql dspdb -u game -pgamegame < item_mods.sql`
This literally dumps the characters from the file into the mysql prompt so make
sure you know what's in your file.

The DSP build guide shows a crafty little script which does this for all files
inside the sql directory. You can do this if you want, but be warned:
**THIS WILL DESTORY ALL CHARACTER AND ACCOUNT DATA**
So I recommend doing this one file at a time.

...................
Updating the live server:

Let's not worry about that for now...
