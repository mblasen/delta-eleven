import re #weapons grade bullshit incoming
import csv

infile = "../sql/item_basic.sql"
outfile = "new.csv"

exclude_items = [13014, #leaping boots
                 12486, #emperor hairpin
                 13056, #peacock charm
                 17440] #kraken club

with open(infile) as f:
    content = f.readlines()
header = "itemid, name, sell01, buy01, price01, stock01, rate01, sell12, buy12, price12, stock12, rate12"
counter = 0 #for some reason this asshole puts an excel header every 100 lines. we will too.
outlines = []
outlines.append(header)
for line in content:
    pieces = line.split(" ")
    try:
        if pieces[0] != "INSERT":
            continue
        else:
            vals = pieces[-1] #last item is what we want
            vals = re.sub('[\'`();_.]', '', vals) #substitute these characters with nothing
            vals = vals.split(",") #open it up as a list
            itemid = vals[0]
            #skip 1 (sub_id); it's probably trash
            #skip 2 (name); sort name is better
            name = vals[3] #(sortname)
            #skip 4 (stack size); we don't care.
            #skip 5 (flags); not sure but we don't need it
            ah = vals[6] #category in AH; 99 -> not sellable! NOTE
            nosell = vals[7] #this is nosell to NPCs! not ah...
            #skip 8 (basesell); don't know or care.
        if ((ah != "99") and (int(itemid) not in exclude_items)): #it goes on the AH
            entry = str("%s, %s, 1, 1, 2, 255, 1.0, 0, 1, 2, 255, 1.0" % (str(itemid), str(name)))
            outlines.append(entry)
            counter += 1
        if (counter == 100):
            outlines.append(header)
            counter = 0
    except:
        continue
with open(outfile, 'w') as f:
    for entry in outlines:
        f.write("%s\n" % entry)
print("new items file written.")
